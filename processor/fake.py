# -*- coding: utf-8 -*-

import numpy as np


non_closure_correction = {
    "fake_electrons": {
        2016: 1.376,
        2017: 1.252,
        2018: 1.325,
    },
    "fake_muons": {
        2016: 1.050,
        2017: 1.157,
        2018: 1.067,
    },
}


def ff(fr):
    fr = np.minimum(fr, 0.8)
    return fr / (1 - fr)

import operator
from functools import reduce

import awkward as ak
import numpy as np


class POGLeptonSF:
    def __init__(self, corrections):
        self.evaluator = corrections[self.lepton]
        self.prefix = f"{self.lepton}_POG_"

    @staticmethod
    def _infer(axes):
        Ipt = Ieta = Aeta = None
        for i, edges in enumerate(axes):
            assert i < 2
            if np.max(edges) > 5:
                # is pT
                assert Ipt is None
                Ipt = i
                continue
            # is Eta
            assert np.max(edges) <= 5
            assert Ieta is None
            Ieta = i
            # abs Eta
            if np.min(edges) < 0:
                assert -np.min(edges) == np.max(edges)
                Aeta = False
            else:
                assert np.min(edges) == 0
                Aeta = True

        return Ipt, Ieta, Aeta

    def _calc(self, corr, obj, offset=0):
        try:
            Ipt, Ieta, Aeta = self._infer(corr._axes)
        except AssertionError as e:
            raise ValueError("can't infer pt/eta parameters for corrector: %s" % corr) from e

        args = [None, None]
        args[Ipt] = obj.pt
        args[Ieta] = abs(obj.eta) if Aeta else obj.eta

        return corr(*args) + offset

    def __contains__(self, key):
        return (self.prefix + key) in self.evaluator

    def _get(self, k2o, shift=False):
        return reduce(
            operator.mul,
            (
                ak.prod(
                    self._calc(self.evaluator[self.prefix + key], obj, offset=1 if shift else 0),
                    axis=-1,
                )
                for key, obj in k2o.items()
            ),
        )

    def _apply(self, weights, name, k2o):
        weights.add(
            name=name,
            weight=ak.to_numpy(self._get(k2o)),
            weightUp=ak.to_numpy(
                self._get({f"{key}_error": obj for key, obj in k2o.items()}, shift=True)
            ),
            weightDown=None,
        )


class POGElectronSF(POGLeptonSF):
    lepton = "electron"

    def __call__(self, electrons, weights):
        self._apply(weights, "electron_id", {"id_EGamma_SF2D": electrons})
        self._apply(
            weights,
            "electron_reco",
            {"reco_EGamma_SF2D": electrons}
            if "reco_EGamma_SF2D" in self
            else {
                "reco_high_EGamma_SF2D": electrons[electrons.pt >= 20],
                "reco_low_EGamma_SF2D": electrons[electrons.pt < 20],
            },
        )


class POGMuonSF(POGLeptonSF):
    lepton = "muon"

    def table(self, id_den, suffix):
        return {
            "muon_iso": f"iso_NUM_TightRelIso_DEN_TightIDandIPCut_{suffix}",
            "muon_id": f"id_NUM_TightID_DEN_{id_den}_{suffix}",
            # "muon_low_pt_id": "NUM_low_pt_id_TightID_DEN_genTracks_pt_abseta",
        }

    def __call__(self, muons, weights, year):
        year = str(year)
        id_den = {
            "2016": "genTracks",
            "2017": "genTracks",
            "2018": "TrackerMuons",
        }[year]
        if year == "2016":
            suffix = "pt_eta_statPlusSyst"
        else:
            suffix = "abseta_pt_statPlusSyst"
        for name, key in self.table(id_den, suffix).items():
            self._apply(weights, name, {key: muons})

import numpy as np
import numba
from numba import cuda

from coffea import hist
import boost_histogram as bh
import timeit


@cuda.jit
def fill_histogram_cuda(
    data: numba.float32[:],
    weights: numba.float32[:],
    bins: numba.float32[:],
    out_w: numba.float32[:, :],
    out_w2: numba.float32[:, :],
):
    assert len(data) == len(weights)
    assert len(bins) - 1 == out_w.shape[1]
    assert len(bins) - 1 == out_w2.shape[1]

    xi = cuda.grid(1)
    xstride = cuda.gridsize(1)
    bi = cuda.blockIdx.x
    bd = cuda.blockDim.x
    ti = cuda.threadIdx.x
    nbins = out_w.shape[1]
    for i in range(xi, len(data), xstride):
        bin_idx = searchsorted_devfunc_right(bins, data[i]) - 1
        if bin_idx >= nbins:
            bin_idx = nbins - 1
        bin_idx_histo = (bi, bin_idx)
        if bin_idx >= 0 and bin_idx < nbins:
            wi = weights[i]
            cuda.atomic.add(out_w, bin_idx_histo, wi)
            cuda.atomic.add(out_w2, bin_idx_histo, wi ** 2)


# Copied from numba source
@cuda.jit(device=True)
def searchsorted_inner_right(a, v):
    n = len(a)
    lo = np.int32(0)
    hi = np.int32(n)
    while hi > lo:
        mid = (lo + hi) >> 1
        if a[mid] <= v:
            # mid is too low => go up
            lo = mid + 1
        else:
            # mid is too high, or is a NaN => go down
            hi = mid
    return lo


@cuda.jit(device=True)
def searchsorted_devfunc_right(bins, val):
    ret = searchsorted_inner_right(bins, val)
    if val < bins[0]:
        ret = 0
    if val >= bins[len(bins) - 1]:
        ret = len(bins) - 1
    return ret


def fill_boost1d():
    h_boost1d = bh.Histogram(bh.axis.Regular(20, 0, 200))
    h_boost1d.fill(test_pt)


def fill_coffea1d():
    h_coffea1d = hist.Hist("pt", hist.Bin("x", "x", 20, 0, 200))
    h_coffea1d.fill(x=test_pt)


def fill_cuda1d():
    test_pt_gpu = cuda.to_device(test_pt)
    bins_gpu = cuda.to_device(np.linspace(0, 200, 21))
    nblocks = 128
    nthreads = 1024
    out_w_gpu = cuda.to_device(np.zeros((nblocks, len(bins_gpu) - 1), dtype=np.float32))
    out_w2_gpu = cuda.to_device(np.zeros((nblocks, len(bins_gpu) - 1), dtype=np.float32))
    weights_gpu = cuda.to_device(np.ones_like(test_pt))
    fill_histogram_cuda[nblocks, nthreads](
        test_pt_gpu, weights_gpu, bins_gpu, out_w_gpu, out_w2_gpu
    )
    cuda.synchronize()
    out_w_gpu.copy_to_host()


if __name__ == "__main__":
    np.random.seed(42)
    test_pt = np.random.exponential(10.0, size=100_000) + np.random.exponential(10, size=100_000)

    # for cuda

    test_pt_gpu = cuda.to_device(test_pt)
    bins_gpu = cuda.to_device(np.linspace(0, 200, 21))
    nblocks = 128
    nthreads = 1024
    out_w_gpu = cuda.to_device(np.zeros((nblocks, len(bins_gpu) - 1), dtype=np.float32))
    out_w2_gpu = cuda.to_device(np.zeros((nblocks, len(bins_gpu) - 1), dtype=np.float32))
    weights_gpu = cuda.to_device(np.ones_like(test_pt))

    """
    In [1]: %timeit fill_coffea1d()
    1.52 ms ± 62.5 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

    In [2]: %timeit fill_boost1d()
    510 µs ± 9.34 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

    In [3]: %timeit fill_histogram_cuda[nblocks, nthreads](test_pt_gpu, weights_gpu, bins_gpu, out_w_gpu, out_w2_gpu)
    319 µs ± 10.8 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
    """

import numpy as np

old = np.load("/home/pfackeldey/coffea_0644.npz")
new = np.load("/home/pfackeldey/coffea_0710.npz")


for k in old.keys():
    print(f"{k}:")
    ru = old[k][:, 0] / new[k][:, 0]
    print(f"\t * Up ratio (old/new): mean={np.mean(ru)}, max={np.max(ru)}, min={np.min(ru)}")
    rd = old[k][:, 1] / new[k][:, 1]
    print(f"\t * Down ratio (old/new): mean={np.mean(rd)}, max={np.max(rd)}, min={np.min(rd)}")
    print("")


"""
OUT: 

Absolute:
         * Up ratio (old/new): mean=1.0000000006729874, max=1.0000000600190562, min=0.9999999399421944
         * Down ratio (old/new): mean=1.000000000233497, max=1.0000000355919445, min=0.9999999628072612

Absolute_2018:
         * Up ratio (old/new): mean=1.0000000002156635, max=1.0000000597959087, min=0.9999999397704601
         * Down ratio (old/new): mean=0.9999999999620439, max=1.0000000337632624, min=0.9999999677455093

BBEC1:
         * Up ratio (old/new): mean=0.999999999778731, max=1.0000000597959087, min=0.9999999401295877
         * Down ratio (old/new): mean=0.999999999925947, max=1.0000000317439728, min=0.9999999682281276

BBEC1_2018:
         * Up ratio (old/new): mean=1.0000000009069787, max=1.000000059591253, min=0.9999999404497422
         * Down ratio (old/new): mean=1.000000000057292, max=1.00000003053609, min=0.999999969261916

EC2:
         * Up ratio (old/new): mean=0.9999999999588407, max=1.0000000597408993, min=0.9999999401204095
         * Down ratio (old/new): mean=1.0000000000317075, max=1.000000032567386, min=0.9999999676011143

EC2_2018:
         * Up ratio (old/new): mean=0.9999999973908422, max=1.0000000601898962, min=0.999999940568393
         * Down ratio (old/new): mean=0.9999999992419616, max=1.000000032095864, min=0.9999999685243838

FlavorQCD:
         * Up ratio (old/new): mean=0.9999999844433501, max=1.000000059518438, min=0.9999999405214154
         * Down ratio (old/new): mean=0.9999999863671226, max=1.0000000309048556, min=0.9999999694005356

HF:
         * Up ratio (old/new): mean=1.0000000000025666, max=1.000000060505765, min=0.9999999397353737
         * Down ratio (old/new): mean=0.9999999999688174, max=1.000000033392365, min=0.9999999659126608

HF_2018:
         * Up ratio (old/new): mean=1.0000000031656848, max=1.0000000594779686, min=0.9999999403922403
         * Down ratio (old/new): mean=0.9999999993429359, max=1.0000000303475824, min=0.9999999697582429

RelativeBal:
         * Up ratio (old/new): mean=1.0000000096972539, max=1.0000000597618215, min=0.9999999400558853
         * Down ratio (old/new): mean=1.0000000023149662, max=1.000000034011907, min=0.9999999664092645

RelativeSample_2018:
         * Up ratio (old/new): mean=0.999999997935234, max=1.000000060041011, min=0.999999939764234
         * Down ratio (old/new): mean=1.0000000011565056, max=1.000000034843052, min=0.9999999655816215

Total:
         * Up ratio (old/new): mean=1.0000000000150733, max=1.0000000613354183, min=0.9999999388278928
         * Down ratio (old/new): mean=1.0000000000744496, max=1.0000000392666604, min=0.9999999605430796
"""

import coffea

assert coffea.__version__ == "0.7.1"

import awkward as ak
from coffea.lookup_tools import extractor
from coffea.nanoevents import NanoEventsFactory
from coffea.jetmet_tools import JetCorrectionUncertainty
import numpy as np

factory = NanoEventsFactory.from_root(
    "/net/cache/cms/dihiggs/store/common/Run2_pp_13TeV_2018/DownloadNanoAODs/TTTo2L2Nu/nominal/FAFFF323-CDBE-D94B-A16D-04F461BD9E5C.root"
)
events = factory.events()

events = events[:100000]
jets = events.Jet

extract = extractor()
extract.add_weight_sets(
    [
        "* * /net/scratch/cms/dihiggs/store/bbww_dl/Run2_pp_13TeV_2018/DownloadFiles/c0ab64a11fd92d2593881369c370cff32dfb6c3c5e319a68ac3ef6c5caed5673/Regrouped_Autumn18_V19_MC_UncertaintySources_AK4PFchs.junc.txt"
    ]
)
extract.finalize()
evaluator = extract.make_evaluator()

junc_names = []
levels = []
for name in dir(evaluator):
    junc_names.append(name)
    if len(name.split("_")) == 8:
        levels.append("_".join(name.split("_")[-2:]))
    else:
        levels.append(name.split("_")[-1])

junc = JetCorrectionUncertainty(**{name: evaluator[name] for name in junc_names})
print(junc)

import time

start = time.time()
ret = junc.getUncertainty(JetEta=jets.eta, JetPt=jets.pt)

out = {}
for tpl in ret:
    out[tpl[0]] = np.array(ak.flatten(tpl[1]))  # shape: (780211, 2)
    ak.sum(tpl[1])

end = time.time()
print(f"Runtime: {(end - start):.2f}s")

np.savez("/home/pfackeldey/coffea_0710.npz", **out)

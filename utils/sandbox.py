# coding: utf-8

import os
from subprocess import Popen

import law
import luigi


class SimpleSandbox(law.Sandbox):
    def run(self, cmd, stdout=None, stderr=None):
        p = Popen(cmd, shell=True, env=self.env)

        try:
            p.wait()
        except KeyboardInterrupt:
            pass
        p.wait()

        return (p.returncode,) + p.communicate()

    @property
    def env(self):
        env = dict(os.environ)
        env.update(self._get_env())
        return env


class SGSandbox(SimpleSandbox):
    sandbox_type = "sg"

    def cmd(self, proxy_cmd):
        return law.util.quote_cmd(["sg", self.name, "-c", proxy_cmd.build()])


class OpenportsTask(law.SandboxTask):
    sandbox = "sg::openports"


class SubmitSandbox(SimpleSandbox):
    sandbox_type = "submit"

    def cmd(self, proxy_cmd):
        _cmd = ("submit", "-f")
        if self.task.memory:
            _cmd += ("-m", str(self.task.memory))
        if self.task.gpu_memory:
            _cmd += ("-M", str(self.task.gpu_memory))
        for key, val in self.task.submit_kwargs.items():
            _cmd += (str(key), str(val))
        return "%s %s" % (
            law.util.quote_cmd(_cmd),
            proxy_cmd.build(),
        )


class SubmitTask(law.SandboxTask):
    local = luigi.BoolParameter(significant=False)

    memory = None
    gpu_memory = None
    submit_kwargs = {}

    @property
    def sandbox(self):
        return "" if self.local else "submit::submit"

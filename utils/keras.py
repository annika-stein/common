# -*- coding: utf-8 -*-

import tensorflow as tf


class OneHotPDGID(tf.keras.layers.Layer):
    def call(self, inputs):
        return tf.one_hot(tf.cast(tf.math.abs(inputs[:, :, -2]) > 11, tf.int32), 2)


class OneHotCharge(tf.keras.layers.Layer):
    def call(self, inputs):
        return tf.one_hot(tf.cast(inputs[:, :, -1] > 0, tf.int32), 2)


class OneHotYear(tf.keras.layers.Layer):
    def call(self, inputs):
        return tf.one_hot(tf.cast(inputs[:, 0] - 2016, tf.int32), 3)


class BlackBtag(tf.keras.layers.Layer):
    def call(self, inputs):
        return tf.keras.layers.Concatenate()([inputs[:, :, :-1], 0 * inputs[:, :, -1:]])


class ThrowVariable1D(tf.keras.layers.Layer):
    def __init__(self, index, n=1):
        super().__init__()
        self.index = index
        self.n = n

    def call(self, inputs):
        return tf.keras.layers.Concatenate()(
            [inputs[..., : self.index], inputs[..., self.index + self.n :]]
        )

    def get_config(self):
        return {"index": self.index, "n": self.n}

# -*- coding: utf-8 -*-

import logging
import re


def get_parent_process(process, processes):
    for _, p in process.walk_parent_processes(include_self=True):
        if any(p == pg for pg in processes):
            return p


def getPGroot(analysis, process, group=("default",)):
    assert len(group) == 1
    processes = analysis.aux["process_groups"][group[0]]
    out = get_parent_process(process, processes)
    if out:
        return out
    else:
        logging.info(f"Process ({process}) not in process_group ({group})")


def dataset_in_group(ds, processes):
    res = set(get_parent_process(p, processes) is not None for p in ds.processes.values)
    if len(res) > 1:
        raise RuntimeError(f"dataset {ds} has incosistent membership among processes: {processes}")
    return True in res


def NanoAOD_version(dataset):
    (ret,) = set(re.search(r"NanoAODv(\d+)", key).group(1) for key in dataset.keys)
    return int(ret)

# coding: utf-8

"""
Binning helper functions.
"""


import numpy as np
import scipy
from numba import njit
from utils.util import linsplit


def optimize_binning(
    full_edges,
    s_vals,
    b_vals,
    s_errs,
    b_errs,
    n_start_bins,
    n_min_bins,
    y_low,
    y_high,
    x_min=None,
    x_max=None,
    callback=None,
    silent=False,
):

    # defaults
    if s_errs is None:
        s_errs = np.zeros_like(s_vals)
    if b_errs is None:
        b_errs = np.zeros_like(b_vals)

    # some input checks
    assert s_vals.sum() > 0
    assert b_vals.sum() > 0
    assert len(s_vals) == len(full_edges) - 1
    assert len(b_vals) == len(full_edges) - 1
    assert s_errs.shape == s_vals.shape
    assert b_errs.shape == b_vals.shape
    assert n_start_bins >= n_min_bins
    assert y_low <= y_high
    _mini_bin_widths = full_edges[1:] - full_edges[:-1]
    assert _mini_bin_widths.max() - _mini_bin_widths.min() < 1e-6

    # helpers
    def raise_min_bins():
        if silent:
            return None
        else:
            raise Exception(
                "bin contents insufficient for n_min_bins {} and y_low {}: {}".format(
                    n_min_bins, y_low, s_vals.sum() + b_vals.sum()
                )
            )

    def select_vals(vals, start, stop):
        vals = np.array(vals)
        vals[start] = vals[: start + 1].sum()
        vals[stop - 1] = vals[stop - 1 :].sum()
        return vals[start:stop]

    def select_errs(errs, start, stop):
        errs = np.array(errs)
        errs[start] = (errs[: start + 1] ** 2.0).sum() ** 0.5
        errs[stop - 1] = (errs[stop - 1 :] ** 2.0).sum() ** 0.5
        return errs[start:stop]

    def sizes_to_edges(sizes):
        return full_edges[[0] + np.cumsum(sizes).tolist()]

    # when x_min or x_max are "auto", auto detect the centrally populated range
    vals = s_vals + b_vals
    if x_min == "auto":
        x_min = full_edges[np.argwhere(vals > 0).reshape(-1)[0]]
    if x_max == "auto":
        x_max = full_edges[np.argwhere(vals > 0).reshape(-1)[-1] + 1]

    # x_min and x_max define the approximate range of optimized edges to return, so when they are
    # set, find the outer most approximate edges and limit all arrays
    start, stop = 0, len(s_vals)
    if x_min is not None:
        start = int(np.argwhere(full_edges <= x_min).reshape(-1)[-1])
    if x_max is not None:
        stop = int(np.argwhere(full_edges >= x_max).reshape(-1)[0])
    full_edges = full_edges[start : stop + 1]
    s_vals, s_errs = select_vals(s_vals, start, stop), select_errs(s_errs, start, stop)
    b_vals, b_errs = select_vals(b_vals, start, stop), select_errs(b_errs, start, stop)

    # recompute things
    vals = s_vals + b_vals
    itg = vals.sum()
    # errs = (s_errs**2. + b_errs**2.)**0.5

    # detect early when the bin contents are insufficient to fill n_min_bins with at least y_low
    if itg < n_min_bins * y_low:
        return raise_min_bins()

    # start with the requested number of bins and an even binning
    # for easier handling, keep track of bin widths ("sizes" below) in units of bins defined by
    # full_edges ("mini bins"), e.g. if bin 0 has a size 5, it combines the first 5 mini bins
    n_bins = n_start_bins
    sizes = None
    while True:
        if sizes is None:
            sizes = linsplit(len(s_vals), n_bins)
            print(f"start from even binning with {n_bins} bins")
            if callable(callback):
                callback(sizes_to_edges(sizes))

        # get bin contents and errors
        split_points = np.cumsum(sizes)[:-1]
        binned_vals = np.array([sum(s) for s in np.split(vals, split_points)])
        # binned_errs = np.array([sum(s**2.)**0.5 for s in np.split(errs, split_points)])
        # binned_rels = binned_errs / binned_vals
        # binned_rels[np.isnan(binned_rels)] = 0.

        # identify bins that are below y_low / above y_high
        low_bins = np.argwhere(binned_vals < y_low).reshape(-1)
        high_bins = np.argwhere(binned_vals >= y_high).reshape(-1)

        # stop when there are no low bins
        if not len(low_bins):
            break

        # when there are no high bins with size > 1 to extract bin contents from
        # reduce the number of bins and start over
        high_bin_sizes = np.array([sizes[b] for b in high_bins])
        if not len(high_bins) or (high_bin_sizes == 1).all():
            n_bins -= 1
            if n_bins >= n_min_bins:
                print("reducing n_bins to {}".format(n_bins))
                sizes = None
                continue
            else:
                return raise_min_bins()

        # find the low bin with the smallest content, select the outermost in case of multiple ones
        smallest_low_bins = np.argwhere(binned_vals == binned_vals.min()).reshape(-1)
        dst_bin = sorted(smallest_low_bins, key=lambda i: abs(i - 0.5 * (n_bins - 1)))[-1]

        # find the widest high bin, select the one with the largest content in case of multiple ones
        widest_high_bins = high_bins[high_bin_sizes == high_bin_sizes.max()]
        src_bin = sorted(widest_high_bins, key=lambda i: binned_vals[i])[-1]

        # reduce the size of the widest high bin and increase that of the smallest low bin
        sizes[src_bin] -= 1
        sizes[dst_bin] += 1

    # convert sizes back into optimized edges
    edges = sizes_to_edges(sizes)

    # call the callback one last time
    if callable(callback):
        callback(edges)

    return edges


def wquant(x, w, q):
    """
    x: bin centers
    w: bin heights (bin content)
    q: quantiles
    """
    assert x.shape == w.shape
    assert x.ndim == 1
    assert q.ndim == 1
    assert np.all((0 <= q) & (q <= 1))
    i = np.argsort(x)
    x = x[i]
    w = w[i]
    c = np.cumsum(w)
    inter = scipy.interpolate.interp1d(c, x, kind="nearest")
    return inter(q[1:-1] * c[-1])


@njit
def bbt(arr, var=np.array([]), thresh=np.array([10, 10, 10, 5, 5, 5])):
    # bbt := binning-by-thresholds
    #
    # This rebinning strategy starts from the most right bin
    # and rebins the histogram according to certain bincontent
    # thresholds. Thus is it safely applicable to variables, which
    # behave like a DNN score: bkg towards 0, sig towards 1.
    #
    # if variance (``) is given the theshold conditions goes from
    #   bincontent >= threshold
    # to
    #   bincontent - sqrt(variance) >= threshold
    #
    #
    # usage:
    #   - bbt(bkgarr, thresh=np.array([10, 10, 5, 5, 5]))
    #
    # binning strategy:
    #
    #
    # ^
    # |
    # | Rest   10 10  5  5  5
    # |        __ __
    # |       |  |  |
    # |       |  |  |__ __ __
    # |       |  |  |  |  |  |
    # |.......|  |  |  |  |  |
    # |----------------------->
    # 0                       1
    #
    # Caution: `Rest` bincontent threshold is not enforced and needs to be checked/caught by user!
    #
    # jit-comparison:
    # - no: `224 µs ± 3.52 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)`
    # - njit: `1.39 µs ± 34.8 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)`
    # => ~160x faster
    if var.size == 0:
        var = np.zeros_like(arr)
    assert arr.ndim == 1
    assert thresh.ndim == 1
    assert arr.shape == var.shape
    assert np.all(thresh >= 0)
    la = len(arr)
    lt = len(thresh)
    rev = arr[::-1]
    revvar = var[::-1]
    sum = 0.0
    unc = 0.0
    idx = np.zeros(lt)
    tidx = -1
    for i in range(la):
        if tidx < -lt:
            break
        sum += rev[i]
        unc += np.sqrt(revvar[i])
        if (sum - unc) >= thresh[tidx]:
            idx[tidx] = la - 1 - i
            sum = 0.0
            unc = 0.0
            tidx -= 1
    return idx


@njit
def thresh_rebin(thresh, val, var=None, extra=None, rsut=np.inf):
    """
    rsut := relative stat. unc. threshold
    """
    assert len(thresh) < len(val)
    assert np.all(thresh >= 0)
    assert rsut >= 0.0

    if var is None:
        var = np.zeros_like(val)
    if extra is None:
        extra = np.expand_dims(val, -1)
    assert val.shape == var.shape == extra.shape[:1]
    assert val.ndim == var.ndim == 1
    assert extra.ndim == 2

    sum_val = 0
    sum_var = 0
    sum_extra = np.zeros_like(extra[0])

    val = val[::-1]
    var = var[::-1]
    extra = extra[::-1]

    la = len(val)
    lt = len(thresh)
    idx = np.zeros(lt, dtype=np.intp)
    tidx = -1
    for i in range(la):
        if tidx < -lt:
            break
        sum_val += val[i]
        sum_var += var[i]
        sum_extra += extra[i]
        unc = np.sqrt(sum_var)

        if (
            (sum_val - unc) >= thresh[tidx]
            and np.all(sum_extra)
            and np.abs((unc / sum_val)) <= rsut
        ):
            idx[tidx] = la - 1 - i
            sum_val = 0.0
            sum_var = 0.0
            sum_extra[:] = 0.0
            tidx -= 1
    return idx[1 + tidx :]


@njit
def thresh_rebin2(thresh, data, fallback):
    """
    thresh: floaty[T] >= 0
    data: (value: floaty, variance: floaty)[P, N]
    fallback: floaty[P]

    note: bins are built "from left to right"
    """
    assert thresh.ndim == 1
    assert data.ndim == 2
    assert fallback.ndim == 1
    assert thresh.shape[0] > 0
    assert data.shape[0] == fallback.shape[0]

    acc_val = np.zeros(data.shape[:1])
    acc_var = np.zeros(data.shape[:1])
    idx = np.zeros(thresh.shape, dtype=np.intp)
    idx_num = 0
    for bin_curr in range(data.shape[1]):
        acc_val += data["value"][:, bin_curr]
        acc_var += data["variance"][:, bin_curr]
        tot_val = np.sum(acc_val)
        tot_var = np.sum(np.where(acc_var, acc_var, fallback))
        if tot_val - np.sqrt(tot_var) > thresh[idx_num]:
            idx[idx_num] = bin_curr
            idx_num += 1
            if len(thresh) > idx_num:
                acc_val[:] = 0
                acc_var[:] = 0
            else:
                break
    return idx[:idx_num]

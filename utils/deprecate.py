from functools import wraps
from importlib import import_module
from operator import attrgetter
from warnings import warn


def deprecated_import(obj=None, module=None, attrpath=None, old_name=None):
    if isinstance(obj, str):
        assert module is None and attrpath is None
        module, attrpath = obj.rsplit(":" if ":" in obj else ".", 1)
        obj = attrgetter(attrpath)(import_module(module))
    if module is None:
        module = obj.__module__
    if attrpath is None:
        attrpath = obj.__qualname__
    name = attrpath.rsplit(".", 1)[-1]
    if old_name is None:
        old_name = name
    if callable(obj) and not isinstance(obj, type):
        if name == attrpath:
            new_loc = f"from {module} import {name}"
        else:
            lead = attrpath.split(".", 1)[0]
            new_loc = f"from {module} import {lead}; {name} = {attrpath}"

        @wraps(obj)
        def wrapper(*args, **kwargs):
            warn(
                f"deprecated import location for {old_name!r}, use {new_loc!r} instead",
                DeprecationWarning,
                stacklevel=2,
            )
            return obj(*args, **kwargs)

        return wrapper
    else:
        raise NotImplementedError(f"object of type {type(obj)} is not supported")

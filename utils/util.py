# coding: utf-8

"""
Some helper functions.
"""

import gc
import json
import logging
import os
import pathlib
import re
import resource
import subprocess
import tempfile
from contextlib import ContextDecorator, contextmanager
from contextvars import ContextVar
from fnmatch import fnmatch
from functools import lru_cache, wraps
from hashlib import sha1
from math import inf
from operator import itemgetter
from os.path import commonprefix
from pathlib import Path
from time import time
from typing import Dict, List
from weakref import finalize

import law
import numpy as np
import requests
import scipy
from filelock import FileLock
from law.util import colored, human_bytes
from numba import njit
from rich.markup import escape
from rich.text import Text
from rich.tree import Tree

from utils.deprecate import deprecated_import


def ulimit(*, lower=False, **kwargs):
    for key, value in kwargs.items():
        res = getattr(resource, "RLIMIT_%s" % key.upper())
        soft, hard = [inf if v == resource.RLIM_INFINITY else v for v in resource.getrlimit(res)]
        if value in ("hard", max):
            value = hard
        else:
            value = min(value, hard)
        if not lower:
            value = max(value, soft)
        resource.setrlimit(
            res, tuple(resource.RLIM_INFINITY if v == inf else v for v in (value, hard))
        )


class MaxRSSWatch(ContextDecorator):
    last = ContextVar("MaxRSSLast", default=None)

    def __init__(self, prefix="MaxRSS", suffix="", thresh=0, spike=0, gc=False):
        self.prefix = prefix
        self.suffix = suffix
        self.thresh = thresh
        self.spike = spike
        self.gc = gc

    @property
    def curr(self):
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    def reset(self):
        if self.gc is True:
            gc.collect()
        elif self.gc not in (False, None):
            gc.collect(self.gc)
        with open("/proc/self/clear_refs", "w") as f:
            f.write("5\n")

    def bump(self):
        self.exit = max(self.exit, self.curr)

    def print(self, *args):
        logging.getLogger("distributed.worker").info(" ".join(args))

    def __enter__(self):
        if self.last.get():
            self.last.get().bump()
        self._token = self.last.set(self)
        self.reset()
        self.enter = self.exit = self.curr
        self.time0 = time()

    def __exit__(self, *exc):
        dtime = time() - self.time0
        self.bump()
        if self.exit > self.thresh and (delta := self.exit - self.enter) > self.spike:
            self.print(
                *filter(
                    None,
                    [
                        self.prefix,
                        f"{self.enter/1024:6.1f}MB -> {self.exit/1024:6.1f}",
                        f"({delta/1024:+6.1f}MB, {dtime:.3f}s)",
                        self.suffix,
                    ],
                )
            )
        self.last.reset(self._token)
        if self.last.get():
            self.last.get().reset()


def ccDump(name, stat):
    n, t = stat
    if n:
        print(f"CC {name:20s}: {n:10d} * {1e3*t/n:8.3f}ms = {t:8.1f}s")


def cc(func):
    stat = [0, 0.0]

    finalize(func, ccDump, getattr(func, "__qualname__", func.__name__), stat)

    @wraps(func)
    def wrapper(*args, **kwargs):
        stat[0] += 1
        t0 = time()
        try:
            r = func(*args, **kwargs)
        finally:
            stat[1] += time() - t0
        return r

    return wrapper


def calc_checksum(*paths, **kwargs):
    exclude = law.util.make_list(kwargs.get("exclude", ["*.pyc", "*.git*"]))
    exclude = " ".join("! -path '{}'".format(p) for p in exclude)

    sums = []
    for path in paths:
        path = os.path.expandvars(os.path.expanduser(path))
        if os.path.isfile(path):
            cmd = 'sha1sum "{}"'.format(path)
        elif os.path.isdir(path):
            cmd = (
                'files="$( find "{}" -type f {} -print | sort -z )"; '
                "(for f in $files; do sha1sum $f; done) | sha1sum".format(path, exclude)
            )
        else:
            raise IOError("file or directory '{}' does not exist".format(path))

        code, out, _ = law.util.interruptable_popen(
            cmd, stdout=subprocess.PIPE, shell=True, executable="/bin/bash"
        )
        if code != 0:
            raise Exception("checksum calculation failed")

        sums.append(out.strip().split(" ")[0])

    if len(sums) == 1:
        return sums[0]
    else:
        cmd = 'echo "{}" | sha1sum'.format(",".join(sums))
        code, out, _ = law.util.interruptable_popen(
            cmd, stdout=subprocess.PIPE, shell=True, executable="/bin/bash"
        )
        if code != 0:
            raise Exception("checksum combination failed")

        return out.strip().split(" ")[0]


class DotDict(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __getattr__(self, attr):
        return self.get(attr)

    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __getstate__(self):
        return self

    def __setstate__(self, state):
        self.update(state)
        self.__dict__ = self

    def __dir__(self):
        return sorted(dict.__dict__.keys() + self.keys())


def iter_chunks(iterable, chunksize):
    from itertools import zip_longest

    assert 0 < chunksize
    assert chunksize == int(chunksize)
    null = object()
    for chunk in zip_longest(*([iter(iterable)] * chunksize), fillvalue=null):
        yield [v for v in chunk if v is not null]


def parametrized(dec):
    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)

        return repl

    return layer


def linsplit(n, l):
    import math

    lengths = []
    avg = float(n) / l
    rest = 0.0
    for i in range(l):
        length = avg + rest
        length_int = int(math.floor(length))
        rest = length - length_int
        lengths.append(length_int)
    return lengths


def round_significant(number, sig=1):
    from math import floor, log10

    return round(number, sig - int(floor(log10(abs(number)))) - 1)


optimize_binning = deprecated_import("utils.binning.optimize_binning")


class NumpyEncoder(json.JSONEncoder):
    """Custom encoder for numpy data types"""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)

        elif isinstance(obj, np.floating):
            return float(obj)

        elif isinstance(obj, (complex, np.complexfloating)):
            return {"real": obj.real, "imag": obj.imag}

        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()

        elif isinstance(obj, (np.bool_)):
            return bool(obj)

        elif isinstance(obj, (np.void)):
            return None

        return json.JSONEncoder.default(self, obj)


wquant = deprecated_import("utils.binning.wquant")


def view_sdtype_flat(array):
    """
    Returns a view of the input, with its structured dtype flattend.
    """
    return array.view(_flatten_sdtype(array.dtype))


@lru_cache(32)
def _flatten_sdtype(dtype: np.dtype) -> np.dtype:
    n = len(dtype.fields)
    assert n, "not a structured dtype"
    for i, (t, o) in enumerate(dtype.fields.values()):
        if i == 0:
            dt = t
        else:
            assert dt == t, "inconsistent field dtype"
        assert dt.itemsize * i == o, "inconsistent field offests"
    assert dt.itemsize * n == dtype.itemsize, "inconsistent total size"
    return np.dtype((dt, (n,)))


def ro_view(array):
    """Returns a read-only view of the given numpy array."""
    ret = array.view()
    ret.flags.writeable = False
    return ret


bbt = deprecated_import("utils.binning.bbt")


class AsteriskDict(dict):
    """
    Dictionary which supports globbing keys with asterisks
    Getitem returns first matching occurance

    Example:
    d = AsteriskDict({
        "asd": 0,
        "de*": 1,
    })
    print(d["def"])  # 1
    """

    def __getitem__(self, key):
        return super().__getitem__(self.lookup(key))

    def __contains__(self, key):
        try:
            self.lookup(key)
        except KeyError:
            return False
        else:
            return True

    def lookup(self, key):
        for pat in self.keys():
            if fnmatch(key, pat):
                return pat
        raise KeyError(key)


def require_create_issue(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if not self.create_issue and self.create_issue is not None:
            return
        return func(self, *args, **kwargs)

    return wrapper


class GitlabIssue:
    """
    usage in task run method:

    class Task(law.Task, CreateIssueMixin):
        def run(self):
            GI = GitlabIssue(self, opts={"collapsibles": r".*.png$", "accepts": (".pdf", ".png)})
            # single output
            uploads = GI.upload_to_issue(self.output().path)
            # multiple outputs
            # uploads = []
            # for path in paths:
            #     uploads += GI.upload_to_issue(path)
            GI.post_to_issue(uploads="\n".join(uploads), message="Hello World")

    opts:
        - important_params: task parameters/attributes to itemize in issue
        - collapsibles: regex to match output files, which are to large to show by default
        - issue_title: title of the gitlab issue
        - accepts: file formats to accept for upload
    """

    project_id = 31958
    apiurl = f"https://git.rwth-aachen.de/api/v4/projects/{project_id}/issues"

    session = requests.Session()
    session.headers.update({"PRIVATE-TOKEN": os.environ["DHA_REPO_TOKEN"]})

    def __init__(self, task: law.Task, opts: Dict = {}) -> None:
        from tasks.base import AnalysisTask, CombinationTask
        from tasks.mixins import ModelMixin

        self.task = task
        self.collapsibles = re.compile(opts.get("collapsibles", r"a^"))
        self.accepts = opts.get("accepts", (".pdf", ".png"))
        assert isinstance(self.accepts, tuple)
        self.important_params = opts.get("important_params", [])
        self.version = getattr(task, "version", None)
        self.model_version = getattr(task, "model_version", None)
        self.create_issue = getattr(task, "create_issue", None)

        # Issue Title
        # get prefix title
        if isinstance(task, AnalysisTask):
            self.prefix_title = getattr(task, "analysis_choice")
        elif isinstance(task, CombinationTask):
            self.prefix_title = "+".join(getattr(task, "analyses"))
        else:
            # common task
            self.prefix_title = "Common"

        # get args
        self.title_args = [f"Version: {self.version}"]
        if isinstance(task, ModelMixin):
            self.title_args += [f"Model Version: {self.model_version}"]

        if issue_title := opts.get("issue_title", False):
            self.issue_title = issue_title
        else:
            self.issue_title = f"{self.prefix_title}"
            if self.title_args:
                self.issue_title += f" ({', '.join(self.title_args)})"

        # common repo issues
        self.breadcrumb = task.local_target(".gitlab_issue.json")
        self.lock = FileLock(self.breadcrumb.path + ".lock")

        lines = [f"<h3 title='{task.task_id}'>{task.__class__.__name__}</h3>\n"]
        lines.extend(f"* {param}: {getattr(task, param, None)}" for param in self.important_params)
        self.header = "\n".join(lines).strip()

    def _json(self, url, **kwargs):
        getty = all(kwargs.get(p, None) is None for p in ["data", "files"])
        kwargs.setdefault("method", "GET" if getty else "POST")
        resp = self.session.request(url=url, **kwargs)
        resp.raise_for_status()
        return resp.json()

    @property
    def issue(self) -> Dict:
        with self.lock:
            if self.breadcrumb.exists():
                return self.breadcrumb.load()["issue"]

            ihash = sha1(self.issue_title.encode("utf8")).hexdigest()[:20]
            with FileLock(Path(tempfile.gettempdir()) / f"gitlab_issue.{ihash}.lock"):
                title = self.issue_title
                json = self._json(
                    self.apiurl, params={"search": title, "in": "title", "state": "opened"}
                )
                if len(json):
                    [issue] = (matches := [j for j in json if j["title"] == title])
                    assert (
                        len(matches) == 1
                    ), f"Found multiple matching issues with the same title: {[m['_links']['self'] for m in matches]}"
                else:
                    issue = self._json(
                        self.apiurl,
                        data={
                            "title": title,
                            "labels": "create-issue (automatic)",
                        },
                    )
                self.breadcrumb.dump({"issue": issue})

        # try automatic issue linking
        if m := re.match(r"^issue_(\d+)_", self.model_version):
            self.session.post(
                f"{issue['_links']['self']}/links",
                data={
                    "target_project_id": str(self.project_id),
                    "target_issue_iid": m.group(1),
                    "link_type": "relates_to",
                },
            )

        return issue

    @contextmanager
    def info_update(self):
        with self.lock:
            info = self.breadcrumb.load()
            yield info
            self.breadcrumb.dump(info)

    @require_create_issue
    def post_to_issue(self, uploads: str = "", message: str = "", update: bool = True) -> None:
        note_url = self.issue["_links"]["notes"]

        body = self.header
        if uploads:
            if not isinstance(uploads, str):
                uploads = "\n".join(map(self.upload_to_issue, uploads))
            body += f"\n#### Files:\n\n{uploads}"
        if message:
            body += f"\n#### Message:\n\n{message}"

        with self.info_update() as info:
            task2note = info.setdefault("task2note", {})

            if update and (noteId := task2note.get(self.task.task_id, None)):
                note = self._json(f"{note_url}/{noteId}", data={"body": body}, method="PUT")
            else:
                note = self._json(note_url, data={"body": body})
            task2note[self.task.task_id] = note["id"]

    @require_create_issue
    def upload_to_issue(self, file_path: str) -> str:
        self.task.publish_message(f"uploading file to issue {self.issue_title}: '{file_path}'")

        with open(file_path, "rb") as content:
            upload = self._json(
                self.apiurl.replace("issues", "uploads"),
                files={"file": content},
            )

        with self.info_update() as info:
            files = info.setdefault("files", [])
            if upload["full_path"] not in files:
                files.append(upload["full_path"])

        markdown = upload["markdown"]

        if self.collapsibles.match(file_path):
            summary = f"<summary>{Path(file_path).name}: Click to expand!</summary>"
            markdown = f"<details>{summary}\n{markdown}</details>"

        return markdown


@law.decorator.factory(accept_generator=True)
def outputs_to_issue(fn, opts, task, *args, **kwargs):
    """
    Example usage:

    @outputs_to_issue(
        important_params=["year", "unblind", "version"],
        collapsibles=r".*.png$",
        issue_title="Test Issue",
    )
    def run(self):
        ...

    opts:
        - important_params: task parameters/attributes to itemize in issue
        - collapsibles: regex to match output files, which are to large to show by default
        - issue_title: title of the gitlab issue
        - accepts: file formats to accept for upload
    """

    def before_call():
        return None

    def call(state):
        return fn(task, *args, **kwargs)

    def after_call(state):
        GI = GitlabIssue(task=task, opts=opts)
        if not GI.create_issue or GI.create_issue is None:
            return

        # collect all paths to upload to issue
        paths = []
        outputs = law.util.flatten(task.output())
        while outputs:
            output = outputs.pop(0)
            if isinstance(output, law.TargetCollection):
                outputs.extend(output._flat_target_list)
                continue
            if not getattr(output, "path", None):
                continue
            if output.path.endswith(GI.accepts) and output.path not in paths:
                paths.append(output.path)

        # upload and post paths
        GI.post_to_issue(uploads=paths, update=True)

    return before_call, call, after_call


def defix(items, *, pre=True, post=True):
    if pre is True:
        pre = len(commonprefix(items))
    if post is True:
        post = len(commonprefix([item[::-1] for item in items]))
    if pre or post:
        return type(items)(map(itemgetter(slice(pre, -post or None)), items))
    return items


def walk_directory(directory: pathlib.Path, tree: Tree) -> None:
    """
    Recursively build a Tree with directory contents.

    Example:
        from rich.console import Console

        console = Console()
        base = Path(__file__)
        tree = Tree(
            f":open_file_folder: [link file://{base}]{base}",
            guide_style="bold bright_blue",
        )
        walk_directory(pathlib.Path(base), tree)
        console.print(tree)
    """
    # Sort dirs first then by filename
    paths = sorted(
        pathlib.Path(directory).iterdir(),
        key=lambda path: (path.is_file(), path.name.lower()),
    )
    for path in paths:
        # Remove hidden files
        if path.name.startswith("."):
            continue
        if path.is_dir():
            style = "dim" if path.name.startswith("__") else ""
            branch = tree.add(
                f"[bold magenta]:open_file_folder: [link file://{path}]{escape(path.name)}",
                style=style,
                guide_style=style,
            )
            walk_directory(path, branch)
        else:
            text_filename = Text(path.name, "green")
            text_filename.highlight_regex(r"\..*$", "bold red")
            text_filename.stylize(f"link file://{path}")
            file_size = path.stat().st_size
            text_filename.append(f" ({human_bytes(file_size, 'kB', fmt=True)})", "blue")
            if path.suffix == ".py":
                icon = "🐍  "
            elif path.suffix in (".pdf", ".png"):
                icon = "🖼  "
            else:
                icon = "📄  "
            tree.add(Text(icon) + text_filename)


def pscan_vispa(directory: str, regex: str) -> str:
    msg = ("For PScan on VISPA:  ",)
    msg += (f"\t- Directory: `{directory}`  ",)
    msg += (f"\t- Regex: `{regex}`  ",)
    return "\n".join(msg)

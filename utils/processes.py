# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import List


@dataclass(unsafe_hash=True)
class Process:
    name: str
    id: int

    @property
    def is_signal(self) -> bool:
        return self.id <= 0


class classproperty(object):
    def __init__(self, func):
        self._func = func

    def __get__(self, owner_self, owner_cls):
        return self._func(owner_cls)


class ProcessesMixin:
    @classproperty
    def _processes(cls):
        raise NotImplementedError()

    @classproperty
    def _sorted_processes(cls):
        return sorted(cls._processes, key=lambda x: x.id)

    @classproperty
    def background_processes(cls) -> List[str]:
        return [p.name for p in cls._sorted_processes if not p.is_signal]

    @classproperty
    def signal_processes(cls) -> List[str]:
        return [p.name for p in cls._sorted_processes if p.is_signal]

    @classproperty
    def processes(cls) -> List[str]:
        return [p.name for p in cls._sorted_processes]

    @classproperty
    def processes_ids(cls) -> List[int]:
        return [p.id for p in cls._sorted_processes]

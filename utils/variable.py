# coding: utf-8

import re
from dataclasses import dataclass
from typing import List, Tuple, Optional, Dict, Union, Any


class IsValid:
    def is_valid(self, shift: str, category: str) -> bool:
        return any(re.match(c, category) for c in self.categories) & any(re.match(s, shift) for s in self.shifts)  # fmt: skip


@dataclass
class Variable(IsValid):
    name: str
    expression: str
    binning: List
    unit: Optional[str] = None
    x_title: Optional[str] = None
    env: "Environment" = None

    def __post_init__(self) -> None:
        if self.x_title is None:
            self.x_title = self.name
        if self.env is None:
            self.env = Environment()
        assert isinstance(self.env, Environment)
        # forward shift and category patterns from env to self
        self.shifts = self.env.shifts
        self.categories = self.env.categories

    @classmethod
    def from_config(cls, cfg, name: str, env: "Environment" = None) -> "Variable":
        cfg_var = cfg.get_variable(name)
        return cls(
            name=cfg_var.name,
            expression=cfg_var.expression,
            binning=cfg_var.binning,
            unit=cfg_var.unit,
            x_title=cfg_var.x_title,
            env=env,
        )


class SettingsDict(dict):
    patterns = {"shifts", "categories"}

    def __or__(self, other: "SettingsDict") -> "SettingsDict":
        if not isinstance(other, SettingsDict):
            other = SettingsDict(other)
        cenv = self.copy()
        for k in other.keys():
            # merge patterns with parent_env
            if k in self.patterns:
                cenv[k] = list(set(self[k]) | set(other[k]))
            # forward parent kwargs
            else:
                cenv[k] = other[k]
        return cenv

    def __ior__(self, other: "SettingsDict") -> "SettingsDict":
        if not isinstance(other, SettingsDict):
            other = SettingsDict(other)
        self = self | other
        return self


class Environment(IsValid):
    cache: Dict[Tuple, Dict[str, Variable]] = {}

    def __init__(
        self,
        name: str = "default",
        shifts: List[str] = [r".*"],
        categories: List[str] = [r".*"],
        parent_env: "Environment" = None,
        **kwargs: Any,
    ):
        self.name = name
        self.shifts = shifts
        self.categories = categories
        self.settings = SettingsDict(shifts=self.shifts, categories=self.categories)
        self.parent_env = parent_env
        if self.parent_env is not None:
            self.settings |= self.parent_env.settings
        self.settings.update(kwargs)

    @staticmethod
    def _pretty_pattern_key(name: str, patterns: List[str]) -> str:
        return f"{name}: ({'|'.join(sorted(patterns))})"

    @classmethod
    def get_cache_key(cls, variable: Union[str, Variable]) -> Union[Tuple, bool]:
        if isinstance(variable, str):
            variable = Variable.cache[variable]
        for key, variables in cls.cache.items():
            if variable in variables:
                return key
        return False

    @classmethod
    def same_env_as(cls, variable: Union[str, Variable]) -> List[Variable]:
        return cls.cache.get(cls.get_cache_key(variable), [])

    def _create_key(self) -> Tuple:
        return (
            self.name,
            self._pretty_pattern_key(name="Shifts", patterns=self.shifts),
            self._pretty_pattern_key(name="Categories", patterns=self.categories),
        )

    def register(self, variables: List[Variable]) -> None:
        self.cache.update({self._create_key(): {v.name: v for v in variables}})

    @property
    def this_cache(self):
        return self.cache[self._create_key()]

    def __enter__(self) -> Dict:
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback) -> None:
        self.settings.clear()


if __name__ == "__main__":

    with Environment() as env1:
        env1.register(
            [
                Variable(
                    name=f"X_{i}",
                    expression="x",
                    binning=[1, 0, 1],
                    env=env1,
                )
                for i in range(10)
            ]
        )
    with Environment(
        name="Low-level", shifts=["^nominal$"], categories=[r"foo*", r".+bar$"]
    ) as env2:
        env2.register(
            [
                Variable(
                    name=f"Y_{i}",
                    expression="y",
                    binning=[1, 0, 1],
                    env=env2,
                )
                for i in range(10)
            ]
        )

    assert env1.is_valid(shift="foo", category="bar") == True
    if env1.is_valid(shift="foo", category="bar"):
        for variable in env1.this_cache.values():
            assert variable.is_valid(shift="foo", category="bar") == True
    assert env2.is_valid(shift="nominal", category="foobar") == True
    assert env2.is_valid(shift="JESUp", category="foobar") == False

    with Environment(shifts=[r"^nominal$"]) as env3:
        print(env3.settings)
        ">> {'shifts': ['^nominal$'], 'categories': ['.*']}"
        with Environment(shifts=[r"^JES*", r"^JER*"], parent_env=env3) as env4:
            print(env4.settings)
            ">> {'shifts': ['^JES*', '^JER*', '^nominal$'], 'categories': ['.*']}"
            with Environment(
                shifts=[], categories=[r".+fr*", r".+prompt*"], parent_env=env4
            ) as env5:
                print(env5.settings)
                ">> {'shifts': ['^JER*', '^nominal$', '^JES*'], 'categories': ['.*', '.+prompt*', '.+fr*']}"
                with Environment() as env6:
                    print(env6.settings)
                    ">> {'shifts': ['.*'], 'categories': ['.*']}"

    from IPython import embed

    embed()

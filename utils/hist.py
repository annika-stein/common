# coding: utf-8

import itertools

import numpy as np
import scipy
import hist


def reduce_along_axis(func, axis, h):
    axes = np.array(h.axes, dtype=object)
    axes = np.delete(axes, axis)
    hnew = hist.Hist(*axes)
    hnew.view()[:] = np.apply_along_axis(func, axis, h.view())
    return hnew


def _kstest(arr):
    with np.errstate(invalid="ignore"):
        return scipy.stats.kstest(arr[0, :], arr[1, :]).statistic
        return scipy.stats.chisquare(arr[0, :], arr[1, :]).statistic


def kstest(h1, h2, compare_axis, feature_axis):
    assert feature_axis == -1, "Other axes not implemented yet"
    axes1 = np.array(h1.axes, dtype=object)
    axes2 = np.array(h2.axes, dtype=object)
    # currently only works for StrCategory
    compared_axis = [
        f"{a}_{b}" for (a, b) in itertools.product(axes1[compare_axis], axes2[compare_axis])
    ]
    new_axes = np.copy(axes1)
    new_axes[compare_axis] = type(axes1[compare_axis])(compared_axis)
    new_axes = np.delete(new_axes, feature_axis)
    hnew = hist.Hist(*new_axes)

    a1, a2 = h1.view(), h2.view()

    c, d = np.broadcast_arrays(
        np.expand_dims(a1, compare_axis), np.expand_dims(a2, compare_axis + 1)
    )

    s = np.stack([c, d], axis=-2)
    # reshape
    o = np.array(s.shape)
    o[compare_axis] = o[compare_axis] * o[compare_axis + 1]
    o = np.delete(o, compare_axis + 1)
    s = np.reshape(s, o)

    # perform test
    kstest = np.vectorize(_kstest, signature="(2,n)->()")
    hnew.view()[:] = kstest(s["value"])
    return hnew

# -*- coding: utf-8 -*-

from functools import partial
from typing import FrozenSet, Iterable, Optional, Union

import numpy as np
from hist import Hist
from tqdm.auto import tqdm

from utils.bh5 import Histogram as Hist5

# Available top systematics
TopSystematics: FrozenSet = frozenset(
    {
        "UnderlyingEvent",
        "TopMass",
        "GluonMove",
        "QCDbased",
        "erdON",
        "MEPSMatchingScale",
    }
)


def topvariation(
    hist: Hist,
    old: Union[str, Iterable[str]],
    new: str,
    norm: bool = True,
    scale: float = 1.0,
) -> Hist:
    """
    hist: Hist
    old: str or Iterable[str]
    new: str
    norm: bool
    scale: float

    Returns:
    Hist including `new` Up/down variations from `old`

    Can calculate the following variations:

    UnderlyingEvent:
        Directly inferred from up/down variations of the datasets

    TopMass (3 GeV shift):
        Up = 175.5 GeV
        Down = 169.5 GeV

    ColorReconnection:
        for cr in GluonMove, QCDbased, erdON:
            Up = nominal + abs(nominal - cr)
            Down = nominal - abs(nominal - cr)

    MEPSMatchingScale:
        Directly inferred from up/down variations of the datasets
    """
    assert new in TopSystematics

    hnew = Hist5.regrow(
        hist,
        {"systematic": [new + "Up", new + "Down"]},
        copy=False,
    )
    newUpIdx = hnew.axes["systematic"].index(new + "Up")
    newDownIdx = hnew.axes["systematic"].index(new + "Down")

    hv = hist.view(True)
    ttIdx = hist.axes["process"].index("tt")

    # assume: old"Up" -> new"Up", old"Down" -> new"Down"
    if isinstance(old, str):
        upIdx = hist.axes["systematic"].index(old + "Up")
        downIdx = hist.axes["systematic"].index(old + "Down")
        fun = lambda x: x
    # assume envelope needs to be build
    else:
        upIdx = downIdx = hist.axes["systematic"].index(old)
        fun = partial(np.maximum.reduce, axis=1)

    nomIdx = hist.axes["systematic"].index("nominal")

    tt = hv[ttIdx, ...]["value"]
    nominal = tt[:, nomIdx, :]
    nomIntegral = np.sum(nominal, axis=-1)

    up = fun(tt[:, upIdx, :])
    down = fun(tt[:, downIdx, :])

    if norm:
        SFup = nomIntegral / np.sum(up, axis=-1)
        SFdown = nomIntegral / np.sum(down, axis=-1)
    else:
        SFup = SFdown = np.ones_like(nomIntegral)

    up *= SFup[:, None]
    down *= SFdown[:, None]

    # Here, we only need to set sumw properly. sumw2 is not needed as these are shifts anyway...

    # Up variation
    hnew.view(True)[ttIdx, :, newUpIdx, :]["value"] = nominal + scale * np.abs(nominal - up)
    # Down variation
    hnew.view(True)[ttIdx, :, newDownIdx, :]["value"] = nominal - scale * np.abs(nominal - down)
    return hnew


def topvariations(hist: Hist, tqdm: Optional[tqdm] = None, norm: bool = True) -> Hist:
    for kwargs in [
        # dataset name(s), systematic name, scale factor
        dict(old="TuneCP5", new="UnderlyingEvent", scale=1.0),
        dict(old="mtop", new="TopMass", scale=1.0 / 3.0),  # 3 GeV shift -> 1 GeV shift
        dict(old=["GluonMove"], new="GluonMove", scale=1.0),
        dict(old=["QCDbased"], new="QCDbased", scale=1.0),
        dict(old=["erdON"], new="erdON", scale=1.0),
        dict(old="HDamp", new="MEPSMatchingScale", scale=1.0),
    ]:
        if tqdm is not None:
            tqdm.set_postfix(
                step=f"Top variation: {kwargs['old']} -> {kwargs['new']} (scale={kwargs['scale']:.2f})"
            )
        hist += topvariation(hist=hist, norm=norm, **kwargs)
    return hist

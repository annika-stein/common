# coding: utf-8

import law
import luigi
import luigi.util
import numpy as np
from rich.console import Console
from rich.table import Table

from tasks.base import AnalysisCampaignTask
from tasks.combine import FitDiagnosticsCombined
from tasks.mixins import ModelMixin, PGroupMixin, RecipeMixin
from tasks.prefit import PrepareFitDiagnostics, decycle


class YieldTables(PrepareFitDiagnostics, ModelMixin, RecipeMixin, AnalysisCampaignTask):
    unblind = FitDiagnosticsCombined.unblind
    norm_by_lumi = luigi.BoolParameter()
    style_AN = luigi.BoolParameter()

    def requires(self):
        return FitDiagnosticsCombined.req(
            self,
            analyses=(self.analysis_choice,),
            years=(self.year,),
        )

    def output(self):
        name = "table"
        if self.style_AN:
            name += "_AN"
        if self.norm_by_lumi:
            name += "_norm"
        return {
            "json": self.local_target(f"{name}.json"),
            "tex": self.local_target(f"{name}.tex"),
        }

    def bkgs(self, h):
        return list(set(h.axes["process"]) & set(self.StatModel.background_processes))

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        from collections import defaultdict

        import hist
        import uproot3 as uproot
        from rich import box
        from rich.console import Console
        from rich.table import Table
        from tqdm.auto import tqdm

        get = self.analysis_inst.processes.get

        console = Console()

        with uproot.open(self.input().path) as f:
            work = [
                (decycle(shape), decycle(cat), None)
                for shape, d in f.items(
                    filtername=lambda x: x.startswith(
                        b"shapes_" if self.unblind else b"shapes_prefit"
                    )
                )
                for cat, cls in d.classnames()
                if cls == "TDirectory"
            ]

        table = defaultdict(dict)
        rtable = Table(show_footer=True, box=box.SIMPLE)
        rtable.add_column("Process")

        def _prettify(label: str) -> str:
            if " - " in label:
                labels = label.split(" - ")
                assert len(labels) == 2, f"{label}"
                labels[1] = "SM"
                return " - ".join(labels)
            if label.startswith(("THQ", "THW")):
                labels = label.split("_")
                return {"THQ": "tHq (4f)", "THW": "tHW (5f)"}[labels[0]]
            else:
                return label

        # only SM signals
        processes_ = [
            (_prettify(get(proc).label), [proc])
            for proc in self.StatModel.signal_processes
            if proc.startswith(("qqHH_CV_1_C2V_1_kl_1", "ggHH_kl_1_kt_1"))
        ]
        # backgrounds
        processes_ += [
            (_prettify(get(proc).label), [proc]) for proc in self.StatModel.background_processes
        ]

        for w in work:
            c, h = self.unpack_data(w[0], w[1], w[2])

            processes = processes_ + [
                ("$\sum$ backgrounds", self.bkgs(h)),
                ("Data", ["data"]),
            ]

            rtable.add_column(c, justify="right")
            hv = h.view(flow=True)
            ax = h.axes["process"]
            for (process_name, process) in processes:
                if process_name == "Data":
                    v = np.sum(hv[ax.index(process), ...], axis=0)["value"].sum()
                    u = np.sum(hv[ax.index(["data_err"]), ...], axis=0)["variance"].sum() + np.abs(
                        np.sum(hv[ax.index(["data_err"]), ...], axis=0)["value"].sum()
                        - np.sum(hv[ax.index(process), ...], axis=0)["value"].sum()
                    )
                elif all(p not in ax for p in process):
                    v = u = 0.0
                else:
                    spv = np.sum(hv[ax.index(process), ...], axis=0)
                    v = spv["value"].sum()
                    u = spv["variance"].sum()
                if self.norm_by_lumi:
                    v /= np.round(self.campaign_inst.aux["lumi"] / 1000.0, 2)
                    u /= np.round(self.campaign_inst.aux["lumi"] / 1000.0, 2)
                table[c][process_name] = (v, np.sqrt(u))

        self.output()["json"].dump(table)

        procs = defaultdict(list)
        for k, v in table.items():
            for p, pv in v.items():
                procs[p].append(pv)

        for p, vs in procs.items():
            rtable.add_row(p, *[f"{v:.2f} +/- {u:.2f}" for v, u in vs])
        console.print(rtable)

        if self.style_AN:
            cats = {c: "{}".format(c).replace("all ", "") for c in table.keys()}
            latex = "\\subsection{{{year}}}\n".format(year=self.year)
            latex += "\\ref{{tab:yield_table_{year}_{analysis}}}\n".format(
                year=self.year, analysis=self.analysis_choice
            )
            latex += "\\begin{sidewaystable}[h!]\n\\centering\n"
            latex += "\\topcaption{{\\scriptsize{{{analysis}, {year}: {cap}}}}}\n".format(
                analysis=self.analysis_choice, year=self.year, cap=", ".join([*cats.values()])
            )
            latex += "\\scalebox{0.55}{"
            latex += f"\\begin{{tabular}}{{l {' '.join(len(table) * ['r'])}}}\n"
            latex += "\\hline\n"
            latex += f"Process & {' & '.join([*cats.values()])}\\\\\n"
            latex += "\\hline\n"
            for p, vs in procs.items():
                nvs = []
                for v, u in vs:
                    nvs.extend([f"$ {v:.2f} \pm {u:.2f} $"])
                latex += f"{p} & {' & '.join(nvs)}\\\\\n"
            latex += "\\end{{tabular}}}}\n\\label{{tab:yield_table_{year}_{analysis}}}\n\\end{{sidewaystable}}\n".format(
                year=self.year, analysis=self.analysis_choice
            )

        else:
            cats = {
                c: "\multicolumn{2}{c}"
                + "{{{}}}".format(self.analysis_inst.categories.get(c).label_short)
                for c in table.keys()
            }
            latex = "\\documentclass{article}\n"
            latex += "\\usepackage{siunitx,booktabs}\n"
            latex += "\\usepackage[version=4]{mhchem}\n"
            latex += "\\begin{document}\n"
            latex += "\\begin{table}\n\centering\n"
            coltex = " S@{${}\pm{}$}S"
            latex += f"\\begin{{tabular}}{{l {' '.join(len(table) * [coltex])}}}\n"
            latex += "\\toprule \n"
            latex += f"Process & {' & '.join([*cats.values()])}\\\\\n"
            latex += "\\midrule \n"
            for p, vs in procs.items():
                nvs = [f"{a:.2f}" for v in vs for a in v]
                latex += f"{' & '.join([p, *nvs])}\\\\\n"
            latex += "\\bottomrule \n"
            latex += "\\end{tabular}\n\\end{table}\n\\end{document}"

        self.output()["tex"].dump(latex, formatter="text")
        console.print("Copy and Paste:\n")
        console.print(latex)


class OtherYieldTables(law.ExternalTask, AnalysisCampaignTask):
    def output(self):
        sync_yield_paths = self.analysis_inst.aux.get("sync_yield", {}).get("paths", {})
        return {name: law.LocalFileTarget(path) for name, path in sync_yield_paths.items()}


class CompareYieldTables(PGroupMixin, RecipeMixin, AnalysisCampaignTask):
    @property
    def lut(self):
        return self.analysis_inst.aux.get("sync_yield", {}).get("lut", {})

    def requires(self):
        sync_yield_category = self.analysis_inst.aux.get("sync_yield", {}).get("category", None)
        return {
            "we": YieldTables.req(
                self,
                category=sync_yield_category,
            ),
            "other": OtherYieldTables.req(self),
        }

    def run(self):
        console = Console()

        inp = self.input()
        if len(inp["other"]) == 0:
            self.logger.info("I do not have yield tables for comparison.")
        we = inp["we"]["json"].load()
        other = {name: target.load() for name, target in inp["other"].items()}

        for cat, cat_val in sorted(we.items(), key=lambda x: x[0][::-1]):
            table = Table(title=cat)
            table.add_column("Process", justify="right", style="cyan", no_wrap=True)
            table.add_column("RWTH", style="magenta")
            for name, _ in other.items():
                table.add_column(name, justify="right", style="magenta")
                table.add_column(f"{name}/RWTH", justify="right", style="green")

            for proc, proc_val in cat_val.items():
                w = proc_val[0]
                o = []
                for name, yields in other.items():
                    try:
                        _cat = self.lut.get(cat, cat)
                        _proc = self.lut.get(proc, proc)
                        y = yields[_cat][_proc][0]
                        o.append(f"{int(y)}")
                        o.append(f"{int(y / w * 100)}%")
                    except:
                        o += ["", ""]
                table.add_row(proc, f"{int(w)}", *o)
            console.print(table)

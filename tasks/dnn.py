# -*- coding: utf-8 -*-

import uuid

import tensorflow as tf
from luigi import Parameter, BoolParameter, IntParameter, FloatParameter, ChoiceParameter
import law

from tasks.base import BaseTask
from tools.keras import WhereEquals


law.contrib.load("tensorflow")


class ModelLoader:
    def load_model(self, target, *args, **kwargs):
        path = target.path if isinstance(target, law.target.base.Target) else target
        return tf.keras.models.load_model(path, *args, **kwargs)

    def load_latest(self, checkpoints, *args, **kwargs):
        ckpts = [(ep, tar) for (ep, tar) in checkpoints.targets.items() if tar.exists()]
        if len(ckpts) == 0:
            raise FileNotFoundError
        id, latest_model = ckpts[-1]
        self.initial_epoch = id + 1

        return self.load_model(latest_model, *args, **kwargs)


class DNNBase:
    training_version = Parameter(description="dnn training version", default="0")
    lbn_particles = IntParameter(default=0, description="lbn_particles")
    network_architecture = ChoiceParameter(
        choices=["FullyConnected", "ResNet", "DenseNet", "Xception1D"], default="FullyConnected"
    )
    # DenseNet Params
    block_size = IntParameter(default=3, description="block_size")
    # ResNet Params
    jump = IntParameter(default=2, description="jump")
    # Common Params
    layers = IntParameter(default=3, description="layers")
    nodes = IntParameter(default=256, description="nodes")
    activation = ChoiceParameter(choices=["selu", "elu", "softplus", "relu"], default="relu")
    batch_norm = BoolParameter(description="Using batch normalization")
    dropout = FloatParameter(default=0.0, description="The used dropout")
    l2 = FloatParameter(default=0.0, description="The used l2 norm")

    def store_parts(self):
        sp = super().store_parts() + (
            f"training_version_{self.training_version}",
            f"lbn_particles_{self.lbn_particles}",
            f"{self.network_architecture}",
        )
        if self.network_architecture == "DenseNet":
            sp += (f"block_size_{self.block_size}",)
        if self.network_architecture == "ResNet":
            sp += (f"jump_{self.jump}",)
        sp += (
            f"layers_{self.layers}",
            f"nodes_{self.nodes}",
            f"activation_{self.activation}",
            f"batch_norm_{self.batch_norm}",
            f"dropout_{self.dropout}",
            f"l2_{self.l2}",
        )
        return sp


class DNNProcessor(DNNBase, ModelLoader):
    optimizer_classes = {"sgd": tf.keras.optimizers.SGD, "adam": tf.keras.optimizers.Adam}
    optimizer_name = ChoiceParameter(choices=optimizer_classes.keys(), default="adam")
    learning_rate = FloatParameter(default=1e-3, description="The learning rate")

    lossdef = "default"

    def __init__(self, *args, **kwargs):
        self.initial_epoch = 0
        super().__init__(*args, **kwargs)

    def store_parts(self):
        return super().store_parts() + (
            f"lossdef_{self.lossdef}",
            # f"focal_loss_{self.focal_loss}",
            f"optimizer_{self.optimizer_name}",
            f"learning_rate_{self.learning_rate:.2e}",
        )


class StitchedBase(ModelLoader, BaseTask):
    checkpoint_ok = BoolParameter(
        description="Trainings do not need to finish regularly but can also stitch latest checkpoints."
    )

    def output(self):
        return {"model": self.local_directory_target("model")}

    def requires(self):
        raise NotImplementedError

    def run(self):
        if self.checkpoint_ok:
            models = [
                self.load_latest(inp["checkpoints"], compile=False) for inp in self.input().values()
            ]
        else:
            models = [self.load_model(inp["model"], compile=False) for inp in self.input().values()]

        model0_inputs = models[0].inputs

        # generate new inputs
        inputs = [
            tf.keras.Input(inp.shape[1:], name=inp.name.split(":")[0]) for inp in model0_inputs
        ]
        eventnr = tf.keras.Input((1,), dtype=tf.int64, name="eventnr")
        const = len(models)
        model_idx = tf.keras.layers.Lambda(lambda x: x % const)(eventnr)
        output = tf.zeros(
            shape=tf.concat((tf.shape(inputs[0])[:1], tf.shape(models[0](inputs))[1:]), axis=0)
        )
        for i, model in enumerate(models):
            idx = WhereEquals(value=i)(model_idx)
            inp_gathered = [tf.gather_nd(a, idx) for a in inputs]
            out = model(inp_gathered)
            output = tf.tensor_scatter_nd_update(output, idx, out)

        stitched_model = tf.keras.Model(
            inputs + [eventnr], output, name=f"model_{uuid.uuid4().hex[0:10]}"
        )
        stitched_model.save(self.output()["model"].path)


class ModelExportBase(ModelLoader, BaseTask):
    """
    Exports model into multiple different formats which
    can be exported to other analysis groups.
    Can be used on local or workflow targets.
    """

    formats = ["model.pb", "model"]

    def requires(self):
        raise NotImplementedError

    def output(self):
        return {format: self.local_target(f"{format}") for format in self.formats}

    def save(self, model):
        # save 4 different versions of the model for export
        outputs = self.output()
        outputs["model"].parent.touch()
        model.save(outputs["model"].path)
        outputs["model.pb"].dump(model, variables_to_constants=True, formatter="tf_graph")

    def run(self):
        inp = self.input()
        if "collection" in inp:
            model_target = self.input()["collection"].targets[0]["model"]
        else:
            model_target = self.input()["model"]
        model = self.load_model(model_target)
        self.save(model)

# coding: utf-8

import os
import re
import subprocess
from functools import cached_property

import law
import luigi
import order as od

from tasks.base import AnalysisCampaignTask, DHITask, FitDistributionsWrapper
from tasks.group import Rebin
from tasks.mixins import CategoryMixin, ModelMixin, RecipeMixin, VariableMixin
from utils.datacard import Datacard
from utils.util import ulimit


class Transfer:
    transfer = luigi.BoolParameter(description="transfer outputs to eos")


class DatacardProducer(
    Transfer, VariableMixin, CategoryMixin, ModelMixin, RecipeMixin, AnalysisCampaignTask
):
    category = luigi.Parameter(default="mumu_2b")
    verbose = luigi.BoolParameter(significant=False)

    def requires(self):
        return self.StatModel.requires(self)

    def base_requires(self):
        return Rebin.req(self, process_group=("default",))

    def output(self):
        out = {
            "datacard": self.local_target("datacard.txt"),
            "shapes": self.local_target("shapes.root"),
        }
        if self.transfer:
            out.update(
                {
                    "datacard_eos": self.wlcg_target("datacard.txt"),
                    "shapes_eos": self.wlcg_target("shapes.root"),
                }
            )
        return out

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")

        output = self.output()
        hists = self.input().load()[self.var][self.category]

        model = self.StatModel(
            analysis=self.analysis_choice,
            variable=self.var,
            category=self.category,
            hists=hists,
            analysis_inst=self.analysis_inst,
            campaign_inst=self.campaign_inst,
            verbose=self.verbose,
        )

        # Write datacard to vispa
        output["datacard"].parent.touch()
        model.dump(
            output["datacard"].dirname,
            output["datacard"].basename,
            output["shapes"].basename,
        )
        # Write datacard to /eos
        if self.transfer:
            with output["datacard_eos"].localize("w") as card, output["shapes_eos"].localize(
                "w"
            ) as shapes:
                output["datacard_eos"].parent.touch()
                card.copy_from_local(output["datacard"].path)
                shapes.copy_from_local(output["shapes"].path)


class DatacardsCategoriesWrapper(
    Transfer, VariableMixin, RecipeMixin, ModelMixin, AnalysisCampaignTask, law.WrapperTask
):
    _producer = DatacardProducer
    verbose = DatacardProducer.verbose

    def requires(self):
        categories = self.analysis_inst.categories
        return {cat.name: self._producer.req(self, category=cat.name) for cat in categories}


class DatacardsVariablesWrapper(
    Transfer, CategoryMixin, RecipeMixin, ModelMixin, AnalysisCampaignTask, law.WrapperTask
):
    _producer = DatacardProducer
    verbose = DatacardProducer.verbose

    def requires(self):
        variables = self.analysis_inst.variables
        return {var.name: self._producer.req(self, variable=var.name) for var in variables}


class DatacardsWrapper(Transfer, ModelMixin, RecipeMixin, FitDistributionsWrapper):
    _task = DatacardProducer
    verbose = _task.verbose


class EFTDatacardProducer(DatacardProducer):
    statmodel = "EFTStatModel"  # inferred from --BM, use default name for store_parts
    BM = luigi.ChoiceParameter(default="SM", choices=["SM", "box", *[*map(str, range(1, 14))]])

    @cached_property
    def StatModel(self):
        # overwrite mixins.ModelMixin for EFTStatModels
        model = self.analysis_inst.aux.get("stat_model", self.model)
        eftmodels = self._import("models", model).EFTStatModels
        sm = eftmodels[self.BM]
        assert issubclass(sm, Datacard)
        return sm

    def output(self):
        out = {
            "datacard": self.local_target(f"datacard_{self.BM}.txt"),
            "shapes": self.local_target(f"shapes_{self.BM}.root"),
        }
        if self.transfer:
            out.update(
                {
                    "datacard_eos": self.wlcg_target(f"datacard_{self.BM}.txt"),
                    "shapes_eos": self.wlcg_target(f"shapes_{self.BM}.root"),
                }
            )
        return out


class EFTDatacardsWrapper(DatacardsWrapper):
    BM = EFTDatacardProducer.BM
    _task = EFTDatacardProducer

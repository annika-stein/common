# coding: utf-8

import logging
import os
import time

import law
import luigi
from tqdm import tqdm

from tasks.base import AnalysisCampaignTask, BaseTask
from tasks.config import ConfigFR2
from tasks.files import DownloadNanoAODs
from utils.das import dictify_datasets, get_aux, get_name, sort_by_version
from utils.md import MDPyTree, join_contents, parse_table


class ShowPDFSets(AnalysisCampaignTask):
    """
    Example usage:
        law run bbww_dl.ShowPDFSets
    """

    version = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        return DownloadNanoAODs.req(self)

    def output(self):
        return dict(
            txt=self.local_target("pdfsets.txt"),
            html=self.local_target("pdfsets.html"),
            json=self.local_target("pdfsets.json"),
        )

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        import json
        from collections import defaultdict

        import uproot
        from rich.columns import Columns
        from rich.console import Console
        from rich.panel import Panel
        from tqdm.auto import tqdm

        from processor.util import get_pdfset

        console = Console(record=True)

        fileset = self.input()
        fileset = {
            (dsname, shift): [target.path for target in files.targets if target.exists()]
            for (dsname, shift), files in fileset.items()
            if self.campaign_inst.datasets.has(dsname)
        }

        def extract(file):
            tree = uproot.open(file)["Events"]
            try:
                lhepdf_branch = tree["LHEPdfWeight"]
            except:
                return
            lhepdf = type("lhepdf_cls", (), {"__doc__": lhepdf_branch.title})()
            return get_pdfset(lhepdf)

        pdfsets = defaultdict(list)
        for (dsname, shift), files in tqdm(
            fileset.items(), unit="dataset", desc="extract pdf set", total=len(fileset.keys())
        ):
            pdfset = extract(fileset[(dsname, shift)][0])
            pdfsets[str(pdfset)].append(f"{dsname} (shift: {shift})")

        panels = []
        for pdfset, datasets in pdfsets.items():
            panels.append(
                Panel(
                    "\n".join(datasets),
                    expand=True,
                    title=f"{pdfset}",
                    border_style="bold yellow",
                )
            )
        console.print(
            Columns(
                panels,
                expand=True,
                title=f"Available PDF Sets (https://lhapdf.hepforge.org/pdfsets.html)",
            )
        )
        # save
        self.output()["txt"].parent.touch()
        console.save_text(self.output()["txt"].path, clear=False)
        console.save_html(self.output()["html"].path)
        self.output()["json"].dump(pdfsets, indent=4)


class ParseDatasets(BaseTask):
    def requires(self):
        return ConfigFR2.req(self)

    def output(self):
        return self.local_target("datasets.json")

    def run(self):
        md = open(os.environ["DHA_BASE"] + "/../cms-hh-to-bbww/Legacy/datasets.md", "r").read()
        toc = MDPyTree.fromMarkdown(md)

        datasets = toc.children[0].children[1].children
        data = datasets[2]
        signal = datasets[4]
        background = datasets[5]
        wjets_dy = datasets[6]

        all_datasets = {2016: [], 2017: [], 2018: []}

        # parse markdown
        for sample in [data, signal, background, wjets_dy]:
            for year in sample.children:
                _year = int(year.el.string.split(" ")[1])
                datasets_year = []
                print(" - year", _year, " - ")

                # data
                if len(year.children) == 0:
                    datasets = year.content[0].el.string
                    datasets = parse_table(datasets, is_data=True)
                    for dataset in datasets:
                        dataset["is_data"] = True
                    print("data", len(datasets))
                    datasets_year += datasets

                for channel in year.children:

                    # background samples
                    if len(channel.children) == 0:
                        contents = max([_.el.contents for _ in channel.content], key=len)
                        datasets = join_contents(contents)
                        datasets = parse_table(datasets)
                        for dataset in datasets:
                            dataset["is_data"] = False
                        print(channel.el.string, len(datasets))
                        datasets_year += datasets

                    # signal samples
                    for process in channel.children:
                        contents = process.content[0].el.contents
                        datasets = join_contents(contents)
                        datasets = parse_table(datasets)
                        for dataset in datasets:
                            dataset["is_data"] = False

                        print(process.el.string, len(datasets))
                        datasets_year += datasets

                all_datasets[_year] += dictify_datasets(datasets_year)

        # add extra datasets
        conf = self.input()
        for key in all_datasets.keys():
            all_datasets[key] += list(
                {
                    "name": val,
                    "is_data": val.endswith("AOD"),
                    "keys": [val],
                    "misc": {"extra": True},
                }
                for val in conf[key]["extra"].load()
            )

        self.output().dump(all_datasets)


@law.wlcg.ensure_voms_proxy
class QueryDatasets(BaseTask):
    nanover = luigi.Parameter(default="v7", description="NanoAOD version. default: v7")
    # fmt: off
    exceptions = {
        "v6": {
            "/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/MINIAODSIM": "/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17NanoAODv6-PU2017_12Apr2018_Nano25Oct2019_102X_mc2017_realistic_v7-v1/NANOAODSIM"
        },
        "v7": {
            "/SingleElectron/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/SingleElectron/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/SingleMuon/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/SingleMuon/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/DoubleEG/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/DoubleEG/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/DoubleMuon/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/DoubleMuon/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/MuonEG/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/MuonEG/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/MuonEG/Run2016E-17Jul2018-v2/MINIAOD": ["/MuonEG/Run2016E-02Apr2020-v2/NANOAOD"],
            "/BTagCSV/Run2016B-17Jul2018_ver1-v1/MINIAOD": ["/BTagCSV/Run2016B-02Apr2020_ver1-v1/NANOAOD"],
            "/BTagCSV/Run2016B-17Jul2018_ver2-v1/MINIAOD": ["/BTagCSV/Run2016B-02Apr2020_ver2-v1/NANOAOD"],
            "/GluGluToHHTo2B2VTo2L2Nu_node_2_13TeV-madgraph-v2/RunIISummer16MiniAODv3-PUMoriond17_94X_mcRun2_asymptotic_v3-v3/MINIAODSIM": ["/GluGluToHHTo2B2VTo2L2Nu_node_2_13TeV-madgraph/RunIISummer16NanoAODv7-PUMoriond17_Nano02Apr2020_102X_mcRun2_asymptotic_v8-v1/NANOAODSIM"],
            "/GluGluToRadionToHHTo2B2Tau_M-650_narrow_13TeV-madgraph/RunIISummer16MiniAODv3-PUMoriond17_94X_mcRun2_asymptotic_v3-v2/MINIAODSIM": ["/GluGluToRadionToHHTo2B2Tau_M-650_narrow_13TeV-madgraph/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"],
            "/GluGluToBulkGravitonToHHTo2B2WToLNu2J_M-3000_narrow_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM": ["/GluGluToBulkGravitonToHHTo2B2WToLNu2J_M-3000_narrow_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"],
            "/GluGluToHHTo2B2WToLNu2J_node_8_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/MINIAODSIM": ["/GluGluToHHTo2B2WToLNu2J_node_8_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"],
            "/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v2/MINIAODSIM": ["/TTTT_TuneCP5_13TeV-amcatnlo-pythia8/RunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1/NANOAODSIM"],
        },
    }
    # fmt: on

    def requires(self):
        return ParseDatasets.req(self)

    def output(self):
        out = {
            year: self.local_target(f"Run2_pp_13TeV_{year}_nano.json")
            for year in [2016, 2017, 2018]
        }
        out["log"] = self.local_target("datasets.log")
        return out

    def run(self):
        from utils.dsdb import dasquery

        ppd_sim = {
            "v6": {
                2016: {"name": "RunIISummer16NanoAODv6", "gt": "102X_mcRun2_asymptotic_v7"},
                2017: {"name": "RunIIFall17NanoAODv6", "gt": "102X_mc2017_realistic_v7"},
                2018: {"name": "RunIIAutumn18NanoAODv6", "gt": "102X_upgrade2018_realistic_v20"},
            },
            "v7": {
                2016: {"name": "RunIISummer16NanoAODv7", "gt": "102X_mcRun2_asymptotic_v8"},
                2017: {"name": "RunIIFall17NanoAODv7", "gt": "102X_mc2017_realistic_v8"},
                2018: {"name": "RunIIAutumn18NanoAODv7", "gt": "102X_upgrade2018_realistic_v21"},
            },
        }

        ppd_data = {
            "v6": {
                2016: {"name": "Nano25Oct2019"},
                2017: {"name": "Nano25Oct2019"},
                2018: {"name": "Nano25Oct2019"},
            },
            "v7": {
                2016: {"name": "02Apr2020"},
                2017: {"name": "02Apr2020"},
                2018: {"name": "02Apr2020"},
            },
        }
        # create logger
        self.output()["log"].touch()
        logging.basicConfig(
            filename=self.output()["log"].path,
            filemode="w",
            format="%(message)s",
            level=logging.INFO,
        )
        all_datasets = {2016: [], 2017: [], 2018: []}
        for year, datasets in self.input().load().items():
            year = int(year)
            all_subs = []
            for dataset in tqdm(datasets, desc=f"Year {year}"):
                is_data = dataset["is_data"]
                ppd = ppd_data[self.nanover] if is_data else ppd_sim[self.nanover]
                subs = []
                for dataset_key in dataset["keys"]:
                    original_datasets = dasquery(f"dataset={dataset_key}")
                    # dataset_key can have stars or similar
                    # so we will get more datasets out of a dataset
                    # need to check for duplicates
                    usables = [
                        ds for ds in original_datasets if "creation_time" in ds["dataset"][0]
                    ]
                    ids = set([ds["dataset"][0]["dataset_id"] for ds in usables])
                    uniques = []
                    for id in ids:
                        for ds in usables:
                            if ds["dataset"][0]["dataset_id"] == id:
                                uniques.append(ds)
                                break

                    for ds in uniques:
                        if "creation_time" not in ds["dataset"][0]:
                            continue
                        original_key = ds["dataset"][0]["name"]
                        children = dasquery(f"child dataset={original_key}")
                        children_keys = [x["child"][0]["name"] for x in children]
                        usable_children_keys = [
                            k
                            for k in children_keys
                            if (ppd[year]["name"] in k and (is_data or (ppd[year]["gt"] in k)))
                        ]
                        # logging for different cases
                        if dataset_key in self.exceptions[self.nanover]:
                            logging.info(f"taking exception for {original_key}")
                            usable_children_keys = self.exceptions[self.nanover][dataset_key]
                            logging.info("")
                        elif len(usable_children_keys) == 0:
                            logging.info(f"== no usable children for: {original_key} ==")
                            cr_time = ds["dataset"][0]["creation_time"]
                            two_months_ago = time.time() - 5184000
                            if len(children_keys) > 0:
                                logging.info("only has:\n" + "\n".join(children_keys))
                            if cr_time > two_months_ago:
                                logging.info("-> very new")
                            if ds["dataset"][0]["status"] == "PRODUCTION":
                                logging.info("-> still in production")
                            logging.info("")
                        elif len(usable_children_keys) > 1:
                            logging.info(f"== many children for: {original_key} ==")
                            logging.info("\n".join(usable_children_keys))
                            best = sort_by_version(usable_children_keys, regex="v(\d+)/NANOAODSIM")[
                                0
                            ]
                            logging.info(f"take {best}")
                            usable_children_keys = [best]
                            logging.info("")

                        sub = dict(dataset)
                        if "/" in sub["name"]:
                            sub["name"] = get_name(original_key, is_data=dataset.get("is_data"))
                        aux = sub.get("aux", {})
                        aux.update(get_aux(original_key, sub.get("is_data")))
                        sub["aux"] = aux
                        sub["keys"] = usable_children_keys
                        subs.append(sub)
                all_subs += subs
            all_datasets[year] += all_subs
        for year, datasets in all_datasets.items():
            self.output()[int(year)].dump(datasets)


class PlotBranchingRatiosHH(BaseTask):
    version = None
    no_numbers = luigi.BoolParameter()

    def output(self):
        name = "BR"
        if self.no_numbers:
            name += "_no_numbers"
        return self.local_target(f"{name}.pdf")

    def run(self):
        import matplotlib
        import numpy as np

        matplotlib.rcParams["text.usetex"] = True
        matplotlib.rcParams["font.family"] = "Times New Roman"
        import matplotlib.pyplot as plt
        from matplotlib.colors import LogNorm

        from config.constants import (
            BR_H_BB,
            BR_H_CC,
            BR_H_GG,
            BR_H_MM,
            BR_H_TT,
            BR_H_WW,
            BR_H_YY,
            BR_H_ZY,
            BR_H_ZZ,
        )

        self.output().parent.touch()

        # high -> low
        BRs = [BR_H_BB, BR_H_WW, BR_H_GG, BR_H_TT, BR_H_CC, BR_H_ZZ, BR_H_YY, BR_H_ZY, BR_H_MM]

        def _calc(x, y):
            if x == y:
                return (x ** 2).nominal
            else:
                return (2 * x * y).nominal

        BR = np.array([[_calc(br1, br2) for br1 in BRs] for br2 in BRs[::-1]])

        idx = np.triu_indices(9, 1)
        BR[::-1, :][(idx[1], idx[0])] = np.nan

        fig, ax = plt.subplots()
        im = ax.imshow(BR.T, norm=LogNorm(), cmap="YlGn")
        ticks = [
            "bb",
            "WW",
            "gg",
            r"$\tau\tau$",
            "cc",
            "ZZ",
            r"$\gamma\gamma$",
            r"Z$\gamma$",
            r"$\mu\mu$",
        ]
        ax.set_xticks(np.arange(len(ticks)))
        ax.set_yticks(np.arange(len(ticks)))
        ax.set_xticklabels(ticks[::-1])
        ax.set_yticklabels(ticks)
        ax.set_xlabel(r"H $\rightarrow$ XX")
        ax.set_ylabel(r"H $\rightarrow$ YY")
        ax.text(
            0.25,
            0.75,
            r"$\mathcal{BR}$(HH $\rightarrow$ XXYY)" + "\n" + r"\small{YR4, arXiv:1610.07922}",
            fontsize=16,
        )

        def _fmt(v: float) -> str:
            if v >= 0.005:
                return f"{v:.4f}"
            else:
                s = f"{v:.2e}".split("e")
                return fr"${s[0]}$" + "\n" + fr"$\cdot 10^{{{int(s[1])}}}$"

        # show values
        if not self.no_numbers:
            for i in range(len(ticks)):
                for j in range(len(ticks)):
                    if np.isnan(BR.T[i, j]):
                        continue

                    color = "white" if BR.T[i, j] >= 1e-3 else "black"
                    ax.text(
                        j, i, _fmt(BR.T[i, j]), ha="center", va="center", fontsize=8, color=color
                    )

        plt.colorbar(im)
        fig.savefig(self.output().path)

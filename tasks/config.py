# -*- coding: utf-8 -*-
import os

import law


class ConfigProvider(law.ExternalTask):
    store_parts = []

    @property
    def paths(self):
        raise NotImplementedError

    def config_target(self, path):
        return law.LocalFileTarget(os.path.join(os.environ["DHA_BASE"], "config", path))

    def output(self):
        return {key: self.config_target(path) for key, path in self.paths.items()}


class CampaignProvider(ConfigProvider):
    @property
    def campaign(self):
        raise NotImplementedError

    @property
    def paths(self):
        return {
            "config": f"{self.campaign}.py",
            "datasets": f"campaigns/{self.campaign}_nano.json",
            "extra": f"campaigns/{self.campaign}_extra.json",
            "processes": f"campaigns/{self.campaign}_nano.csv",
        }


class Campaign2016(CampaignProvider):
    campaign = "Run2_pp_13TeV_2016"


class Campaign2017(CampaignProvider):
    campaign = "Run2_pp_13TeV_2017"


class Campaign2018(CampaignProvider):
    campaign = "Run2_pp_13TeV_2018"


class ConfigFR2(ConfigProvider):
    paths = {}

    def requires(self):
        return {
            2016: Campaign2016.req(self),
            2017: Campaign2017.req(self),
            2018: Campaign2018.req(self),
        }

    def output(self):
        return self.input()

# coding: utf-8

from __future__ import absolute_import

__all__ = [
    "BaseTask",
    "CampaignTask",
    "AnalysisTask",
    "AnalysisCampaign",
    "CombinationsTask",
    "HTCondorWorkflow",
    "HTCondorGPUWorkflow",
]


import fnmatch
import importlib
import os
import shlex
from contextlib import contextmanager
from functools import cached_property, partial
from multiprocessing import Pool
from pathlib import Path
from subprocess import DEVNULL, call, check_output
from time import sleep, time
from typing import Tuple

import law
import luigi
from tqdm.auto import tqdm

from utils import aci

law.contrib.load(
    "numpy", "tasks", "root", "slack", "telegram", "wlcg", "htcondor", "hdf5", "coffea"
)


class BaseTask(law.Task):
    version = luigi.Parameter(description="task version")
    notify = law.telegram.NotifyTelegramParameter()

    exclude_params_req = {"notify"}
    exclude_params_branch = {"notify"}
    exclude_params_workflow = {"notify"}
    output_collection_cls = law.SiblingFileCollection
    workflow_run_decorators = [law.decorator.notify]
    message_cache_size = 20

    def local_path(self, *path):
        parts = [str(p) for p in self.store_parts() + path]
        return os.path.join(os.environ["DHA_STORE"], *parts)

    def wlcg_path(self, *path):
        parts = [str(p) for p in self.store_parts() + path]
        return os.path.join(*parts)

    def local_target(self, *args):
        cls = law.LocalFileTarget if args else law.LocalDirectoryTarget
        return cls(self.local_path(*args))

    def local_directory_target(self, *args):
        return law.LocalDirectoryTarget(self.local_path(*args))

    def wlcg_target(self, *args, **kwargs):
        cls = law.wlcg.WLCGFileTarget if args else law.wlcg.WLCGDirectoryTarget
        return cls(self.wlcg_path(*args), **kwargs)

    def deep_requires(self, condition=lambda x: True):
        outp = self.requires()
        while not condition(outp):
            outp = outp.requires()
        return outp

    @contextmanager
    def timeit(self, msg, publish=True):
        if "%" not in msg:
            msg += ": %.2fs"
        t = time()
        try:
            yield
        finally:
            msg %= time() - t
            if publish:
                self.publish_message(msg)
            self.logger.info(repr(self), msg)

    def store_parts(self):
        parts = (self.__class__.__name__,)
        if self.version is not None:
            parts += (self.version,)
        return parts


class EOSFileUpload(BaseTask):
    filepath = luigi.Parameter()

    @property
    def File(self):
        return Path(self.filepath)

    def output(self):
        return self.wlcg_target(self.File.name)

    def run(self):
        self.output().copy_from_local(str(self.File))


class FileProvider(law.ExternalTask):
    filepath = luigi.Parameter()

    def output(self):
        if self.filepath:
            return law.LocalFileTarget(self.filepath)


class CampaignBase(BaseTask):
    year = luigi.Parameter(description="year", default="2017")

    @property
    def campaign_name(self):
        return f"Run2_pp_13TeV_{self.year}"

    @cached_property
    def campaign_inst(self):
        return self.analysis_inst.campaigns.get(self.campaign_name)


class AnalysisTask(BaseTask):
    analysis_choice = luigi.Parameter(
        default=os.environ["DHA_ANALYSIS_ID"],
        description="Analysis type (name of submodule)",
    )

    def _import(self, *parts):
        return importlib.import_module(".".join((self.analysis_choice,) + parts))

    @cached_property
    def analysis_inst(self):
        return self._import("config.analysis").analysis

    def store_parts(self):
        parts = (self.analysis_choice, self.__class__.__name__)
        if self.version is not None:
            parts += (self.version,)
        return parts


class CampaignTask(CampaignBase):
    @cached_property
    def analysis_inst(self):
        return importlib.import_module("config.analysis").analysis

    def store_parts(self):
        parts = ("common", self.campaign_name, self.__class__.__name__)
        if self.version is not None:
            parts += (self.version,)
        return parts


class AnalysisCampaignTask(AnalysisTask, CampaignBase):
    def store_parts(self):
        parts = (self.analysis_choice, self.campaign_name, self.__class__.__name__)
        if self.version is not None:
            parts += (self.version,)
        return parts


class CombinationTask(BaseTask):
    analyses = law.CSVParameter(
        default=("bbww_dl", "bbww_sl"),
        description="Analyses for combination",
    )
    years = law.CSVParameter(
        default=("2016", "2017", "2018"),
        description="Years for combination",
    )
    analysis_categories = luigi.DictParameter(default={}, description="Analysis categories")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert set(self.years) <= (
            avail := {"2016", "2017", "2018"}
        ), f"[--years] can not understand: {set(self.years)-avail}, choose from: {sorted(avail)}"
        if not self.analysis_categories:
            self.analysis_categories = {
                analysis: analysis_inst.categories.query(name=".+", tags={"fit"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }

    def _import(self, *parts):
        return importlib.import_module(".".join(parts))

    @property
    def analysis_insts(self):
        return {
            analysis: self._import(f"{analysis}.config.analysis").analysis
            for analysis in self.analyses
        }

    @property
    def campaign_insts(self):
        return {
            (analysis, year): self._import(f"{analysis}.config.analysis").analysis.campaigns.get(
                f"Run2_pp_13TeV_{year}"
            )
            for analysis in self.analyses
            for year in self.years
        }

    def cat2acc(self, cat: str) -> Tuple[aci.Analysis, aci.Campaign, aci.Category]:
        if "__" in cat:
            analysis, cat = cat.split("__", 1)
        else:
            (analysis,) = self.analyses
        if cat.startswith(("y", "20")):
            year, cat = cat.split("_", 1)
            year = year.replace("y", "")
        else:
            year = self.year
        ana = self.analysis_insts[analysis]
        return ana, self.campaign_insts[analysis, year], ana.categories.get(cat)

    def store_parts(self):
        parts = (
            "common",
            f"{'_'.join(sorted(self.analyses))}__{'_'.join(sorted(self.years))}",
            self.__class__.__name__,
        )
        if self.version is not None:
            parts += (self.version,)
        return parts


class FitDistributionsWrapper(AnalysisCampaignTask, law.WrapperTask):
    """
    Wrapper task which requires one task for all fit distributions in all fit categories.
    Can be used for example for datacard producing or plotting.
    """

    categories = law.CSVParameter(
        default=(),
        description="Fit categories to process. Default: all with tag 'fit'.",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # make sure '_task' has 'category' and 'variable' attr
        assert hasattr(self._task, "variable") and hasattr(self._task, "category")
        if not self.categories:
            self.categories = self.analysis_inst.categories.query(name=".+", tags={"fit"}).keys

    @property
    def _task(self):
        raise NotImplementedError

    def requires(self):
        cv = {}
        for category in map(self.analysis_inst.categories.get, self.categories):
            if category.aux is not None and "fit_variable" in category.aux:
                cv[category.name] = category.aux["fit_variable"]
        return {cat: self._task.req(self, category=cat, variable=var) for cat, var in cv.items()}


class HTCondorWorkflow(law.htcondor.HTCondorWorkflow):

    htcondor_logs = luigi.BoolParameter()
    memory = 2000
    gpu_memory = 0

    def htcondor_post_submit_delay(self):
        return self.poll_interval * 60

    def htcondor_output_directory(self):
        # the directory where submission meta data should be stored
        return law.LocalDirectoryTarget(self.local_path())

    def htcondor_job_config(self, config, job_num, branches):
        # copy the entire environment
        config.custom_content.append(("getenv", "true"))
        config.custom_content.append(("request_cpus", "1"))
        config.custom_content.append(("RequestMemory", str(self.memory)))
        config.custom_content.append(("Request_GpuMemory", str(self.gpu_memory)))

        if self.htcondor_logs:
            config.stdout = "out.txt"
            config.stderr = "err.txt"
            config.log = "log.txt"

        return config

    def htcondor_use_local_scheduler(self):
        return True


class HTCondorGPUWorkflow(HTCondorWorkflow):
    exclude_hosts = [
        "vispa-gpu04",
        "vispa-gpu07",
    ]
    memory = 9000
    gpu_memory = 600

    def htcondor_job_config(self, config, job_num, branches):
        config = super().htcondor_job_config(config, job_num, branches)
        requirements = " && ".join(
            f'(TARGET.Machine != "{host}.physik.rwth-aachen.de")' for host in self.exclude_hosts
        )
        config.custom_content.append(("requirements", requirements))
        config.render_variables["dha_base"] = os.getenv("DHA_BASE")
        return config


class Debuggable:
    debug = luigi.BoolParameter()
    debugme = luigi.BoolParameter()
    # exclude_params_req = {"debugme"}


class PoolMap:

    n_parallel = luigi.IntParameter(default=0, significant=False)
    n_parallel_max = None

    @cached_property
    def pool(self):
        n = self.n_parallel
        n = min(self.n_parallel_max or n, n or self.n_parallel_max or n)
        return Pool(n or None)

    def __getstate__(self):
        state = super().__getstate__() if hasattr(super(), "__getstate__") else self.__dict__
        if "pool" in state:
            state = dict(state)
            del state["pool"]
        return state

    def pmap(self, func, iterable, chunksize=1, unordered=False, items=False, **kwargs):
        if items:
            unordered = False
            keys, iterable = zip(*iterable.items())
            # dict = type(iterable)
        try:
            kwargs.setdefault("total", len(iterable))
        except:
            pass
        if self.n_parallel == 1:
            iterable = map(func, iterable)
        else:
            imap = self.pool.imap_unordered if unordered else self.pool.imap
            iterable = imap(func, iterable, chunksize)
        ret = tqdm(iterable, **kwargs)
        return dict(zip(keys, ret)) if items else ret

    def on_failure(self, exc):
        self.__dict__.pop("pool", None)

    def on_success(self):
        self.__dict__.pop("pool", None)


class DHITask:
    outmap = None  # optionally output mapping via: output path -> fnmatch pattern
    dhi_command = luigi.Parameter("")
    dhi_sleep = luigi.IntParameter(120, significant=False)
    pcas = luigi.Parameter(default="", description="pcas=[p]rint [c]ommand [a]nd [s]ubstitute")

    interactive_params = law.Task.interactive_params + ["pcas"]

    def _pcas(self, args):
        import warnings

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            cmd = self.command()
            if self.pcas in cmd:
                dc_idx = cmd.index(self.pcas) + 1
                # substitute
                cmd[dc_idx] = "<blinded>"
            print(f"> {self.repr(color=True)}")
            print("|   Command:")
            print(f"{law.util.colored(shlex.join(['dhi_law.sh', 'run'] + list(cmd)), 'cyan')}")

    def command(self):
        raise NotImplementedError("command must be overridden")

    @staticmethod
    def remote_path(target):
        uri = target.uri()
        prefix = "root://eosuser.cern.ch"
        assert uri.startswith(prefix + "/eos/user")
        return uri[len(prefix) :]

    def output(self):
        if self.outmap is None:
            return self.local_target(".done")
        else:
            out = [self.local_target(path) for path in self.outmap.keys()]
            return out if len(out) > 1 else out[0]

    def complete(self):
        return False if self.dhi_command else super().complete()

    def _repr_flags(self):
        return super()._repr_flags() + ["DHI task"]

    @law.decorator.safe_output
    def run(self):
        cmd = ["dhi_law.sh", "run"] + self.command()
        if self.dhi_command:
            cmd += shlex.split(self.dhi_command.strip())

        while (rc := call(cmd, stdin=DEVNULL)) == 10:
            suffix = "sleep" if self.dhi_sleep else "exit"
            self.logger.info(f"==>> command already running, going to {suffix}")
            if self.dhi_sleep:
                sleep(self.dhi_sleep)
            else:
                return
        if rc != 0:
            raise RuntimeError("command failed: " + shlex.join(cmd))
        if self.dhi_command:
            return

        prefix = "file:///eos/user"
        output = [] if self.outmap is None else {}

        skip = True
        unused = []
        inaccessible = []
        for line in check_output(
            cmd + ["--print-output", "0"], encoding="utf8", stdin=DEVNULL
        ).splitlines():
            line = line.strip()
            if skip:
                skip = bool(line)
                continue

            if not line.startswith(prefix):
                inaccessible.append(line)
                continue
            line = line[len(prefix) :]

            if self.outmap is None:
                output.append(line)
            else:
                for path, pattern in self.outmap.items():
                    if fnmatch.fnmatch(line, pattern):
                        output[path] = line
                        break
                else:
                    unused.append(line)

        if self.outmap is None:
            if len(output) == 1:
                output = {os.path.basename(output[0]): output[0]}
            else:
                prefix = os.path.commonpath(output)
                output = {out[len(prefix) :]: out for out in output}
        else:
            missing = set(self.outmap.keys()) - set(output.keys())
            if missing or not self.outmap:
                msg = []
                if missing:
                    msg.append("some outputs are missing:")
                    msg.extend(f"\t{name} (pattern: {self.outmap[name]})" for name in missing)
                if inaccessible:
                    msg.append("inaccessible outputs:")
                    msg.extend(f"\t{path}" for path in inaccessible)
                if unused:
                    msg.append("unused outputs:")
                    msg.extend(f"\t{line}" for line in unused)
                if msg:
                    raise RuntimeError("\n".join(msg))

        for dst, src in output.items():
            self.wlcg_target(src, fs="eos_fs").copy_to_local(self.local_target(dst))

        if self.outmap is None:
            self.output().touch()

# coding: utf-8

from typing import Dict, Optional
from warnings import warn

import awkward as ak
import hist
import luigi
import numpy as np
import scinum as sn

import utils.aci as aci
from config.constants import HHres, gfHHparams
from tasks.coffea import CoffeaProcessor
from tasks.files import DownloadFiles
from utils.coffea import BaseProcessor, hist_accumulator

from .base import CorrectionsBase


@luigi.util.inherits(CoffeaProcessor)
class HHReweigthing(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "HHCount"
    debug = False
    explorative = False

    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def requires(self):
        return dict(
            coeff=DownloadFiles.req(self, type="default"),
            count=CoffeaProcessor.req(self, processor=self.processor),
        )

    def output(self):
        return self.local_target("correction.coffea")

    def run(self):
        inp = self.input()

        count = inp["count"].load()["MhhCosT"].hist.to_hist()
        assert count.axes.name == ("dataset", "Mhh", "CosT")
        assert np.all(count.axes[-2].edges == HHreweigther.edges_Mhh)
        assert np.all(count.axes[-1].edges == HHreweigther.edges_CosT)

        cview = count.view()["value"]
        frac = {}
        for root_proc in self.analysis_inst.aux[self.__class__.__name__].keys():
            if isinstance(root_proc, str):
                root_proc = self.analysis_inst.processes.get(root_proc)
            assert isinstance(root_proc, aci.Process)
            sm = tot = 0
            for i, ds in enumerate(map(self.campaign_inst.datasets.get, count.axes[0])):
                if not any(map(root_proc.has_process, ds.processes.values)):
                    continue
                tot += cview[i]
                if ds.name.endswith(("_node_cHHH1", "_CV_1_C2V_1_C3_1")):
                    sm = cview[i].copy()

            # this has to be caught in a meaningful way!
            if sm[-1, -1] == 0:
                sm[-1, -1] = sm[sm != 0].min()
            assert np.sum(sm) > 0, f"{root_proc.name}: SM shape missing"
            assert np.all(sm > 0), f"{root_proc.name}: SM shape contains empty bins"

            frac[root_proc.name] = np.divide(sm, tot, out=np.zeros_like(tot), where=tot > 0)

        self.output().dump(
            HHreweigther(
                coeff_file=inp["coeff"]["hh_coeff"].path,
                frac=frac,
            )
        )

    @staticmethod
    def getoradd_process(proc, **kwargs):
        name = kwargs.get("name", None)
        if name is None:
            raise ValueError("Need name")
        p = proc.processes.get(name)
        if p is None:
            p = aci.Process(**kwargs)
            proc.processes.add(p)
        return p

    @classmethod
    def update_config(
        cls,
        config: aci.Analysis,
        root_processes: aci.Process,
        benchmarks: Dict[str, sn.Number],
        offset: int = 10000,
        # base XS for reweighted process, default 1pb, None: actually calculate
        base_xs: Optional[float] = 1,
    ):
        config.aux.setdefault("non_common_corrections", []).append(cls.__name__)

        benchmarks = list(map(cls._parse_benchmark, benchmarks))
        (ecm,) = set(campaign.ecm for campaign in config.campaigns)

        mapping = {}
        for root, br in root_processes.items():
            root = config.processes.get(root)
            assert root is not None
            assert root.has_parent_process(config.processes.get("diHiggs"))
            assert not root.is_leaf_process
            proc = cls.getoradd_process(
                root,
                name=f"{root.name}_reweight",
                id=root.id + offset,
                label=f"{root.label} (reweighted)",
            )
            mapping[root.name] = [
                aci.Dataset(
                    name=f"{proc.name}_{name}",
                    id=id,
                    is_data=False,
                    processes=[
                        cls.getoradd_process(
                            proc,
                            name=f"{proc.name}_{name}",
                            id=id,
                            label=f"{proc.label} ({label})",
                            xsecs={
                                ecm: br
                                * (
                                    HHreweigther.functionGF(
                                        **dict(zip(params.dtype.names, params)),
                                        A=HHreweigther.A13tev,
                                    )
                                    * 1e-3
                                    if base_xs is None
                                    else base_xs
                                )
                            },
                            aux=dict(gfHHparams=params),
                        ),
                    ],
                )
                for id, (name, label, params) in enumerate(benchmarks, start=proc.id + 1)
            ]
            for campaign in config.campaigns.values:
                campaign.datasets.extend(mapping[root.name])

        config.aux[cls.__name__] = mapping

    @staticmethod
    def _parse_benchmark(benchmark):
        bm = benchmark.copy()
        params = gfHHparams[0].copy()
        for n in params.dtype.names:
            params[n] = bm.pop(n, params[n])
        name = bm.pop(
            "name", "_".join(str(v).rstrip("0").rstrip(".").replace(".", "p") for v in params)
        )
        label = bm.pop("label", " ".join(f"{k}={params[k]:.1f}" for k in params.dtype.names))
        if bm:
            warn(f"unexpected benchmark parameters {list(bm.keys())!r} in {benchmark!r}")
        return name, label, params


def events2MhhCosT(events):
    H = events.GenPart
    H = H[(H.pdgId == 25) & H.hasFlags(["isHardProcess"])]
    assert ak.all(ak.num(H) == 2, axis=-1)

    HH = H[:, 0] + H[:, 1]
    Hb = H[:, 0].boost(-HH.boostvec)

    Hp = ak.to_numpy(Hb.p)
    Hz = ak.to_numpy(Hb.z)

    return (
        ak.to_numpy(HH.mass),
        # CosT=np.abs(np.cos(Hb.theta)),
        # https://root.cern.ch/doc/master/classTVector3.html#a7f07d5b781cb229ccc89c8c7fbe7f2b5
        np.abs(np.divide(Hz, Hp, out=np.ones_like(Hp), where=Hp != 0)),
    )


class HHreweigther(object):
    edges_Mhh = np.array(
        [
            250,
            270,
            290,
            310,
            330,
            350,
            370,
            390,
            410,
            430,
            450,
            470,
            490,
            510,
            530,
            550,
            570,
            590,
            610,
            630,
            650,
            670,
            700,
            750,
            800,
            850,
            900,
            950,
            1000,
            1100,
            1200,
            1300,
            1400,
            1500,
            1750,
            2000,
            5000,
        ]
    )
    edges_CosT = np.array([0.0, 0.4, 0.6, 0.8, 1.0])
    A13tev = np.array(
        [
            62.5088,
            345.604,
            9.63451,
            4.34841,
            39.0143,
            -268.644,
            -44.2924,
            96.5595,
            53.515,
            -155.793,
            -23.678,
            54.5601,
            12.2273,
            -26.8654,
            -19.3723,
            -0.0904439,
            0.321092,
            0.452381,
            -0.0190758,
            -0.607163,
            1.27408,
            0.364487,
            -0.499263,
        ]
    )
    params = gfHHparams

    @property
    def lenA(self):
        return len(self.A13tev)

    def __init__(self, coeff_file, frac):
        raw_values = np.loadtxt(coeff_file, delimiter=",", usecols=range(1, 5 + self.lenA))
        raw_shifts = np.loadtxt(coeff_file, delimiter=",", usecols=[0], dtype=str)
        raw_shifts = np.char.strip(raw_shifts, '"')
        shifts = sorted(set(raw_shifts))

        A = np.full(
            (len(shifts), len(self.edges_Mhh) - 1, len(self.edges_CosT) - 1, self.lenA), np.nan
        )

        assert len(raw_values) == len(raw_shifts)
        if len(raw_values) != np.prod(A.shape[:-1]):
            raise RuntimeError("not enough entries to fill all (fixed) bins")
        if len(bad := np.setdiff1d(raw_values[:, 0:2], self.edges_Mhh)):
            raise RuntimeError(f"unexpected Mhh bin edges: {bad}")
        if len(bad := np.setdiff1d(raw_values[:, 2:4], self.edges_CosT)):
            raise RuntimeError(f"unexpected CosT bin edges: {bad}")

        A[
            np.searchsorted(shifts, raw_shifts),
            np.searchsorted(self.edges_Mhh, raw_values[:, 0]),
            np.searchsorted(self.edges_CosT, raw_values[:, 2]),
        ] = raw_values[:, 4:]
        A = np.moveaxis(A, -1, 0)
        A = np.ascontiguousarray(A)

        assert not np.isnan(A).any()
        assert all(f.shape == A.shape[-2:] for f in frac.values())
        assert all(np.all(f >= 0) for f in frac.values())

        self.A = A
        self.shifts = shifts
        self.frac = frac

    @staticmethod
    def functionGF(kl, kt, c2, cg, c2g, A):
        assert len(A) == 23
        return sum(
            A[i] * f
            for i, f in enumerate(
                [
                    kt ** 4,
                    c2 ** 2,
                    kt ** 2 * kl ** 2,
                    cg ** 2 * kl ** 2,
                    c2g ** 2,
                    c2 * kt ** 2,
                    kl * kt ** 3,
                    kt * kl * c2,
                    cg * kl * c2,
                    c2 * c2g,
                    cg * kl * kt ** 2,
                    c2g * kt ** 2,
                    kl ** 2 * cg * kt,
                    c2g * kt * kl,
                    cg * c2g * kl,
                    kt ** 3 * cg,
                    kt * c2 * cg,
                    kt * cg ** 2 * kl,
                    cg * kt * c2g,
                    kt ** 2 * cg ** 2,
                    c2 * cg ** 2,
                    cg ** 3 * kl,
                    cg ** 2 * c2g,
                ]
            )
        )

    def makeSF(self, kl, kt, c2, cg, c2g, frac, shift=""):
        assert frac.shape == self.A.shape[-2:]

        A = self.A
        if shift is not all:
            A = A[:, self.shifts.index(shift)]

        XSbin = self.functionGF(kl, kt, c2, cg, c2g, A=A) / 1000
        XSbin *= np.diff(self.edges_Mhh)[:, None]  # Mhh bin width
        XSbin *= np.diff(self.edges_CosT)  # CosT bin width
        # XSbin = np.maximum(0, XSbin)  # negative XS makes no sense?
        XStot = self.functionGF(kl, kt, c2, cg, c2g, A=self.A13tev)

        return np.divide(XSbin / XStot, frac, out=np.zeros_like(XSbin), where=frac > 0)

    def generate(self, events, dataset, config):
        # is it involved in reweighting, if so what is the root process
        for root_proc, targets in config.aux[HHReweigthing.__name__].items():
            if isinstance(root_proc, str):
                root_proc = config.processes.get(root_proc)
            assert isinstance(root_proc, aci.Process)
            if any(map(root_proc.has_process, dataset.processes.values)):
                break
        else:
            return {}

        Mhh, CosT = events2MhhCosT(events)

        # https://github.com/fabio-mon/HHStatAnalysis/blob/NLO_model/AnalyticalModels/python/NonResonantModelNLO.py#L68-L69
        assert np.min(Mhh) >= self.edges_Mhh[0] * 0.999

        # https://github.com/fabio-mon/HHStatAnalysis/blob/NLO_model/AnalyticalModels/python/NonResonantModelNLO.py#L79-L80
        assert np.all(0 <= CosT) and np.all(CosT <= 1)

        idx = tuple(
            np.clip(np.searchsorted(edges, value, side="right") - 1, 0, len(edges) - 2)
            for edges, value in zip([self.edges_Mhh, self.edges_CosT], [Mhh, CosT])
        )

        return {
            target_dataset.name: np.where(
                # https://github.com/pmandrik/VSEVA/blob/master/HHWWgg/reweight/reweight_HH.C#L456
                # https://github.com/fabio-mon/HHStatAnalysis/blob/NLO_model/AnalyticalModels/python/NonResonantModelNLO.py#L120-L121
                (Mhh < self.edges_Mhh[0]) | (Mhh > self.edges_Mhh[-1]),
                0,
                self.makeSF(
                    *target_dataset.processes.values[0].aux["gfHHparams"],
                    frac=self.frac[root_proc.name],
                )[idx],
            )
            for target_dataset in targets
        }


class HHCount(BaseProcessor):
    @classmethod
    def requires(cls, task):
        return task.base_requires(
            data_source="mc",
            corrections=False,
            data_filter=cls.dataset_filter,
        )

    @staticmethod
    def dataset_filter(dataset):
        good = False
        for process in dataset.processes.values:
            for _, proc in process.walk_parent_processes(include_self=True):
                if proc.name.endswith(HHres["resonance"]):
                    return False
                if proc.name == "diHiggs":
                    good = True
        return good

    def __init__(self, task):
        super().__init__(task)

        self._accumulator["MhhCosT"] = hist_accumulator(
            hist.Hist(
                self.dataset_axis,
                hist.axis.Variable(HHreweigther.edges_Mhh, name="Mhh"),
                hist.axis.Variable(HHreweigther.edges_CosT, name="CosT", flow=False),
                storage=hist.storage.Weight(),
            )
        )

    def process(self, events):
        output = self.accumulator.identity()

        Mhh, CosT = events2MhhCosT(events)

        output["MhhCosT"].hist.fill(
            dataset=self.get_dataset(events).name,
            Mhh=Mhh,
            CosT=CosT,
            weight=events.Generator.weight,
        )
        return output

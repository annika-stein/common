# coding: utf-8

from tqdm.auto import tqdm

from tasks.files import DownloadFiles

from .base import CorrectionsBase, PlotCorrectionsBase, SFPTEta


class MuonSFPTEta(SFPTEta):
    particle = "muon"


class MuonCorrections(CorrectionsBase):
    def requires(self):
        all = DownloadFiles.req(self, type="histo")
        prep = {}
        if "muon_sf_pteta_files" in all.files["histo"]:
            name = sorted(all.files["histo"]["muon_sf_pteta_files"].keys())[0]
            prep[name] = MuonSFPTEta.req(self)
        return {"all": all, "prep": prep}

    def run(self):
        # add muon sf
        weights = []
        corrections = {**self.input()["all"]["muon_sf_files"], **self.input()["prep"]}
        for key, target in corrections.items():
            weights.append(
                self.template.format(prefix="muon_{}_".format(key), hist="*", path=target.path)
            )
        self.ext.add_weight_sets(weights)
        self.finish()


class PlotMuonCorrections(PlotCorrectionsBase):
    def requires(self):
        return MuonCorrections.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()
        pb = tqdm(Map := set(k.replace("_error", "") for k in inp.keys()), total=len(Map), unit="Map")  # fmt: skip
        for m in pb:
            self.plot(inp, self.local_path(), m)
        self.output().touch()

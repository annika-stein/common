# coding: utf-8

from typing import Tuple

import luigi
import luigi.util
import numpy as np
import awkward as ak

from tasks.cmsenv import PileUpReweighting
from tasks.coffea import CoffeaProcessor
from .base import CorrectionsBase
from utils.coffea import BaseProcessor
from coffea import hist

from coffea.hist import Interval
from utils.coffea import mk_dense_evaluator

"""
    SF = corr[self.get_pu_key(events)](events.nTrueInt)
"""


@luigi.util.inherits(CoffeaProcessor)
class PileupCorrections(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "PUCount"
    debug = False
    explorative = False

    def requires(self):
        return dict(
            data=PileUpReweighting.req(self), mc=CoffeaProcessor.req(self, processor=self.processor)
        )

    def run(self):
        self.output().dump(
            PileupSF(
                pu_data={
                    dir: tar.load(formatter="uproot")["pileup"]
                    for dir, tar in self.input()["data"].items()
                },
                pu_mc=self.input()["mc"].load(),
            )
        )


class PileupSF:
    hardmax = 3
    maxshift = 0.0025
    clip_step = 0.95
    clip_min = 5

    def __init__(self, pu_data, pu_mc):
        edges = pu_data["nominal"].axes[0].edges()
        pu_mc = pu_mc[:, Interval(edges[0], edges[-1])]

        self.values = {
            k: mk_dense_evaluator(
                {dir: self.calc_ratio(pud.values(), mc) for dir, pud in pu_data.items()}, edges
            )
            for (k,), mc in pu_mc.values().items()
        }

    def calc_ratio(self, ref, mc):
        assert ref.shape == mc.shape
        sf0 = np.divide(
            np.divide(ref, ref.sum(), dtype=np.float64),
            np.divide(mc, mc.sum(), dtype=np.float64),
            out=np.ones_like(ref, dtype=np.float64),
            where=mc > 0,
        )

        # clip SF according to:
        # https://github.com/cms-nanoAOD/nanoAOD-tools/blob/555b3075892c38b63a98f84527685fa042ffcf59/src/WeightCalculatorFromHistogram.cc#L99-L116

        def clip_shift(maxw) -> Tuple[np.ndarray, float]:
            sfn = np.minimum(sf0, maxw)
            return sfn, ((sfn - sf0) * mc).sum() / (sf0 * mc).sum()

        maxw = min(self.clip_min, sf0.max())
        if maxw <= self.hardmax:
            return sf0
        while maxw > self.hardmax:
            if clip_shift(maxw)[1] > self.maxshift:
                break
            maxw *= self.clip_step

        sf, shift = clip_shift(maxw / self.clip_step)
        return sf * (1.0 - shift)

    def __call__(self, pu_key, nTrueInt):
        pu_sf = self.values[pu_key]
        return dict(
            weight=pu_sf["nominal"](nTrueInt),
            weightUp=pu_sf["up"](nTrueInt),
            weightDown=pu_sf["down"](nTrueInt),
        )


class PUCount(BaseProcessor):
    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc", corrections=False)

    def __init__(self, task):
        super().__init__(task)

        self._accumulator = hist.Hist(
            "nTrueInt",
            hist.Cat("key", "PU key"),
            hist.Bin("nTrueInt", "Number of True interactions", 150, 0, 150),
            # dtype=np.int64,
        )

    def process(self, events):
        output = self.accumulator.identity()
        output.fill(key=self.get_pu_key(events), nTrueInt=ak.to_numpy(events.Pileup.nTrueInt))
        return output

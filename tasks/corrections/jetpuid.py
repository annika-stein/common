# coding: utf-8

from enum import IntEnum
from typing import Dict
import numpy as np
import awkward as ak

from .base import CorrectionsBase
from tasks.files import DownloadFiles
from utils.coffea import Weights, mk_dense_evaluator

"""
    assert wp in ["tight", "medium", "loose"]
    corr[wp](jets, weights)
"""

# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJetID?rev=64#Efficiencies_and_data_MC_scale_f
# https://github.com/latinos/LatinoAnalysis/blob/master/NanoGardener/python/modules/JetSFMaker.py
# https://github.com/latinos/LatinoAnalysis/blob/master/NanoGardener/python/data/JetPUID_cfg.py


class JetPUIdCorrections(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="histo")

    def run(self):
        inp = self.input()["jet_pu_id"]
        eff = inp["POG_eff"].load(formatter="uproot")
        sf = inp["POG_sf"].load(formatter="uproot")

        self.output().dump(
            {
                wp: JetPUIdSF(
                    {
                        kind: dict(
                            eff=eff["h2_%s_mc%s_%s" % (kind, self.year, wp[0].upper())],
                            sf=sf["h2_%s_sf%s_%s" % (kind, self.year, wp[0].upper())],
                            unc=sf["h2_%s_sf%s_%s_Systuncty" % (kind, self.year, wp[0].upper())],
                        )
                        for kind in ["eff", "mistag"]
                    },
                    idx=i,
                )
                for i, wp in enumerate(["tight", "medium", "loose"])
            }
        )


class JetPUIdSF:
    abseta_thresh = 2.5

    def __init__(self, data, idx):
        self.idx = idx

        # check edges
        hists = [(kind, val, h) for kind, dat in data.items() for val, h in dat.items()]
        assert len(hists)
        hist0 = hists[0][-1]
        assert hist0.axes[0].edges()[-1] == 50.0  # last pT edge
        assert hist0.axes[1].edges().min() < 0  # is actually eta
        assert self.abseta_thresh in hist0.axes[1].edges()  # abseta threshold for systematic unc.
        assert -self.abseta_thresh in hist0.axes[1].edges()  # abseta threshold for systematic unc.
        for kind, val, h in hists[1:]:
            for i, ref in enumerate(hist0.axes):
                assert np.all(ref.edges() == h.axes[i].edges()), (
                    kind,
                    val,
                    i,
                    h.axes[i].edges(),
                    ref,
                )

        # dimensions: kind, pass, pt, eta, sf
        #  sf: nominal, up, down
        edges = (
            np.r_[0:3],  # kind: mistag (pu ^= 0), eff (real ^= 1)
            np.r_[0:3],  # pass: 0 (fail), 1 (pass)
        ) + tuple(ax.edges(flow=True) for ax in hist0.axes)

        values = np.ones([len(e) - 1 for e in edges] + [3])

        #
        Ieta = edges[-1].tolist().index
        Mthresh = np.s_[:, Ieta(-self.abseta_thresh) : Ieta(self.abseta_thresh)]

        for ik, kind in enumerate(["mistag", "eff"]):
            sf = data[kind]["sf"].values(flow=True)
            eff = data[kind]["eff"].values(flow=True)
            unc = np.sqrt(data[kind]["sf"].variances(flow=True)) + data[kind]["unc"].values(
                flow=True
            )

            for ip, pfunc in enumerate([lambda x: (1 - x * eff) / (1 - eff), lambda x: x]):
                up = sf + unc
                down = sf - unc

                if kind == "mistag":
                    up[Mthresh] = 1 + np.abs(sf[Mthresh] - 1)
                    down[Mthresh] = 1 - np.abs(sf[Mthresh] - 1)

                for i, x in enumerate([sf, up, down]):
                    values[ik, ip, ..., i] = pfunc(x)

        # overflow
        values[..., 0, :, :] = values[..., -1, :, :] = 1  # pt
        values[..., :, 0, :] = values[..., :, -1, :] = 1  # eta

        self.values = values
        self.lookup = mk_dense_evaluator({True: values}, edges)[True]

    def has_gen(self, jets):
        return jets.genJetIdx >= 0

    def eval(self, jets):
        has = self.has_gen(jets)
        ret = self.lookup(
            ak.values_astype(has, int),
            (jets.puId >> self.idx) & 1,
            jets.pt,
            jets.eta,
        )
        return ak.zip(
            {
                "sf": ret[..., 0],
                "up": ret[..., 1],
                "down": ret[..., 2],
                "has_gen": has,
            }
        )

    @staticmethod
    def apply(res, weights, name="jet_PUid", split=True):
        if split:
            for suffix, mask in dict(
                efficiency=res.has_gen,
                mistag=~res.has_gen,
            ).items():
                weights.add(
                    name="%s_%s" % (name, suffix),
                    weight=ak.to_numpy(ak.prod(res.sf[mask], axis=-1)),
                    weightUp=ak.to_numpy(ak.prod(res.up[mask], axis=-1)),
                    weightDown=ak.to_numpy(ak.prod(res.down[mask], axis=-1)),
                )
        else:
            weights.add(
                name=name,
                weight=ak.to_numpy(ak.prod(res.sf, axis=-1)),
                weightUp=ak.to_numpy(ak.prod(res.up, axis=-1)),
                weightDown=ak.to_numpy(ak.prod(res.down, axis=-1)),
            )

    def __call__(self, jets, weights, **kwargs):
        self.apply(self.eval(jets), weights, **kwargs)


class JetPUidHelper:
    def __init__(self, sfs: Dict[str, JetPUIdSF], jets: "Jets") -> None:
        """
        Helper for selecting `jets` w.r.t. JetPUId. Tracks information to apply the JetPUIdSF
        accordingly from `sfs` (the `JetPUIdCorrections` output).
        """
        self.sfs = sfs
        self.jets = jets
        self.noPUid = jets.pt > 50
        self.wp = None
        self.cuts = False

    class WP(IntEnum):
        tight = 0
        medium = 1
        loose = 2

    def select(self, cuts: "ak.Array[bool]", wp: "WP | ak.Array[int]") -> "Jets":
        """
        Selects Jets according to `cuts` and the given JetPUId `wp`.
        Will also track information needed to calculate JetPUIdSF.
        """
        if isinstance(wp, int):
            wp = self.WP(wp)
        assert np.all(0 <= wp) and np.all(wp <= max(self.WP.__members__.values()))

        if self.wp is None:
            self.wp = wp
        elif self.wp is wp:
            pass
        else:
            if np.any(both := self.cuts & cuts):
                assert ak.all(~both | self.noPUid | (self.wp == wp)), "inconsistent PUId WPs"
            self.wp = ak.where(cuts, wp, self.wp)

        self.cuts = self.cuts | cuts

        pu_cut = ak.values_astype(((self.jets.puId >> wp) & 1), bool)
        return self.jets[cuts & (pu_cut | self.noPUid)]

    def applySF(self, weights: Weights, **kwargs):
        """
        Applies the JetPUIdSF onto `weights` according to all previous `select` usages.
        All `kwargs` are passed to `JetPUIdSF.apply`.
        """
        acc = acc0 = ak.zip({"sf": 1, "up": 1, "down": 1, "has_gen": False})
        if self.wp is None:
            pass
        elif isinstance(self.wp, int):
            acc = self.sfs[self.WP(self.wp).name].eval(self.jets)
        else:
            for wp, i in self.WP.__members__.items():
                if np.any(active := ((self.wp == i) & self.cuts)):
                    acc = ak.where(active, self.sfs[wp].eval(self.jets), acc)
        if acc is acc0:
            acc = ak.broadcast_arrays(acc, self.jets.pt)
        JetPUIdSF.apply(acc, weights, **kwargs)

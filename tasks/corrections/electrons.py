# coding: utf-8

from tqdm.auto import tqdm

from tasks.files import DownloadFiles

from .base import CorrectionsBase, PlotCorrectionsBase, SFPTEta


class ElectronSFPTEta(SFPTEta):
    particle = "electron"


class ElectronCorrections(CorrectionsBase):
    def requires(self):
        all = DownloadFiles.req(self, type="histo")
        prep = {}
        if "electron_sf_pteta_files" in all.files["histo"]:
            name = sorted(all.files["histo"]["electron_sf_pteta_files"].keys())[0]
            prep[name] = ElectronSFPTEta.req(self)
        return {"all": all, "prep": prep}

    def run(self):
        # add electron sf
        weights = []
        corrections = {**self.input()["all"]["electron_sf_files"], **self.input()["prep"]}
        for key, target in corrections.items():
            weights.append(
                self.template.format(prefix="electron_{}_".format(key), hist="*", path=target.path)
            )
        self.ext.add_weight_sets(weights)
        self.finish()

    def mod_evaluator(self, evaluator):
        for key in evaluator.keys():
            if key.endswith("_error"):
                continue
            ekey = f"{key}_error"
            if ekey not in evaluator:
                continue
            val = evaluator[key]._values
            err = evaluator[ekey]._values
            bad = (val == 1) & (err == 1)
            if bad.any():
                print(f"{key}: {bad.sum()} invalid values set to 1+-0")
                val[bad] = 1
                err[bad] = 0
        return evaluator


class PlotElectronCorrections(PlotCorrectionsBase):
    def requires(self):
        return ElectronCorrections.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()
        pb = tqdm(Map := set(k.replace("_error", "") for k in inp.keys()), total=len(Map), unit="Map")  # fmt: skip
        for m in pb:
            self.plot(inp, self.local_path(), m)
        self.output().touch()

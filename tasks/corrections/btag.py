# coding: utf-8

from collections import ChainMap

import awkward as ak  # noqa
import coffea.processor.accumulator as ACC
import hist
import law
import luigi
import luigi.util
import numpy as np

from processor import util  # noqa
from tasks.coffea import CoffeaProcessor
from tasks.files import DownloadFiles
from utils.coffea import BaseProcessor, hist_accumulator
from utils.csv_reader import BTagSFCSVReader

from .base import CorrectionsBase


class BTagCorrections(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="btag")

    def run(self):
        self.output().dump(law.util.map_struct(lambda tar: BTagSFCSVReader(tar.path), self.input()))


@luigi.util.inherits(CoffeaProcessor)
class BTagNormCorrections(CorrectionsBase):
    version = CoffeaProcessor.version
    processor = "BTagSFNorm"
    debug = False
    explorative = False

    def store_parts(self):
        return super().store_parts() + (self.analysis_choice, self.recipe)

    def requires(self):
        return CoffeaProcessor.req(self, processor=self.processor)

    def calculate(self, v0, v1):
        return np.divide(v0, v1, where=(v0 != 0) & (v1 != 0), out=np.ones_like(v0))

    def run(self):
        nJet = self.input().load()["nJet"]
        nj0 = nJet[0].hist.to_hist()
        nj1 = nJet[1].hist.to_hist()
        assert nj0.shape == nj1.shape
        self.put_dense(
            {
                d: self.calculate(
                    nj0[d, ...].view()["value"],
                    nj1[d, ...].view()["value"],
                )
                for d in nj0.axes["dataset"]
            },
            nj0.axes["nJet"].edges,
        )


class BTagSFNorm(BaseProcessor):
    njets = "njets"
    btagWeight = "btagWeight"

    jes_shifts = False
    individual_weights = True

    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc", btagnorm=False, hhreweigthing=False)

    def __init__(self, task):
        super().__init__(task)
        self._accumulator["nJet"] = ACC.dict_accumulator(
            {
                sf: hist_accumulator(
                    hist.Hist(
                        hist.axis.StrCategory(
                            [], name="dataset", label="Primary dataset", growth=True
                        ),
                        hist.axis.Integer(0, 10, name="nJet", label="Number of Jets"),
                        storage=hist.storage.Weight(),
                    )
                )
                for sf in (0, 1)
            }
        )

    def process(self, events):
        select_output = next(self.select(events))
        assert select_output["unc"] == "nominal"

        dsname = self.get_dataset(events).name
        categories = select_output["categories"]
        selection = select_output["selection"]
        weights = select_output["weights"]
        output = select_output["output"]

        if callable(self.njets):
            njets = self.njets(select_output)
        else:
            njets = eval(
                self.analysis_inst.variables.get(self.njets).expression,
                globals(),
                ChainMap({}, select_output),
            )

        cut = False
        for cuts in categories.values():
            cut |= selection.all(*cuts)
        if not np.any(cut):
            cut = np.s_[:1]
            njets = [-1]

        for o, w in [
            (output["nJet"][0], weights.partial_weight(exclude=[self.btagWeight])),
            (output["nJet"][1], weights.weight()),
        ]:
            o.hist.fill(weight=w[cut], nJet=njets[cut], dataset=dsname)

        return output


def BTagSF_reshape(processor, events, weights, jets, shift, unc):
    """Helper for applying DeepFlavour reshape BTagSF"""
    dataset_inst = processor.get_dataset(events)
    if not dataset_inst.is_mc:
        return

    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods
    # AK4 Jet reshape SF
    corr = processor.corrections["btag"]["deepjet"][
        "POG_reduced"
        + (
            "_TuneCP5"
            if int(processor.year) == 2016 and "_TuneCP5_" in processor.get_lfn(events, default="")
            else ""
        )
    ].get("reshape")
    if shift is None:
        shifts = list(processor.analysis_inst.aux.get("btag_sf_shifts", []))
        # shifts = [] # no shifts -> speedup
        c = "central"
        corr = corr.reduce(shifts=shifts)
    else:
        shifts = []
        if unc in ("jer", "UnclustEn"):
            c = "central"
        else:
            # flip up/down direction for certain jec shifts, since they changed direction:
            # ReducedJECv1 (used for BTagSF) <-> ReducedJECv2 (applied here)
            # https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources#Run_2_reduced_set_of_uncertainty
            # if unc in ("RelativeSample", "RelativeBal"):
            if unc in ("FlavorQCD", "RelativeBal") or unc.startswith(
                ("RelativeSample_", "EC2")  # these are suffixed by their year
            ):
                btag_shift = dict(up="down", down="up")[shift]
            else:
                btag_shift = shift
            btag_unc = "HEMIssue" if unc == "HemIssue" else unc
            c = f"{btag_shift}_jes{btag_unc}"
        assert c in corr.shifts, (c, corr.shifts)
        corr = corr.reduce(shifts=[c], updown=False)
    sfs = ak.prod(corr.eval_awk1(obj=jets, discr="btagDeepFlavB"), axis=-1)
    sf0 = sfs[c]
    weights.add("btagWeight", sf0)
    for btag_shift in shifts:
        weights.add(
            "btagWeight_%s" % btag_shift,
            ak.ones_like(sf0),
            weightUp=sfs[f"up_{btag_shift}"] / sf0,
            weightDown=sfs[f"down_{btag_shift}"] / sf0,
        )

    if "btagnorm" in processor.corrections:
        btagnorm = processor.corrections["btagnorm"][dataset_inst.name]
        weights.add("btagNorm", btagnorm(ak.num(jets)))

# coding: utf-8

import operator
from copy import copy
from dataclasses import dataclass
from operator import methodcaller
from typing import Callable, Optional, Tuple

import awkward as ak
import law
import numpy as np
import vector
from awkward._connect._numpy import array_ufunc

from tasks.files import DownloadFiles
from utils.jec import JECJet, JECStack, JERJets

from .base import CorrectionsBase


def make_files(campaign, jet_types=None, stem=False):
    if jet_types is all:  # all available
        jet_types = [f"AK{k}PF{clean}" for k in [4, 8] for clean in ["", "chs", "Puppi"]]
    if jet_types is None:  # all useful (for NanoAOD)
        jet_types = ["AK4PFchs", "AK8PFPuppi"]

    jer_type = campaign.aux["jer_type"]
    jer_version = campaign.aux["jer_version"]
    jes_version = campaign.aux["jes_version"]
    jes_levels = campaign.aux["jes_levels"]
    junc_format = campaign.aux.get("junc_format", "{version}_UncertaintySources_{type}")

    def url(db, dir, fn):
        if stem:
            return fn
        return f"https://raw.githubusercontent.com/cms-jet/{db}/master/textFiles/{dir}/{fn}.txt"
        # return f"https://github.com/cms-jet/{db}/blob/master/textFiles/{dir}/{fn}.txt"

    # JEC
    ret = dict(
        jec={
            jt: {
                run: {
                    jl: url("JECDatabase", jv, f"{jv}_{jl}_{jt}")
                    for jl in jes_levels["mc" if run == "mc" else "data"]
                }
                for run, jv in jes_version.items()
            }
            for jt in jet_types
        },
        junc={
            jt: url(
                "JECDatabase",
                jes_version["mc"],
                junc_format.format(version=jes_version["mc"], type=jt),
            )
            for jt in jet_types
        },
    )

    # JER(SF)
    for key, type in [("jr", jer_type), ("jersf", "SF")]:
        ret[key] = {
            jt: url("JRDatabase", f"{jer_version}_MC", f"{jer_version}_MC_{type}_{jt}")
            for jt in jet_types
        }

    return ret


class JetCorrections(CorrectionsBase):
    def requires(self):
        return dict(
            jec=DownloadFiles.req(self, type="jec"),
            junc=DownloadFiles.req(self, type="junc"),
            jer=DownloadFiles.req(self, type="jr"),
            jersf=DownloadFiles.req(self, type="jersf"),
        )

    def run(self):
        self.ext.add_weight_sets(
            set(
                self.template.format(prefix="*", hist="*", path=t.path)
                for t in law.util.flatten(self.input())
            )
        )

        self.ext.finalize()
        ev = self.ext.make_evaluator()

        jet_types = self.campaign_inst.aux.get("jet_types", None)
        keys = make_files(self.campaign_inst, jet_types=jet_types, stem=True)
        jet_types = list(keys["jec"].keys())

        jes_sources = self.campaign_inst.aux["jes_sources"]

        def mk(keys):
            return JECStack.assemble({k: ev[k] for k in keys})

        out = JECCorrector()
        for jt in jet_types:
            jec_data = {
                run: mk(levels.values()) for run, levels in keys["jec"][jt].items() if run != "mc"
            }
            jec_mc = mk(
                [
                    # JEC
                    *keys["jec"][jt]["mc"].values(),
                    # JER
                    keys["jr"][jt],
                    # JER SF
                    keys["jersf"][jt],
                    # JES SHIFTS
                    *[f"{keys['junc'][jt]}_{src}" for src in jes_sources],
                ],
            )
            out[jt] = jec_data
            out[jt]["mc"] = jec_mc

        self.output().dump(out)


@dataclass
class JECObject:
    obj: ak.Array
    gen: Optional[ak.Array]
    rho: ak.Array
    stack: JECStack
    dRcone: float
    metName: str = "MET"
    metT1: bool = True
    output: bool = True

    met = None
    objEMok = None
    unclEnThreshold = 15
    skipEMfractionThreshold = 0.9

    @staticmethod
    def _gen_clip(obj, dRmax):
        return ak.fill_none(
            ak.where(obj.delta_r(obj.matched_gen) < dRmax, obj.matched_gen.pt, 0), 0
        )

    @staticmethod
    def _gen_pt(obj, gen, dRmax):
        pairs = ak.cartesian((obj, gen), axis=1, nested=True)
        pobj, pgen = ak.unzip(pairs)

        dr = pobj.delta_r(pgen)

        return ak.max(pgen.pt[dr < dRmax], axis=-1, initial=0, mask_identity=False)

    @staticmethod
    def _normal(ref):
        seeds = ref.view("i4")
        gen = np.random.Generator(np.random.PCG64(seeds[:20]))
        return gen.normal(size=ref.shape).astype(ref.dtype)

    @property
    def fix17(self):
        return self.metName == "METFixEE2017"

    def _make_raw(self):
        entries = {
            dst: self.obj[src]
            for dst, src in {
                # initialized from rawPt anyway
                # "JetPt": "pt",
                # "JetE": "energy",
                # "JetMass": "mass",
                "JetPhi": "phi",  # for HemIssue
                "JetEta": "eta",
                "JetA": "area",
            }.items()
        }

        if hasattr(self.obj, "jetId"):
            entries["JetId"] = self.obj.jetId

        # ptGenJet
        if hasattr(self.obj, "matched_gen"):
            entries["ptGenJet"] = ak.virtual(
                self._gen_clip, args=(self.obj, self.dRcone / 2), cache="new"
            )
        elif self.gen is not None:
            entries["ptGenJet"] = ak.virtual(
                self._gen_pt, args=(self.obj, self.gen, self.dRcone / 2), cache="new"
            )
        else:
            entries["ptGenJet"] = ak.virtual(ak.zeros_like, args=(self.obj.eta,), cache="new")

        def put(what: str, value):
            entries[f"Jet{what.capitalize()}"] = entries[f"{what.lower()}Raw"] = value

        if hasattr(self.obj, "rawPt"):
            put("pt", self.obj.rawPt)
            put("e", self.obj.rawPt)
            put("mass", ak.virtual(ak.zeros_like, (self.obj.rawPt,)))
        elif hasattr(self.obj, "rawFactor"):
            f = 1 - self.obj["rawFactor"]
            for key, val in dict(pt=self.obj.pt, e=self.obj.energy, mass=self.obj.mass).items():
                put(key, f * val)
        else:
            raise RuntimeError("no rawPt available/calculateable")

        entries["Rho"] = ak.broadcast_arrays(self.rho, self.obj.phi)[0]

        return ak.zip(entries, with_name=JECJet.__name__)

    def __post_init__(self):
        jRaw = self._make_raw()

        for lvl, jJEC, fJEC in self.stack.jec.correct_gen(jRaw):
            if lvl.startswith("L1"):
                fL1 = fJEC

        JER = jJEC.makeJER(
            stack=self.stack,
            normals=array_ufunc(self._normal, "__call__", (jRaw.ptRaw,), {}),
        )

        self.jet = jJEC.applyJER(JER)

        if self.metT1:
            muPt = jRaw.JetPt * self.obj.muonSubtrFactor
            mRaw = jRaw.addPt(-muPt)
            mMu0 = mRaw.scale(fJEC)
            mJet = mMu0.addPt(muPt)

            EMthresh = self.skipEMfractionThreshold
            good = mMu0.JetPt > self.unclEnThreshold

            if self.fix17:
                aeta = abs(jRaw.JetEta)
                good_alt = mJet.JetPt > self.unclEnThreshold

                isEE = (2.65 < aeta) & (aeta < 3.14) & (jRaw.JetPt < 50) & good_alt
                # not unclEnThreshold for EE jets (why?)
                EMthresh = ak.where(isEE, 1.1, self.skipEMfractionThreshold)

                good = ak.where(isEE, good_alt, good)

            if hasattr(self.obj, "neEmEF") and hasattr(self.obj, "chEmEF"):
                good = good & ((self.obj.neEmEF + self.obj.chEmEF) < EMthresh)

            met = JERJets(*(x[good] for x in mJet.applyJER(JER)))
            v2L1 = self.asVec2(mRaw.scale(fL1).addPt(muPt))[good]

            if self.fix17:  # do not apply JER for EE jets (why??)
                met = JERJets(*(ak.where(isEE[good], mJet[good], x) for x in met))

            met = JERJets(*map(ak.packed, met))
            v2L1 = ak.packed(v2L1)

            self.met = met
            self.v2L1 = v2L1

    def asVec2(self, jet: JECJet) -> vector.Vector2D:
        return vector.awk({"pt": jet.JetPt, "phi": jet.JetPhi}).to_xy()

    def realize(
        self, func: Callable[[JECStack, JERJets], JECJet]
    ) -> Tuple[Optional[ak.Array], Optional[ak.Array]]:
        if self.output:
            val = func(self.stack, self.jet)

            ret = copy(self.obj)
            ret["pt"] = val["JetPt"]
            ret["mass"] = val["JetMass"]
        else:
            ret = None

        if self.metT1:
            val = func(self.stack, self.met)

            jetDelta = self.asVec2(val) - self.v2L1
            metDelta = vector.awk(ak.sum(jetDelta, axis=1))
        else:
            metDelta = None

        return ret, metDelta


class JECCorrector(dict):
    def generate(self, evt, run: str, met="MET", shifts=()):
        dirs = "up", "down"
        common = dict(rho=evt.fixedGridRhoFastjetAll, metName=met)
        objs = [
            JECObject(
                obj=evt.Jet,
                gen=getattr(evt, "GenJet", None),
                **common,
                stack=self["AK4PFchs"][run],
                dRcone=0.4,
            ),
            JECObject(
                obj=evt.FatJet,
                gen=getattr(evt, "GenJetAK8", None),
                **common,
                stack=self["AK8PFPuppi"][run],
                dRcone=0.8,
                metT1=False,
            ),
            # JECObject(
            #     obj=evt.CorrT1METJet,
            #     gen=None,
            #     **common,
            #     stack=self["AK4PFchs"][run],
            #     dRcone=0.4,
            #     output=False,
            # ),
        ]

        shifts = set(shifts)
        juncs = shifts.intersection(
            *(set(obj.stack.junc.levels) if obj.stack.junc else set() for obj in objs)
        )
        shifts -= juncs

        doJER = "jer" in shifts and all(obj.stack.jer and obj.stack.jersf for obj in objs)
        doUnc = "UnclustEn" in shifts
        doHEM = "HemIssue" in shifts
        shifts -= {"jer", "UnclustEn", "HemIssue", "nominal"}

        if juncs and (
            missing := (set.union(*(juncs - set(obj.stack.junc.levels) for obj in objs)))
        ):
            missing = ", ".join(sorted(missing))
            raise RuntimeError(f"Some JECUncertaintySources not available for all Jets: {missing}")

        rawmet = vector.awk(evt.RawMET).to_xy()
        if met == "METFixEE2017":
            rawmet = rawmet + vector.awk(evt.METFixEE2017).to_xy() - vector.awk(evt.MET).to_xy()

        def bake(func, withVec2=False):
            ret = [copy(getattr(evt, met))]
            newmet = rawmet

            for jet, metDelta in map(methodcaller("realize", func), objs):
                if jet is not None:
                    ret.append(jet)
                if metDelta is not None:
                    newmet = newmet - metDelta

            ret[0]["pt"] = newmet.rho
            ret[0]["phi"] = newmet.phi

            if withVec2:
                ret.insert(0, newmet)

            return ret

        # nominal
        metVec2, metArr, *jets = bake(lambda stack, jer: jer.nominal, withVec2=True)
        yield ("nominal", None, metArr, *jets)

        # addtitional shifts (with nominal content)
        for shift in shifts:
            for ud in dirs:
                yield (shift, ud, metArr, *jets)

        if doUnc:
            uncVec2 = vector.awk(
                {key.lower(): getattr(metArr, f"MetUnclustEnUpDelta{key}") for key in "XY"}
            )
            for op, ud in zip([operator.add, operator.sub], dirs):
                vec2 = op(metVec2, uncVec2)
                metUnc = copy(metArr)
                metUnc["pt"] = vec2.rho
                metUnc["phi"] = vec2.phi

                yield ("UnclustEn", ud, metUnc, *jets)

        if doHEM:
            yield ("HemIssue", "up", metArr, *jets)
            yield ("HemIssue", "down", *bake(self.HemIssue))

        del metArr, jets

        if doJER:
            for ud in dirs:
                yield ("jer", ud, *bake(lambda stack, jer: getattr(jer, ud)))

        # TODO: add HemIssue unc.

        for unc in sorted(juncs):
            for i, ud in enumerate(dirs):
                yield (unc, ud, *bake(lambda stack, jer: stack.junc.correct(unc, i, jer.nominal)))

    @staticmethod
    def HemIssue(stack, jer):
        # https://hypernews.cern.ch/HyperNews/CMS/get/JetMET/2000.html
        jet = jer.nominal

        if not hasattr(jet, "JetId"):
            return jet

        delta = ak.ones_like(jet.JetPt)
        tight = ak.values_astype(jet.JetId & 2, bool)
        cond = (jet.JetPt > 15) & (-1.57 < jet.JetPhi) & (jet.JetPhi < -0.87) & tight
        for etaMin, etaMax, factor in [(-3.0, -2.5, 0.65), (-2.5, -1.3, 0.8)]:
            delta = ak.where(cond & (etaMin < jet.JetEta) & (jet.JetEta < etaMax), factor, delta)

        return jet.scale(delta)

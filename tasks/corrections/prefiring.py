# coding: utf-8

import numpy as np
from tqdm.auto import tqdm

from processor.util import reduce_or
from tasks.files import DownloadFiles

from .base import CorrectionsBase, PlotCorrectionsBase


class PrefiringCorrections(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="histo")

    def run(self):
        from coffea.util import save

        self.ext.add_weight_sets(
            [
                self.template.format(prefix="*", hist="*", path=prefiring.path)
                for prefiring in self.input()["l1_ecal_prefiring"]
            ]
        )
        self.ext.finalize()
        evaluator = self.ext.make_evaluator()
        out = PrefiringApply(evaluator, year=self.year)
        self.output().parent.touch()
        save(out, self.output().path)


class PrefiringApply(object):
    # ref: https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/PatUtils/plugins/L1ECALPrefiringWeightProducer.cc
    def __init__(self, corr, year, syst=0.2):
        # store corr for plotting
        self.corr = corr
        if year == "2017":
            self.runname = "2017BtoF"
        elif year == "2016":
            self.runname = "2016BtoH"
        else:
            raise Exception("There is no prefiring issue in 2018")
        self.syst = syst
        for kind in ["photon", "jet", "jetem"]:
            setattr(
                self,
                kind,
                (
                    corr[f"L1prefiring_{kind}pt_{self.runname}"],
                    corr[f"L1prefiring_{kind}pt_{self.runname}_error"],
                ),
            )

    def _get_part(self, kind, obj, dir):
        nom, err = getattr(self, kind)
        pt = obj.pt * (obj.chEmEF + obj.neEmEF) if kind == "jetem" else obj.pt
        w = nom(obj.eta, pt)
        if dir:
            e = err(obj.eta, pt)
            if self.syst:
                e = (e ** 2 + self.syst ** 2) ** 0.5
            w = np.minimum(w + e, 1) if dir > 0 else np.maximum(w - e, 0)
        return 1 - w

    def __call__(self, jet, photon, dir=0, jetem=False):
        dir = int(np.sign(dir))

        photon = photon[
            ~reduce_or(
                photon.pt < 20,
                photon.eta < 2,
                photon.eta > 3,
            )
        ]

        jet = jet[
            ~reduce_or(
                jet.pt < 20,
                jet.eta < 2,
                jet.eta > 3,
            )
        ]

        p_w = self._get_part("photon", photon, dir)
        j_w = self._get_part("jetem" if jetem else "jet", jet, dir)

        from IPython import embed

        embed()
        jp = jet.cross(photon, nested=True)
        # ak.prod([jet.metric_table(photon) <= 0.4], axis=-1)
        # ak.prod(ak.unzip(ak.cartesian([events.Jet, events.Photon], nested=True))[0][events.Jet.metric_table(events.Photon) <= 0.4], axis=-1)
        jp_w = jet.cross(p_w, nested=True).i1[jp.i0.delta_r(jp.i1) <= 0.4].prod()
        assert np.isfinite(jp_w).all().all()

        return p_w.prod() * (j_w / np.maximum(jp_w, jp_w > j_w)).prod()


class PlotPrefiringCorrections(PlotCorrectionsBase):
    def requires(self):
        return PrefiringCorrections.req(self)

    def run(self):
        self.output().parent.touch()
        inp = self.input().load()
        pb = tqdm(
            Map := set(k.replace("_error", "") for k in inp.corr.keys()),
            total=len(Map),
            unit="Map",
        )
        for m in pb:
            self.plot(inp.corr, self.local_path(), m)
        self.output().touch()

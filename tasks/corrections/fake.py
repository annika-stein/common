# coding: utf-8
from tasks.files import DownloadFiles

from .base import CorrectionsBase


class Fake(CorrectionsBase):
    def requires(self):
        return DownloadFiles.req(self, type="histo")

    @property
    def available(self):
        return self.__class__.__name__ in self.analysis_inst.aux.get("non_common_corrections", [])

    def run(self):
        self.ext.add_weight_sets(
            [
                self.template.format(prefix=f"{key}_", hist="*", path=target.path)
                for key, target in self.input()["fake"].items()
            ]
        )
        self.finish()
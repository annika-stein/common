# -*- coding: utf-8 -*-

import json
from collections import defaultdict
from functools import reduce
from hashlib import sha1
from importlib import import_module
from pathlib import Path
from typing import Dict, Tuple

import hist
import law
import luigi
import numpy as np
import uproot as uproot4
import uproot3

from tasks.base import AnalysisTask, CombinationTask, DHITask, EOSFileUpload
from tasks.datacard import DatacardProducer, DatacardsVariablesWrapper, DatacardsWrapper
from tasks.mixins import CreateIssueMixin, ModelMixin, RecipeMixin
from tasks.plotting import CheckSystsCombined
from utils.bh5 import Histogram as Hist5
from utils.uproot import decycle
from utils.util import outputs_to_issue

years = [2016, 2017, 2018]


class FitDiagnostics(DHITask, DatacardProducer):
    def requires(self):
        return DatacardProducer.req(self, transfer=True)

    outmap = {"fitdiagnostics.root": "*/fitdiagnostics__*.root"}

    def command(self):
        return [
            "FitDiagnostics",
            "--version",
            self.version,
            "--datacards",
            self.remote_path(self.input()["datacard_eos"]),
            "--skip-save",
            "Toys",
            "--custom-args",
            "--ignoreCovWarning --skipSBFit",
        ]


class FitDiagnosticsWrapper(DatacardsWrapper):
    _task = FitDiagnostics


class FitDiagnosticsVariablesWrapper(DatacardsVariablesWrapper):
    _producer = FitDiagnostics
    # maybe set category by default to "all_incl_sr"


class DHI(DHITask, ModelMixin, RecipeMixin):
    outmap = {"plot.pdf": "*.pdf", "plot.png": "*.png"}
    no_poll = luigi.BoolParameter(significant=False)
    unblind = luigi.BoolParameter()

    @property
    def datacards_hash(self):
        return sha1(json.dumps(self.datacards, sort_keys=True).encode("utf8")).hexdigest()[:10]

    def complete(self):
        try:
            self.datacards_hash
        except:
            return False
        return super().complete()

    def store_parts(self):
        parts = super().store_parts()
        parts += (self.datacards_hash,)
        return parts

    def base_command(self, name=None, workflows=()):
        if name is None:
            name = type(self).__name__
        ret = [
            name,
            "--hh-model",
            "hh_model.model_default",
            "--Snapshot-workflow",
            "local",
            "--use-snapshot",
            "--Snapshot-custom-args",
            " --setParameterRanges r=-100,100",
            "--version",
            f"{self.version}__{self.model_version}",
            "--campaign",
        ]
        if hasattr(self, "years"):
            if len(self.years) == 1:  # 1 year
                ret += [str(self.years[0])]
            elif len(self.years) == 3 and (
                tuple(sorted(self.years)) == ("2016", "2017", "2018")
            ):  # FR 2
                ret += ["run2"]
            else:
                ret += [f"({', '.join(self.years)})"]  # any 2-year combination
        else:
            ret += [self.model_version]
        if not isinstance(workflows, dict):
            workflows = {wf: None for wf in workflows}
        for wf, runtime in workflows.items():
            ret += [f"--{wf}-workflow", "htcondor", f"--{wf}-poll-interval", "300sec"]
            if runtime and runtime is not True:
                ret += [f"--{wf}-max-runtime", runtime]
            if self.no_poll:
                ret.append(f"--{wf}-no-poll")

        return ret

    def wrapper2paths(self, wrapper, prefix=""):
        if isinstance(wrapper, dict):
            return sum((self.wrapper2paths(w, prefix=y) for y, w in wrapper.items()), [])
        elif isinstance(wrapper, (list, tuple)):
            return sum((self.wrapper2paths(w, prefix=prefix) for w in wrapper), [])
        else:
            _prefix = (
                lambda p, s: f"y{p}_{s}" if p.startswith(("2016", "2017", "2018")) else f"{p}_{s}"
            )
            return sorted(
                f"{_prefix(prefix, key)}={self.remote_path(output['datacard_eos'])}"
                for key, output in wrapper.input().items()
            )


class BlindingMixin:
    unblind = luigi.BoolParameter()

    def store_parts(self):
        if self.unblind:
            blind = "unblind"
        else:
            blind = "blind"

        return super().store_parts() + (blind,)


class DatacardProvider(BlindingMixin, CombinationTask):
    def requires(self):
        return {
            f"{analysis}__{year}": DatacardsWrapper.req(
                self,
                year=str(year),
                analysis_choice=analysis,
                transfer=True,
                categories=self.analysis_categories[analysis],
            )
            for analysis in self.analyses
            for year in self.years
        }

    @property
    def datacards(self):
        def condition(outp):
            return isinstance(outp, dict) and all(
                isinstance(o, (DatacardsWrapper, EOSFileUpload)) for o in outp.values()
            )

        req = {
            k: v
            for k, v in self.deep_requires(condition).items()
            if isinstance(v, DatacardsWrapper)
            and k in [f"{analysis}__{year}" for analysis in self.analyses for year in self.years]
        }
        # individual
        datacards = {
            key: self.wrapper2paths(
                value,
                prefix=key,
            )
            for key, value in req.items()
        }
        # all years
        if len(self.years) > 1:
            for year in self.years:
                datacards[year] = self.wrapper2paths(
                    [value for k, value in req.items() if year in k],
                    prefix=year,
                )
        # all analysis
        if len(self.analyses) > 1:
            for analysis in self.analyses:
                datacards[analysis] = self.wrapper2paths(
                    [value for k, value in req.items() if analysis in k],
                    prefix=analysis,
                )
        # in case 1 year and 1 analysis is given we can have the categories individually:
        if len(self.analyses) == len(self.years) == 1:
            analysis = self.analyses[0]
            year = self.years[0]
            datacards.update(
                {
                    p.split("=")[0].replace(f"{analysis}__{year}_", "", 1): [p]
                    for p in self.wrapper2paths(req)
                }
            )
        # all combined
        datacards["combined"] = self.wrapper2paths(req)
        return datacards


class FlatDatacardsCommand:
    @property
    def datacards(self):
        datacards = super().datacards
        return datacards["combined"]

    def base_command(self, name=None, workflows=()):
        ret = super().base_command(name=name, workflows=workflows)
        ret += ["--datacards", ",".join(self.datacards)]
        return ret

    def _repr_flags(self):
        flags = super()._repr_flags()
        flags[-1] = flags[-1] + " (flat)"
        return flags


class MultiDatacardsCommand:
    @property
    def datacards(self):
        datacards = super().datacards
        if len(self.years) == len(self.analyses) == 1:
            datacards.pop("combined", 1)
            return datacards
        # remove individual if multiple analyses are given
        if len(self.analyses) > 1:
            return {k: v for k, v in datacards.items() if k in k in self.analyses + ("combined",)}
        else:
            if len(self.years) > 1:
                return {k: v for k, v in datacards.items() if k in k in self.years + ("combined",)}

    def base_command(self, name=None, workflows=()):
        datacards = self.datacards
        ret = super().base_command(name=name, workflows=workflows)
        ret += [
            "--multi-datacards",
            ":".join(map(",".join, datacards.values())),
            "--datacard-names",
            ",".join(datacards.keys()),
        ]
        if not self.no_poll:
            ret += ["--workers", str(len(datacards.keys()))]
        return ret

    def _repr_flags(self):
        flags = super()._repr_flags()
        flags[-1] = flags[-1] + " (multi)"
        return flags


class FlatDatacardsTask(FlatDatacardsCommand, DHI, DatacardProvider):
    pass


class MultiDatacardsTask(MultiDatacardsCommand, DHI, DatacardProvider):
    pass


class FitDiagnosticsCombined(FlatDatacardsTask):
    outmap = {"fitdiagnostics.root": "*/fitdiagnostics__*.root"}
    priority = 200

    def command(self):
        bc = self.base_command(name="FitDiagnostics", workflows=dict(FitDiagnostics="96h"))
        idx = bc.index("--campaign")
        del bc[idx : idx + 2]
        bc += [
            "--unblinded",
            "--skip-save",
            "Toys",
        ]
        # Caution with '--ignoreCovWarning':
        # this option can lead to unnoticed
        # incorrect results if the fit fails
        if self.unblind:
            bc += [
                "--skip-b-only",
                "False",
                "--custom-args",
                "--ignoreCovWarning --skipSBFit",
            ]
        else:
            bc += [
                "--skip-b-only",
                "True",
                "--custom-args",
                "'--ignoreCovWarning'",
            ]
        return bc


class FitDiagnosticsPostprocessed(CombinationTask):
    def requires(self):
        raise NotImplementedError()

    def output(self):
        return self.local_target("fitdiagnostics_postprocessed.coffea")

    def cat2vax_pmap(self, cat: str) -> Tuple[hist.axis.AxisProtocol, Dict[str, str]]:
        """
        Returns a tuple of:
            - the variable axis for the given category
            - a dict to translate datacard processes into aci.Process names
        """
        analysis, campaign, category = self.cat2acc(cat)

        (dcprod,) = self.find_datacard_reqs(category=category.name, year=str(campaign.aux["year"]))
        variable = analysis.variables.get(dcprod.variable)
        horig = dcprod.input().load()
        vax = horig[variable.name][category.name].axes[-1]

        model = analysis.aux.get("stat_model", self.model)
        module = ".".join([dcprod.analysis_choice, "models", model])
        sm = getattr(import_module(module), self.statmodel)
        pmap = {v: k for k, v in sm.rprocesses.fget(None).items()}

        return vax, pmap

    def find_datacard_reqs(self, **kwargs):
        reqs = [self]
        while not any(isinstance(r, DatacardProducer) for r in reqs):
            reqs = law.util.flatten([r.requires() for r in reqs])
        return [
            r
            for r in reqs
            if isinstance(r, DatacardProducer)
            and all(getattr(r, k, None) == v for k, v in kwargs.items())
        ]

    @law.decorator.safe_output
    def run(self):
        postprocessed_hists = defaultdict(dict)

        with uproot3.open(self.input().path) as f:
            work = [
                (decycle(shape), decycle(cat))
                for shape, d in f.items(
                    filtername=lambda x: x.startswith(
                        b"shapes_" if self.unblind else b"shapes_prefit"
                    )
                )
                for cat, cls in d.classnames()
                if cls == "TDirectory"
            ]
            for shape, cat in work:
                vax, pmap = self.cat2vax_pmap(cat)

                d = f[shape][cat]
                procs = [
                    pmap.get(decycle(p), decycle(p))
                    for p in d.keys(filterclass=lambda cls: cls.__name__.startswith("TH1"))
                ]
                if "data" in d:
                    assert "data_err" not in d
                    procs += ["data", "data_err"]

                hnew = hist.Hist(
                    hist.axis.StrCategory(procs, name="process", growth=True),
                    vax,
                    storage=hist.storage.Weight(),
                )
                for p, h in d.items():
                    p = pmap.get(decycle(p), decycle(p))
                    if p not in hnew.axes[0]:
                        continue
                    if p == "data":
                        # HACK: histograms may have superfluous (empty) bins; cut them off
                        ce = h.yvalues[: len(vax)]
                        hi = ce + h.yerrorshigh[: len(vax)]
                        lo = ce - h.yerrorslow[: len(vax)]

                        view = hnew.view(False)[hnew.axes[0].index("data")]
                        view["value"] = ce
                        view = hnew.view(False)[hnew.axes[0].index("data_err")]
                        view["value"] = 0.5 * (lo + hi)
                        view["variance"] = np.square(0.5 * (hi - lo))
                    else:
                        view = hnew.view(False)[hnew.axes[0].index(p)]
                        view["value"] += h.values[: len(vax)]
                        view["variance"] += h.variances[: len(vax)]
                postprocessed_hists[shape][cat] = hnew

            # add histograms for fr2
            for shape, hists in postprocessed_hists.items():
                # find histograms which can be merged (present throughout fr2)
                merged = defaultdict(set)
                for year in [2016, 2017, 2018]:
                    for k in hists.keys():
                        if str(year) in k:
                            merged[k.replace(str(year), "run2")].add(k)

                for k, v in merged.items():
                    # get all processes, must not be the same in all histograms
                    all_procs = reduce(lambda a, b: a | b, [set(hists[i].axes[0]) for i in v])

                    # create merge histogram with all procs and integer var axis
                    orig = hists[list(v)[0]]
                    var_axis = orig.axes[1]
                    hnew = hist.Hist(
                        hist.axis.StrCategory(all_procs, name="process", growth=True),
                        hist.axis.Integer(
                            start=0,
                            stop=var_axis.size,
                            name=var_axis.name,
                            label=f"{var_axis.label} bin / a.u.",
                            underflow=var_axis.traits.underflow,
                            overflow=var_axis.traits.overflow,
                        ),
                        storage=orig._storage_type(),
                    )

                    # merge histograms on var axis with views
                    hview = hnew.view(True)
                    hview += sum(
                        Hist5.regrow(
                            hists[k],
                            dict(process=all_procs),
                        ).view(True)
                        for k in v
                    )

                    # add merged histogram to histograms
                    hists[k] = hnew

        # extract fit quality infos
        with uproot4.open(self.input().path) as f:
            fit_results = {}
            for key in f.keys(recursive=False, cycle=False, filter_classname="RooFitResult"):
                try:
                    o = f[key]
                except uproot4.DeserializationError as e:
                    o = e.context["breadcrumbs"][0]
                assert "_covQual" in o._members
                fit_results[key] = o._members.copy()

        assert "fit_results" not in postprocessed_hists
        postprocessed_hists["fit_results"] = fit_results

        self.output().dump(postprocessed_hists)


class FitDiagnosticsCombinedPostprocessed(FitDiagnosticsPostprocessed, FlatDatacardsTask):
    @property
    def analysis_choice(self):
        if len(self.analyses) == 1:
            return self.analyses[0]
        else:
            raise ValueError("can't access singular analysis when multiple present")

    def requires(self):
        return FitDiagnosticsCombined.req(self)


class PlotPullsAndImpacts(FlatDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/pullsandimpacts.html"""

    mc_stats = luigi.BoolParameter()
    show_best_fit = luigi.BoolParameter(default=False)
    order_by_impact = luigi.BoolParameter(default=True)
    priority = 300

    def requires(self):
        req = super().requires()
        req.update(
            {
                "nuisance_renaming": EOSFileUpload.req(
                    self,
                    filepath=str(Path(__name__).parent / "config" / "rename_nuisance.py"),
                )
            }
        )
        return req

    def command(self):
        ret = self.base_command(workflows=["PullsAndImpacts"]) + [
            "--keep-failures",
            "--left-margin",
            "750",
            "--pad-width",
            "1500",
            "--label-size",
            "18",
            "--file-types",
            "pdf,png",
            "--labels",
            self.remote_path(self.input()["nuisance_renaming"]),
            "--PullsAndImpacts-custom-args",
            " --robustFit=1 --cminDefaultMinimizerPrecision=1E-12 --setParameterRanges r=-100,100",
        ]
        if self.unblind:
            ret.append("--unblinded")
        else:
            ret.append("--order-by-impact")
        if self.mc_stats:
            ret.append("--mc-stats")
        if self.show_best_fit:
            ret += ["--show-best-fit", "True"]
        else:
            ret += ["--show-best-fit", "False"]
        if self.order_by_impact:
            ret.append("--order-by-impact")
        return ret

    def store_parts(self):
        sp = super().store_parts()
        if self.mc_stats:
            sp += ("mc_stats",)
        return sp

    @outputs_to_issue(important_params=["analyses", "years", "unblind"], collapsibles=r".*.png$")
    def run(self):
        super().run()


class PlotMultipleGoodnessOfFits(MultiDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/gof.html#testing-multiple-datacards"""

    algo = luigi.ChoiceParameter(choices=["saturated", "AD", "KS"], default="saturated")
    priority = 400

    toys = luigi.IntParameter(default=300)

    def store_parts(self):
        return super().store_parts() + (self.algo,) + (f"toys_{self.toys}",)

    def command(self):
        cmd = self.base_command(workflows={"GoodnessOfFit": "23h"})
        cmd += [
            "--file-types",
            "pdf,png",
            "--toys",
            f"{self.toys}",
            "--toys-per-branch",
            "1",
            "--algorithm",
            f"{self.algo}",
        ]
        if self.algo == "saturated":
            cmd += ["--frequentist-toys"]

        # remove snapshot mechanism here, hits walltime for combination
        cmd.remove("--use-snapshot")
        idx = cmd.index("--Snapshot-workflow")
        del cmd[idx : idx + 2]
        idx = cmd.index("--Snapshot-custom-args")
        del cmd[idx : idx + 2]
        return cmd

    @outputs_to_issue(important_params=["analyses", "years", "unblind", "algo"])
    def run(self):
        super().run()


class PlotUpperLimitsAtPoint(MultiDatacardsTask, CreateIssueMixin):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#multiple-limits-at-a-certain-point-of-parameters"""

    poi = luigi.ChoiceParameter(default="r", choices=["r", "r_gghh", "r_qqhh"])
    workflows = ["UpperLimits"]
    priority = 500

    def store_parts(self):
        return super().store_parts() + (self.poi,)

    def command(self):
        cmd = self.base_command(workflows=self.workflows)
        cmd += ["--file-types", "pdf,png"]
        cmd += ["--pois", self.poi]
        if self.unblind:
            cmd.append("--unblinded")
        return cmd

    @outputs_to_issue(important_params=["analyses", "years", "unblind", "poi", "statmodel"])
    def run(self):
        super().run()


class ParameterScan(CreateIssueMixin):
    scan_parameter = luigi.Parameter(default="kl")
    workflows = ["LikelihoodScan"]
    y_log = True

    def store_parts(self):
        return super().store_parts() + (self.scan_parameter.replace(":", "__").replace(",", "_"),)

    def command(self):
        cmd = self.base_command(workflows=self.workflows)
        cmd += ["--file-types", "pdf,png"]
        if self.y_log:
            cmd += ["--y-log"]
        cmd += ["--scan-parameters", f"{self.scan_parameter}"]
        if self.unblind:
            cmd.append("--unblinded")
        return cmd

    @outputs_to_issue(
        important_params=["analyses", "years", "unblind", "scan_parameter", "statmodel"]
    )
    def run(self):
        super().run()


class LikelihoodScan(ParameterScan):
    @property
    def priority(self):
        return len(self.pois.split(",")) * 400

    @property
    def Nsp(self):
        return len(self.scan_parameter.split(":"))

    @property
    def y_log(self):
        return self.Nsp == 1

    @property
    def pois(self):
        sp = self.scan_parameter.split(":")
        if len(sp) == 1:
            return sp[0].split(",")[0]
        else:
            assert len(sp) == 2
            return ",".join([sp_.split(",")[0] for sp_ in sp])

    def command(self):
        cmd = super().command()
        cmd += ["--pois", self.pois]
        if self.Nsp == 2:
            cmd += ["--shift-negative-values"]
        return cmd


class LimitScan(ParameterScan):
    outmap = {"plot.pdf": "*.pdf", "plot.png": "*.png", "exclusion_ranges.json": "*.json"}
    priority = 400
    workflows = ["UpperLimits"]

    def command(self):
        cmd = super().command()
        if self.scan_parameter == "C2V":
            cmd += ["--pois", "r_qqhh"]
        else:
            cmd += ["--pois", "r"]
        cmd += ["--xsec", "fb"]
        cmd += ["--save-ranges"]
        return cmd


class ExclusionAndBestFit:
    pois = PlotUpperLimitsAtPoint.poi
    workflows = ["UpperLimits", "LikelihoodScan"]
    y_log = False
    priority = 1000

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if "2D" in type(self).__name__:
            assert self.Nsp == 2, "This task only works for 2D scans!"
        else:
            assert self.Nsp == 1, "This task only works for 1D scans!"


class PlotExclusionAndBestFit(ExclusionAndBestFit, LikelihoodScan, MultiDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/exclusion.html"""


class PlotExclusionAndBestFit2D(ExclusionAndBestFit, LikelihoodScan, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/exclusion.html#2d-parameter-exclusion"""


class PlotMultipleUpperLimits(LimitScan, MultiDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#multiple-limits-on-poi-vs-scan-parameter"""


class PlotUpperLimits(LimitScan, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/limits.html#limit-on-poi-vs-scan-parameter"""


class PlotMultipleLikelihoodScans(LikelihoodScan, MultiDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/likelihood.html#1d_1"""


class PlotLikelihoodScan(LikelihoodScan, FlatDatacardsTask):
    """https://cms-hh.web.cern.ch/cms-hh/tools/inference/tasks/likelihood.html#1d"""


class InferenceCombined(
    ModelMixin,
    RecipeMixin,
    CreateIssueMixin,
    CombinationTask,
    law.WrapperTask,
):

    dhi_command = DHITask.dhi_command
    no_poll = DHI.no_poll
    mode = luigi.Parameter(
        default="lrGIi",
        description="""One or more of these (lrGPIpigs):
        l = limit asimov @SM
        r = limit scans
        G = gof [unblinded and only in background regions]
        P = postfit [unblinded and only in background regions]
        I = impacts [unblinded and only in background regions]
        p = prefit
        i = impacts
        g = gof other algorithms
        s = systematic envelopes
        """,
    )

    def fdiag(self, ub=False):
        from tasks.prefit import PlotFitDiagnosticsCombined

        if tuple(sorted(self.years)) == ("2016", "2017", "2018"):
            years = ("run2",)
        elif len(self.years) == 2:
            raise ValueError(
                "Can not produce Prefit/Postfit plots from only 2 years. Either one year or FR2!"
            )
        else:
            years = self.years
        if self.dhi_command:
            req = {
                (a, ub): FitDiagnosticsCombined.req(
                    self,
                    years=self.years,
                    unblind=ub,
                    analyses=(a,),
                )
                for a in self.analyses
            }
            return req
        else:
            # TODO: this is not yet compatible with analyses combination!
            return {
                (a, y, ub): PlotFitDiagnosticsCombined.req(
                    self,
                    year=y,
                    analysis_choice=a,
                    log_scale=True,
                    blind_thresh=1e4,
                    process_group=dict(bbww_dl=("plotting_DYest",), bbww_sl=("plotting",))[a],
                    unblind=ub,
                )
                for y in years
                for a in self.analyses
            }

    def requires(self):
        self.logger.info(f"Running mode {self.mode} with year(s) {self.years}")
        reqs = {}
        if "l" in self.mode:
            reqs["limits"] = [
                PlotUpperLimitsAtPoint.req(self, poi="r"),
                PlotUpperLimitsAtPoint.req(self, poi="r_qqhh"),
            ]
        if "r" in self.mode:
            reqs["scans"] = [
                PlotUpperLimits.req(self, scan_parameter="kl"),
                PlotUpperLimits.req(self, scan_parameter="C2V"),
            ]
        if "G" in self.mode:
            reqs["gof"] = {"saturated": PlotMultipleGoodnessOfFits.req(self, algo="saturated")}
        if "P" in self.mode:
            reqs["fdiag_ub"] = self.fdiag(ub=True)
        if "I" in self.mode:
            reqs["pulls"] = {
                "unblind": {
                    (a, y): PlotPullsAndImpacts.req(
                        self,
                        years=(y,),
                        unblind=True,
                        analyses=(a,),
                    )
                    for y in self.years
                    for a in self.analyses
                }
            }
            if len(self.years) > 1 or len(self.analyses) > 1:
                # add combination
                reqs["pulls"]["unblind"].update(
                    {
                        ("+".join(self.analyses), "+".join(self.years)): PlotPullsAndImpacts.req(
                            self, unblind=True
                        )
                    }
                )
        if "p" in self.mode:
            reqs["fdiag_b"] = self.fdiag(ub=False)
            # remove combined prefit if exists
            if "run2" in self.years:
                reqs["fdiag_b"].pop(("run2", False))
        if "i" in self.mode:
            reqs["pulls"].update(
                {
                    "asimov": {
                        (a, y): PlotPullsAndImpacts.req(
                            self,
                            years=(y,),
                            analyses=(a,),
                        )
                        for y in self.years
                        for a in self.analyses
                    }
                }
            )
            if len(self.years) > 1 or len(self.analyses) > 1:
                # add combination
                reqs["pulls"]["asimov"].update(
                    {("+".join(self.analyses), "+".join(self.years)): PlotPullsAndImpacts.req(self)}
                )
        if "g" in self.mode:
            reqs["gof"] = {
                algo: PlotMultipleGoodnessOfFits.req(self, algo=algo)
                for algo in PlotMultipleGoodnessOfFits.algo._choices
            }
        if "s" in self.mode:
            reqs["sys"] = CheckSystsCombined.req(self)
        return reqs

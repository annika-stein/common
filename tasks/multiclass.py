# -*- coding: utf-8 -*-

import os
import uuid
from dataclasses import dataclass
from functools import cached_property, reduce
from collections import defaultdict
from typing import Callable, List, Mapping
import logging

# has to be imported before other things
import comet_ml

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import mplhep as hep
from tensorflow.keras.callbacks import TensorBoard, CSVLogger
from sklearn.cluster import KMeans
import skopt
from tqdm.auto import tqdm

from luigi import Parameter, BoolParameter, IntParameter, FloatParameter, ListParameter, ChoiceParameter  # fmt:skip
import luigi.util
import law
from law import CSVParameter

from utils.order import get_parent_process
from utils.util import ulimit, round_significant
from tasks.base import AnalysisCampaignTask, AnalysisTask, HTCondorWorkflow, HTCondorGPUWorkflow
from tasks.dnn import DNNProcessor, StitchedBase, ModelExportBase
from tasks.coffea import CoffeaProcessor
import tools.dssutils
import tools.comet
import tools.plotting
from tools import keras as tk
from tools.data import DSS, DSSDisk
from utils import keras as uk
from utils import optimize as uopt
from utils.optimize import SIntParameter, SFloatParameter, SChoiceParameter
from utils.sandbox import SubmitTask as OriginalSubmitTask

logger = logging.getLogger(__name__)


class SubmitTask(OriginalSubmitTask):
    silent = luigi.BoolParameter(significant=False)

    @property
    def submit_kwargs(self):
        _submit_kwargs = super().submit_kwargs
        if self.silent:
            _submit_kwargs.update({"-o": "/dev/null", "-e": "/dev/null"})
        return _submit_kwargs


class DSSProcessor:
    export_version = Parameter(description="dss export version", default="0")

    def store_parts(self):
        return super().store_parts() + (f"export_version_{self.export_version}",)


@dataclass
class MulticlassConfig:
    """Class for configuring multiclass handling."""

    groups: dict
    group: str
    maxn: int = None


class MulticlassDSSProcessor(DSSProcessor):

    n_splits = 5

    batch_size = 2 ** 8
    validation_batch_size = 2 ** 14
    y = "label"
    w = "weight"
    train = "train"
    valid = "valid"
    test = "test"
    eventnr = "eventnr"
    procid = "procid"

    cut_neg = BoolParameter(default=True, parsing=BoolParameter.EXPLICIT_PARSING)
    debug = BoolParameter(description="Use/produce debug dataset and do short training.")

    @property
    def processes(self):
        raise NotImplementedError

    @property
    def maxn(self) -> int:
        return self.config.maxn

    def merge(self, dss: DSS, identifier: str = "eventnr") -> DSS:
        """Merges dss object according to identifier.
            Fusing is done such that only the dimension of the identifier persists.
            If only one key, no need to materialize array.

        Args:
            dss (DSS): The dss object to merge.
            identifier (str, optional): Used to declare which dimensions shall be merged.
                All dimensions in which the identifier occurs are merged.
                Defaults to "eventnr".

        Returns:
            DSS: The merged DSS object.
        """
        keys = dss[identifier].keys()
        if len(keys) == 1:
            return dss[list(keys)[0]]
        else:
            return dss.fuse(*keys)

    def print_processes_summary(self, processes):
        from rich.console import Console
        from rich.table import Table

        table = Table(title="Processes used in Training")

        table.add_column("Parent Process")
        table.add_column("Child Processes")
        table.add_column("Count")

        for parent_process, child_processes in processes.items():
            total_count = sum(child_process["count"] for child_process in child_processes)
            table.add_row(f"{parent_process.name}", "", f"{total_count:,}", style="magenta")
            for child_process in sorted(child_processes, key=lambda d: d["count"], reverse=True):
                table.add_row("", child_process["process"].name, f"{child_process['count']:,}")

        console = Console()
        console.print(table)

    def split_processes(self, dss: DSS) -> DSS:
        procids = dss[self.procid]
        unique_procids = np.unique(procids)
        processes = defaultdict(list)

        # gather parent and child processes
        for procid in unique_procids:
            process = self.analysis_inst.processes.get(int(procid))
            count = np.sum(procids == procid)

            parent_processes = [p for p in self.processes if process.has_parent_process(p)]
            if len(parent_processes) == 0:
                logger.warn(f"Process {process.name} will not be used in training.")
                continue
            assert len(parent_processes) == 1, f"{process} has >1 parents ({parent_processes})"
            parent_process = parent_processes[0]

            processes[parent_process].append(dict(process=process, procid=procid, count=count))

        # print summary
        self.print_processes_summary(processes)

        # norm processes
        weight = np.copy(dss["weight"])
        for parent_process, child_processes in tqdm(
            processes.items(), unit="process", desc="norming processes"
        ):
            if "norm" in self.processes_dict[parent_process.name].get("groups", []):
                for child_process in child_processes:
                    mask = (dss[self.procid] == child_process["procid"]).flatten()
                    weight[mask] = dss["weight"][mask] / np.sum(dss["weight"][mask])
        dss["weight"] = weight

        # split parent processes
        out = DSSDisk()
        for parent_process, child_processes in tqdm(
            processes.items(), unit="process", desc="splitting processes"
        ):
            masks = [
                (dss[self.procid] == child_process["procid"]).flatten()
                for child_process in child_processes
            ]
            mask = reduce(np.logical_or, masks)
            one_hot = np.array(np.array(self.processes) == parent_process)
            one_hot_repeated = np.repeat(one_hot[np.newaxis, ...], mask.sum(), axis=0)

            # write new dss
            out[parent_process.name] = dss.map(lambda x: x[mask])
            out[parent_process.name, "label"] = one_hot_repeated

        return out

    def neg_weights_mask_fn(self, w: str = "weight") -> Callable:
        def fn(x):
            mask = x[w] > 0
            return x.map(lambda y: y[mask])

        return fn

    def nan_to_zero(self, dss: DSS) -> DSS:
        return dss.map(tools.dssutils.nan_to_zero, groups=dss[self.eventnr].keys())

    def cut_events(self, dss: DSS, maskfn: Callable[[np.array], np.array]) -> DSS:
        dss = dss.map(maskfn, groups=dss[self.eventnr].keys())
        return dss

    def shuffle(self, dss: DSS, maxn: int):
        def _shuffle(dss: DSS) -> DSS:
            # shuffle within processes
            ind = np.random.permutation(dss.blen)
            dss = dss.map(lambda y: y[ind[:maxn]])
            return dss

        return dss.map(_shuffle, groups=dss[self.eventnr].keys(), prog="Shuffle")

    def add_weight_variants(self, dss: DSS, w: str = "weight") -> DSS:
        dss[w + "_abs"] = dss[w].map(tools.dssutils.abs)
        dss[w + "_cut"] = dss[w].map(tools.dssutils.cut)
        dss[w + "_norm"] = dss[w].map(tools.dssutils.norm)
        return dss

    @property
    def tensors(self) -> dict:
        proc = MulticlassDataprepPre.requires(self).Processor
        out = proc.tensors.fget(proc)
        return dict({k: v for k, v in out.items() if "multiclass" in v[-1]["groups"]})

    @property
    def feed_vars(self) -> tuple:
        return tuple(k for k, v in self.tensors.items() if "input" in v[-1]["groups"])

    @property
    def parts(self) -> tuple:
        return tuple(k for k, v in self.tensors.items() if "part" in v[-1]["groups"])

    @property
    def parts_names(self) -> list:
        out = []
        for part in self.parts:
            (id, n, feats, typ, aux) = self.tensors[part]
            if n > 0:
                out += [f"{part}{i+1}" for i in range(n)]
            else:
                out += [part]
        return out

    @property
    def columns(self) -> dict:
        def fourvecname(feat):
            return f"p{feat}" if feat in ["x", "y", "z"] else feat

        out = {}
        for (tensor, (id, n, feats, typ, aux)) in self.tensors.items():
            if n > 0:
                out[tensor] = [
                    f"{tensor}{i}_{fourvecname(feat)}" for i in range(n) for feat in feats
                ]
            else:
                out[tensor] = [f"{tensor}_{feat}" for i, feat in enumerate(feats)]
        return out

    def get_val(self, dss):
        feed = tk.KFeed(x=self.feed_vars, y=self.y, w=self.w)
        generator = feed.gfeed(
            src=dss,
            batch_size=self.batch_size,
        )
        return dict(
            batch_size=self.validation_batch_size,
            **dict(zip(("x", "y", "sample_weight"), generator["validation_data"])),
        )

    def store_parts(self):
        sp = super().store_parts()
        sp += ("debug",) if self.debug else ("default",)
        sp += ("cut_neg",) if self.cut_neg else ("incl_neg",)
        return sp


class MulticlassAnalysisMixin:
    group_override = Parameter(
        description="""
            The id of the group to use in the multiclass training, evaluation,...
            Overrides the group specified in the multiclass-config.
            Must be one of the defined in analysis config (aux: multiclass.groups).
        """,
        default="",
    )
    recipe = Parameter()
    processor = Parameter(default="Exporter")
    years = CSVParameter(default=["2016", "2017", "2018"])

    @property
    def config(self):
        return self.analysis_inst.aux["multiclass"]

    @property
    def groups(self):
        return self.config.groups

    @property
    def group(self):
        return self.group_override or self.config.group

    @property
    def processes_dict(self):
        return self.groups[self.group]

    @property
    def processes(self):
        return [self.analysis_inst.processes.get(p) for p in self.processes_dict.keys()]

    @property
    def class_names(self) -> List[str]:
        return [process.label_short for process in self.processes]

    def store_parts(self):
        return super().store_parts() + (
            f"{self.group}_{len(self.processes)}",
            "years_" + "_".join(self.years),
        )


class MulticlassProcessor(MulticlassAnalysisMixin, DNNProcessor):
    # fmt: off
    model_choice = Parameter(default="default")

    lossdef = ChoiceParameter(choices=["default", "stdbin" , "binary"], default="default")
    focal_loss = SFloatParameter(0, 5, default=2, description="Used Focal Loss").freeze()
    class_weight = ListParameter(default=[], description="class weights used in model training")

    lbn_particles = SIntParameter(0, 30, default=0, name="lbn_particles")
    network_architecture = SChoiceParameter(["FullyConnected", "ResNet"], name="network_architecture", default="FullyConnected")
    jump = SIntParameter(2, 4, default=2, name="jump")

    layers = SIntParameter(3, 10, default=3, name="layers")
    nodes = SIntParameter(256, 1024, default=256, name="nodes")
    activation = SChoiceParameter(["selu", "elu", "softplus", "relu"], name="activation", default="relu")
    batch_norm = SIntParameter(0, 1, default=1, name="batch_norm").freeze()
    dropout = SFloatParameter(0.0, 0.9, default=0.0, name="dropout")
    l2 = SFloatParameter(1e-10, 1e-4, default=1e-8, name="l2", prior="log-uniform")
    learning_rate = SFloatParameter(1e-4, 1e-2, default=1e-3, name="learning_rate", prior="log-uniform")

    # Callbacks: EarlyStopping, ReduceLROnPlateau
    earlystopping_patience = SIntParameter(1, 20, default=8, name="es patience").freeze()
    earlystopping_jank = SIntParameter(1, 20, default=2, name="es jank").freeze()
    reducelronplateau_factor = SFloatParameter(0.01, 0.99, default=0.2, name="factor").freeze()
    reducelronplateau_patience = SIntParameter(1, 10, default=4, name="patience").freeze()
    reducelronplateau_min_delta_rel = SFloatParameter(-0.2, 0.2, default=1e-4, name="min_delta_rel").freeze()
    # fmt: on

    @property
    def loss(self):
        p_enum = enumerate(self.processes_dict.items())
        if len(self.class_weight):
            cw = self.class_weight
        else:
            cw = [v.get("weight", 1) for i, (k, v) in p_enum]

        binary_group_ids = [
            (1.0, [i for i, (k, v) in p_enum if "signal" in v.get("groups", [])]),
            (1.0, [i for i, (k, v) in p_enum if "background" in v.get("groups", [])]),
        ]
        if self.lossdef == "default":
            group_ids = []
        if self.lossdef == "binary":
            return tk.GroupedXEnt(
                group_ids=binary_group_ids,
                std_xent=False,
                focal_gamma=self.focal_loss,
                class_weight=cw,
            )
        if self.lossdef == "stdbin":
            group_ids = binary_group_ids
        return tk.GroupedXEnt(group_ids=group_ids, focal_gamma=self.focal_loss, class_weight=cw)

    def metrics(self, extended=False, classes=2):
        ret = [tf.keras.metrics.CategoricalAccuracy(name="accuracy")]
        if extended:
            ret += [
                tf.keras.metrics.CategoricalCrossentropy(name="crossentropy"),
                tf.keras.metrics.TruePositives(name="tp"),
                tf.keras.metrics.FalsePositives(name="fp"),
                tf.keras.metrics.TrueNegatives(name="tn"),
                tf.keras.metrics.FalseNegatives(name="fn"),
                tf.keras.metrics.Precision(name="purity"),
                tf.keras.metrics.Recall(name="efficiency"),
                tf.keras.metrics.AUC(name="auc"),
            ]
            if classes > 2:
                ret += [tk.AUCOneVsAll(one=i, name=f"custom_auc_{i}") for i in range(classes)]
        return ret

    def store_parts(self):
        sp = super().store_parts()
        sp += (f"model_{self.model_choice}",)
        cw = "_".join(str(w) for w in self.class_weight)
        cw = cw.replace(".", "p")
        sp += (f"cw_{cw}",)
        return sp


class MulticlassDataprepPre(
    MulticlassAnalysisMixin, MulticlassDSSProcessor, AnalysisCampaignTask, SubmitTask
):
    memory = 35000

    split = None
    years = {}

    def requires(self):
        return CoffeaProcessor.req(self, debug=False)

    @property
    def config(self):
        return self.analysis_inst.aux["multiclass"]

    def output(self):
        return self.local_target(".dss")

    def store_parts(self):
        sp = super().store_parts()
        sp += (f"maxn_{self.maxn}",)
        return sp

    def run(self):
        ulimit(AS="hard")
        # load inputs (one Exporter)
        inp = DSSDisk(self.input().load(sep="+"))
        # merge categories (e.g. ee, mumu, emu)
        dss = self.merge(inp, identifier=self.eventnr)
        # split by process ids
        dss = self.split_processes(dss)
        # shuffle and possibly reduce data
        dss = self.shuffle(dss, maxn=self.maxn)

        # if running debug, only take small portion of dataset
        if self.debug:
            dss = dss.map(lambda x: x[:10000])

        self.output().dump(dss, sep="+")


class MulticlassDataprep(MulticlassAnalysisMixin, MulticlassDSSProcessor, AnalysisTask, SubmitTask):
    memory = 50000
    rng = np.random.RandomState

    def requires(self):
        return [MulticlassDataprepPre.req(self, year=year) for year in self.years]

    def output(self):
        return self.local_target(".dss")

    def store_parts(self):
        sp = super().store_parts()
        sp += (f"maxn_{self.maxn}",)
        return sp

    def run(self):
        ulimit(AS="hard")
        inputs = [DSSDisk(inp.load(sep="+")) for inp in self.input()]

        # assert that all dss objs have the same keys
        keys = [dss.keys() for dss in inputs]
        assert keys[:-1] == keys[1:]

        # concatenate years
        dss = DSS.concatenate(*inputs)

        if self.cut_neg:
            dss = self.cut_events(dss, self.neg_weights_mask_fn(w="weight"))

        # set all nans to zero # TODO: should some arise here?
        dss = self.nan_to_zero(dss)

        # add different weight variants
        dss = self.add_weight_variants(dss, w="weight")

        # shuffle dss
        dss = dss.map(lambda x: x.shuffle(rng=self.rng(42)), groups=dss[self.w].skeys())

        # prepare dss for train valid test splitting
        preparing_function = tools.dssutils.get_prepare_train_valid_test(self.n_splits, "eventnr")
        dss = dss.map(preparing_function, groups=dss[self.w].skeys())
        self.output().dump(dss, sep="+")


class MulticlassCheckInputs(MulticlassAnalysisMixin, MulticlassDSSProcessor, AnalysisTask):
    split = None

    def requires(self):
        return MulticlassDataprep.req(self, cut_neg=False)

    def output(self):
        return self.local_directory_target("plots")

    def plot(self, arrays, sample_weight, label=None, key="", title="", ratio=False, path=""):
        fig, (ax1, ax2) = plt.subplots(
            nrows=2,
            figsize=(4, 4),
            sharex="col",
            gridspec_kw={"height_ratios": [2.5, 1], "hspace": 0},
        )

        ns1, bins1, _ = ax1.hist(
            arrays[0],
            weights=sample_weight[0],
            bins=40,
            histtype="stepfilled",
            label=label[0],
            density=True,
        )
        ns2, bins2, _ = ax1.hist(
            arrays[1],
            weights=sample_weight[1],
            bins=bins1,
            histtype="step",
            label=label[1],
            density=True,
        )

        ax1.set_yscale("log")
        ax1.set_ylabel("# events normed")
        hep.cms.label(llabel="Private Work", ax=ax1, year="FR2")
        ax1.legend(title=title, loc="best")

        ax2.axhline(0.0, color="black", linestyle="--")
        ax2.step(bins2[:-1], 100 * np.nan_to_num((ns1 - ns2) / ns2, nan=0, posinf=0, neginf=0))
        ax2.set_ylabel("Rel. Diff. [%]")
        ylim = ax2.get_ylim()
        ax2.set_ylim(bottom=max(ylim[0], -20), top=min(ylim[1], 20))

        d = {
            "hl_ht": r"$H_T$ [GeV]",
            "hl_m_hh_simplemet_bregcorr": r"$m_{hh}^{simple,breg}$ [GeV]",
            "hl_m_ll": r"$m_{ll}$ [GeV]",
        }
        key = d.get(key, key)
        ax2.set_xlabel(key)
        fig.subplots_adjust(bottom=0.15, left=0.2)
        plt.savefig(path)
        plt.close()

    def run(self):
        ulimit(AS="hard")
        data = self.input().load(sep="+")
        cols = self.columns["hl"]
        hl = data["valid", "hl"]
        # create parent dir for plots
        self.output().touch()
        for key in data["train", "weight_norm"].keys():
            k = list(key)[0]
            for i, col in enumerate(cols):
                nom, w = data[k, "train", "hl"], data[k, "train", "weight_norm"]
                nom_neg, w_neg = nom[w > 0], w[w > 0]
                self.plot(
                    [nom[:, i], nom_neg[:, i]],
                    sample_weight=[w, w_neg],
                    label=["incl negative", "cut negative"],
                    key=col,
                    ratio=True,
                    title=f"Process: {k.upper()}",
                    path=f"{self.output().path}/{k}_{col}.pdf",
                )


class MulticlassTraining(MulticlassProcessor, MulticlassDSSProcessor, AnalysisTask, SubmitTask):
    memory = 6000
    gpu_memory = 1500

    w = "weight_norm"
    epochs = IntParameter(default=500)
    training_steps_per_epoch = IntParameter(default=12000)
    checkpoint_frequency = IntParameter(default=500, description="Frequency of checkpoints")
    runtime = FloatParameter(
        default=10000, description="Maximum runtime in hours", significant=False
    )
    comet = BoolParameter()
    split = IntParameter(
        default=0,
        description="The split of the dataset. Corresponds to the event number used in the application.",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def requires(self):
        return MulticlassDataprep.req(self)

    def store_parts(self):
        return super().store_parts() + (f"{self.split}",)

    @property
    def checkpoints(self):
        ep, freq = self.epochs, self.checkpoint_frequency
        if freq > ep:
            return [ep]
        else:
            n = ep // freq
            return [freq * (i + 1) for i in range(n)]

    def output(self):
        return {
            "model": self.local_directory_target("model"),
            "history": self.local_target("history.csv"),
            "tensorboard": self.local_directory_target("tensorboard"),
            "checkpoints": law.SiblingFileCollection(
                {ckp: self.local_directory_target(f"cp-{ckp:02d}") for ckp in self.checkpoints},
                threshold=0,
            ),
        }

    @property
    def optimizer_class(self):
        return self.optimizer_classes[self.optimizer_name]

    @property
    def optimizer(self):
        return self.optimizer_class(self.learning_rate)

    @property
    def use_lbn(self):
        return self.lbn_particles > 0

    @property
    def callbacks(self):
        history_path = self.output()["history"].path
        tensorboard_path = self.output()["tensorboard"].path
        checkpoints = self.output()["checkpoints"]
        callbacks = [
            tk.ReduceLROnPlateau(
                factor=self.reducelronplateau_factor,
                patience=self.reducelronplateau_patience,
                min_delta_rel=self.reducelronplateau_min_delta_rel,
                jank=-1,
            ),
            tk.EarlyStopping(
                patience=self.earlystopping_patience,
                jank=self.earlystopping_jank,
                restore_best_weights=True,
            ),
            tk.EarlyStopping(
                patience=100,
                min_delta=np.inf,
                restore_best_weights=True,
                runtime=dict(hours=self.runtime),
            ),  # max epochs
            # EarlyStopping(patience=1, min_delta=np.inf),  # for testing, 1 epoch
            tk.CheckpointModel(checkpoints=checkpoints),
            CSVLogger(history_path, append=True),
            TensorBoard(log_dir=tensorboard_path, profile_batch=0),
        ]
        return callbacks

    def build_model(
        self,
        input_shapes: Mapping[str, tuple] = None,
        output_shapes=None,
        normal_ref: Mapping[str, np.array] = {},
    ) -> tf.keras.Model:
        """Function to build (possibly) complex tf.keras model.

        Args:
            input_shapes (Mapping[str, tuple], optional): Dict of input shapes, used to define input tensors of model. Defaults to None.
            output_shapes ([type], optional): Output shapes, used to define output tensors of model. Defaults to None.
            normal_ref (Mapping[str, np.array], optional): Dict of numpy arrays used to normalize input tensors. Defaults to {}.

        Raises:
            NotImplementedError: Currently only one output tensor is implemented.

        Returns:
            tf.keras.Model: Built tf.keras model which is ready to be used.
        """
        try:
            _build_model = self._import(f"multiclass.model.{self.model_choice}").build_model
        except (ImportError, AttributeError):
            if len(output_shapes) > 1:
                raise NotImplementedError
            inputs = {
                name: tf.keras.layers.Input(shape, name=name)
                for name, shape in input_shapes.items()
            }
            x = [tf.keras.layers.Flatten()(_x) for _x in inputs.values()]
            x = x[0] if len(x) == 1 else tf.keras.layers.Concatenate()(x)
            Network = getattr(tk, self.network_architecture)
            x = Network(
                block_size=self.block_size,
                jump=self.jump,
                layers=self.layers,
                nodes=self.nodes,
                activation=self.activation,
                batch_norm=self.batch_norm,
                dropout=self.dropout,
                l2=self.l2,
            )(x)
            x = tf.keras.layers.Dense(output_shapes[0][0], activation="softmax", name="output")(x)
            model = tf.keras.Model(
                inputs=inputs.values(),
                outputs=x,
                name=f"model_{self.split}_{uuid.uuid4().hex[0:10]}",
            )
        else:
            model = _build_model(self, input_shapes, output_shapes, normal_ref)
        return model

    @property
    def steps_per_epoch(self):
        if self.debug:
            return 1
        else:
            return self.training_steps_per_epoch

    def run(self):
        # increase virtual memory
        ulimit(AS="hard")

        if self.comet is True:
            print("Starting comet experiment")
            comet_experiment = comet_ml.Experiment(
                project_name=os.getenv("DHA_ANALYSIS_ID") + "_multiclassification",
                workspace=os.getenv("DHA_COMET_WORKSPACE"),
            )
            comet_experiment.add_tag(self.version)
            # if self.explorative is True:
            # comet_experiment.add_tag("explorative")
            # if self.debug is True:
            # comet_experiment.add_tag("debug")
            # comet_experiment.add_tag("long")
        else:
            comet_experiment = None

        # load inputs
        dss = self.input().load(sep="+")
        dss.map(np.sum)

        # take only what we need in this split
        apply_train_valid_test = tools.dssutils.get_apply_train_valid_test(self.split)
        dss = dss.map(apply_train_valid_test, groups=dss[self.w].skeys())

        # init feed stream
        feed = tk.KFeed(x=self.feed_vars, y=self.y, w=self.w)

        # memmap val data: val (gen) should (not) use DSSDisk
        dss = DSSDisk(dss)
        dss = DSS(feed.compact(dss))

        input_shapes = feed.getShapesK(src=dss)[0]
        output_shapes = feed.getShapesK(src=dss)[1]

        generator = feed.gfeed(
            src=dss,
            batch_size=self.batch_size,
            steps_per_epoch=self.steps_per_epoch,
            epochs=self.epochs,
            validation_batch_size=self.validation_batch_size,
        )

        val = self.get_val(dss)

        try:
            model = self.load_latest(
                self.output()["checkpoints"], custom_objects={"GroupedXEnt": tk.GroupedXEnt}
            )
        except FileNotFoundError:
            input_shapes = dict(zip(self.feed_vars, input_shapes))
            normal_ref = dict(zip(self.feed_vars, val["x"]))
            model = self.build_model(
                input_shapes=input_shapes,
                output_shapes=output_shapes,
                normal_ref=normal_ref,
            )
        callbacks = self.callbacks + [
            tk.PlotMulticlass(
                **val,
                logdir=self.output()["tensorboard"].path,
                class_names=self.class_names,
                columns=self.columns,
                comet_experiment=comet_experiment,
                freq=self.checkpoint_frequency,
                file_extension="png",
            ),
        ]
        if self.comet is True:
            callbacks += [
                tools.comet.MulticlassificationConfusionMatrix(
                    comet_experiment,
                    **val,
                    labels=list(map(str, self.processes.keys())),
                    freq=self.checkpoint_frequency,
                    verbose=1,
                )
            ]

        model.compile(optimizer=self.optimizer, loss=self.loss, weighted_metrics=self.metrics())
        model.fit(**generator, initial_epoch=self.initial_epoch, callbacks=callbacks)

        model.save(self.output()["model"].path)


class MulticlassEvaluation(MulticlassProcessor, MulticlassDSSProcessor, AnalysisTask, SubmitTask):
    memory = 25000
    gpu_memory = 1000

    split = IntParameter(default=0)
    plot_importance = luigi.BoolParameter()

    def store_parts(self):
        return super().store_parts() + (f"{self.split}",)

    def requires(self):
        return {
            "data": MulticlassDataprep.req(self),
            "training": MulticlassTraining.req(self),
        }

    def output(self):
        return {
            "summary": self.local_target("summary.txt"),
            "evaluation": self.local_target("evaluation.json"),
            "objective": self.local_target("objective.json"),
            "plots": self.local_directory_target("plots"),
        }

    @cached_property
    def model(self):
        model_target = self.input()["training"]["model"]
        if model_target.exists():
            model = self.load_model(model_target, compile=False)
        else:
            checkpoint_target = self.input()["training"]["checkpoints"]
            model = self.load_latest(checkpoint_target, compile=False)
        model.compile(loss=self.loss, weighted_metrics=self.metrics())
        return model

    def data(self):
        dss = DSSDisk(self.input()["data"].load(sep="+"))
        # take only what we need in this split
        apply_train_valid_test = tools.dssutils.get_apply_train_valid_test(self.split)
        dss = dss.map(apply_train_valid_test, groups=dss[self.w].skeys())
        return dss

    def run(self):
        # plot histories
        self.output()["plots"].touch()

        if "history" in self.input()["training"]:
            tools.plotting.plot_histories(
                self.input()["training"]["history"].path,
                self.output()["plots"].path,
            )

        # write model summary
        tk.write_summary(self.model, self.output()["summary"])

        data = self.data()
        val = self.get_val(data)

        ck = {
            "class_names": self.class_names,
            "logdir": self.output()["plots"].path,
            "columns": self.columns,
            "path": self.output()["plots"].path,
        }
        callbacks = [
            tk.PlotMulticlassEval(plot_importance=self.plot_importance, **val, **ck),
        ]
        lbn_layer = tk.find_layer(self.model, "LBNLayer")
        if lbn_layer is not None:
            callbacks.append(
                tk.PlotLBNEval(
                    lbn_layer=lbn_layer,
                    logdir=self.output()["plots"].path,
                    inp_particle_names=self.parts_names,
                    path=self.output()["plots"].path,
                )
            )
        evaluation = self.model.evaluate(
            **val,
            callbacks=callbacks,
        )
        self.output()["evaluation"].dump({"evaluate": evaluation})
        # monitor loss
        loss = evaluation[0]
        self.output()["objective"].dump(loss)


class MulticlassCluster(MulticlassEvaluation):
    n_clusters = FloatParameter(30)
    split = IntParameter(default=0)

    def requires(self):
        return {
            "data": MulticlassTraining.req(self, version="dev1").requires(),
            "training": MulticlassTraining.req(
                self, version="dev1_tmp7", batch_norm="True", l2=0.0002
            ),
        }

    def output(self):
        return self.local_target("kmeans_%d.pkl" % self.n_clusters)

    def run(self):
        ulimit(AS="hard")

        dss = self.input()["data"].load(sep="+")

        # take only what we need in this split
        apply_train_valid_test = tools.dssutils.get_apply_train_valid_test(self.split)
        dss = dss.map(apply_train_valid_test, groups=dss[self.w].skeys())

        val = self.get_val(dss)
        x = val["x"]
        w = val["sample_weight"]
        del val

        with self.timeit("predict"):
            pred = self.model.predict(x=x, batch_size=4096)

        assert np.all(np.isfinite(pred))
        print("start kmeans on %s (%.1fMB)" % (pred.shape, pred.nbytes / (1 << 20)))

        km = KMeans(n_clusters=int(self.n_clusters))
        with self.timeit("kmeans"):
            km.fit(pred, sample_weight=w)

        self.output().dump(km)


class MulticlassExport(MulticlassProcessor, ModelExportBase, AnalysisTask):
    def requires(self):
        return MulticlassTraining.req(self, branch=-1)


class MulticlassStitched(MulticlassProcessor, StitchedBase, DSSProcessor, AnalysisTask):
    def requires(self):
        n_splits = MulticlassTraining.n_splits
        return {i: MulticlassTraining.req(self, split=i) for i in range(n_splits)}


class MulticlassMultiStitched(MulticlassProcessor, StitchedBase, DSSProcessor, AnalysisTask):
    def run(self):
        models = [tf.keras.models.load_model(inp["model"].path) for inp in self.input()]
        model0_inputs = models[0].inputs

        # generate new inputs
        inputs = [
            tf.keras.Input(inp.shape[1:], name=inp.name.split(":")[0], dtype=inp.dtype)
            for inp in model0_inputs
        ]
        values = [model(inputs) for model in models]

        output = tf.keras.layers.concatenate(
            [tf.keras.layers.Lambda(lambda x: tf.expand_dims(x, 0))(val) for val in values],
            0,
        )
        stitched_model = tf.keras.Model(inputs, output)
        stitched_model.save(self.output()["model"].path)


class MulticlassMultiStitchedWeights(MulticlassMultiStitched):
    signal_weights = [1, 1 / 2, 1 / 5, 1 / 8, 1 / 10, 1 / 20, 1 / 35, 1 / 50]
    req = MulticlassStitched

    def requires(self):
        network = dict(
            lossdef="default",
            lbn_particles=12,
            network_architecture="ResNet",
            layers=3,
            jump=2,
            nodes=256,
            batch_norm=1,
        )
        all_weights = []
        for signal_weight in self.signal_weights:
            weights = []
            for process, process_aux in self.processes_dict.items():
                process_weight = signal_weight if "signal" in process_aux.get("groups", []) else 1
                weights.append(process_weight)
            all_weights.append(weights)

        return [self.req.req(self, **network, class_weight=weights) for weights in all_weights]


class MulticlassMultiStitchedWeightsAN(MulticlassMultiStitchedWeights):
    signal_weights = [10, 2, 1, 1 / 2, 1 / 5, 1 / 8, 1 / 10, 1 / 20, 1 / 35, 1 / 50, 1 / 100]


class MulticlassMultiStitchedTrainings(MulticlassMultiStitched):
    signal_weight = 1 / 8
    req = MulticlassStitched

    @property
    def class_weight(self):
        weights = []
        for process, process_aux in self.processes_dict.items():
            process_weight = self.signal_weight if "signal" in process_aux.get("groups", []) else 1
            weights.append(process_weight)
        return weights

    def requires(self):
        network = dict(
            lossdef="default",
            lbn_particles=12,
            network_architecture="ResNet",
            layers=3,
            jump=2,
            nodes=256,
            batch_norm=1,
        )
        return [
            self.req.req(
                self,
                **network,
                class_weight=self.class_weight,
                training_version=f"{self.training_version}_{i}",
            )
            for i in range(10)
        ]


class MulticlassStitchedExport(MulticlassProcessor, ModelExportBase, DSSProcessor, AnalysisTask):
    def requires(self):
        return MulticlassStitched.req(self)


class MulticlassStitchedEvaluation(MulticlassEvaluation):
    memory = 50000
    gpu_memory = 5000

    split = -1

    @property
    def feed_vars(self):
        return super().feed_vars + (MulticlassDSSProcessor.eventnr,)

    def data(self):
        """
        Data routing within the stitched network works automatically.
        To ensure that networks are validated with their original validation
        data, each event number is decreased by 1.
        """
        dss = super().data()
        dss[self.eventnr] = dss[self.eventnr].map(lambda x: x + self.n_splits - 1)
        return dss

    def requires(self):
        return {
            "data": MulticlassDataprep.req(self),
            "training": MulticlassStitched.req(self),
        }

    def workflow_requires(self):
        return {
            "data": MulticlassDataprep.req(self),
            "training": MulticlassStitched.req(self),
        }


class MulticlassMultiStitchedEvaluation(MulticlassMultiStitchedTrainings, law.WrapperTask):
    req = MulticlassStitchedEvaluation


class MulticlassMultiStitchedWeightsANEvaluation(MulticlassMultiStitchedWeights, law.WrapperTask):
    signal_weights = [10, 2, 1, 1 / 2, 1 / 5, 1 / 8, 1 / 10, 1 / 20, 1 / 35, 1 / 50, 1 / 100]
    req = MulticlassStitchedEvaluation


class MulticlassOptimizerPreparation(uopt.OptimizerPreparation):
    objective = MulticlassEvaluation


class MulticlassOptimizer(uopt.Optimizer):
    objective = MulticlassEvaluation

    def requires(self):
        return MulticlassOptimizerPreparation.req(self)

    def obj_req(self, ask):
        lower = 0
        upper = MulticlassTraining.n_splits
        seed = hash(frozenset(ask.items())) % (2 ** 32 - 1)
        split = np.random.RandomState(seed).randint(lower, upper)
        return self.objective.req(self, **ask, split=split, branch=-1)


class MulticlassOptimizerPlot(uopt.OptimizerPlot):
    def requires(self):
        return MulticlassOptimizer.req(self)


class MulticlassOptimizerDraw(uopt.OptimizerDraw):
    def requires(self):
        return MulticlassOptimizerPreparation.req(self)


class MulticlassOptimizedStitched(StitchedBase, uopt.Opt, AnalysisTask):
    def output(self):
        return {
            "model": self.local_directory_target("model"),
            "param": self.local_target("param.json"),
        }

    def requires(self):
        MulticlassOptimizerPlot.req(self, branch=-1)
        opt_req = MulticlassOptimizer.req(self, branch=-1)
        opt = opt_req.output()["collection"].targets[0]["opt"].load()
        opt_result = opt.get_result()
        best = {}
        for var, val in zip(opt_result["space"], opt_result["x"]):
            name = var.name
            if name == "split":
                continue
            if isinstance(var, (skopt.space.Real, skopt.space.Integer)):
                sig = 1 if var.prior == "log-uniform" else 2
                val = round_significant(val, sig=sig)
            best[name] = val

        self.param = best

        n_splits = MulticlassTraining.n_splits
        return {
            i: MulticlassTraining.req(self, branch=-1, split=i, **best) for i in range(n_splits)
        }

    def run(self):
        super().run()
        self.output()["param"].dump(self.param)


class MulticlassOptimizedStitchedExport(ModelExportBase, uopt.Opt, AnalysisTask):
    def requires(self):
        return MulticlassOptimizedStitched.req(self)

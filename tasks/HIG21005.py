# -*- coding: utf-8 -*-

import shutil
from pathlib import Path

import law
import luigi
import numpy as np

from tasks.base import AnalysisCampaignTask, CombinationTask
from tasks.combine import PlotPullsAndImpacts  # patched
from tasks.combine import (
    DHI,
    DHITask,
    PlotExclusionAndBestFit,
    PlotLikelihoodScan,
    PlotMultipleGoodnessOfFits,
    PlotMultipleLikelihoodScans,
    PlotMultipleUpperLimits,
    PlotUpperLimits,
    PlotUpperLimitsAtPoint,
)
from tasks.mixins import CreateIssueMixin, ModelMixin, RecipeMixin
from tasks.prefit import PlotFitDiagnosticsCombined
from tasks.yields import YieldTables
from utils.util import walk_directory


class PlotPullsAndImpactsAN20119(PlotPullsAndImpacts):
    outmap = {"plot.pdf": "*.pdf"}

    def command(self):
        # multiple pdf pages
        cmd = super().command()
        cmd[0] = "PlotPullsAndImpacts"
        # only pdf for multiple pages
        idx = cmd.index("--file-types")
        cmd[idx + 1] = "pdf"
        cmd += ["--parameters-per-page", "20"]
        return cmd


class AN20119(
    ModelMixin,
    RecipeMixin,
    CreateIssueMixin,
    CombinationTask,
):
    dhi_command = DHITask.dhi_command
    no_poll = DHI.no_poll
    # fix for AN:
    analyses = ["bbww_dl", "bbww_sl"]
    years = ["2016", "2017", "2018"]

    def requires(self):
        reqs = {}

        # pulls per year and analysis
        reqs["pulls"] = {
            "unblind": {
                (a, y): PlotPullsAndImpactsAN20119.req(
                    self,
                    years=(y,),
                    unblind=True,
                    analyses=(a,),
                )
                for y in self.years
                for a in self.analyses
            },
            "asimov": {
                (a, y): PlotPullsAndImpactsAN20119.req(self, years=(y,), analyses=(a,))
                for y in self.years
                for a in self.analyses
            },
        }
        # Prefit/Postfit Plots
        reqs["prefit+postfit"] = {
            (analysis, year, unblind): PlotFitDiagnosticsCombined.req(
                self,
                year=year,
                analysis_choice=analysis,
                log_scale=True,
                blind_thresh=1e4,
                process_group="plotting",
                unblind=unblind,
            )
            for analysis in self.analyses
            for year in self.years
            for unblind in (True, False)
        }
        # Yield Tables
        reqs["yields"] = {
            (analysis, year, unblind): YieldTables.req(
                self,
                year=year,
                analysis_choice=analysis,
                unblind=unblind,
                style_AN=True,
            )
            for analysis in self.analyses
            for year in self.years
            for unblind in (True, False)
        }

        reqs["limits"] = []
        reqs["scans"] = []
        reqs["gof"] = []

        # DL, SL, Comb: ["bbww_dl", "bbww_sl", ["bbww_dl", "bbww_sl"]]
        for analyses in self.analyses + [self.analyses]:
            analyses = law.util.make_list(analyses)
            # fmt: off
            # limits @SM
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses))
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses))
            # Stat. only
            # only with autoMCStats
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, statmodel="StatModelNoSysts"))
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, statmodel="StatModelNoSysts"))
            # only data stat uncertainties
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, statmodel="StatModelStatOnly"))
            reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, statmodel="StatModelStatOnly"))
            # best fit and exclusion
            reqs["limits"].append(PlotExclusionAndBestFit.req(self, pois="r", scan_parameter="kl,-20,20,81", analyses=analyses))
            reqs["limits"].append(PlotExclusionAndBestFit.req(self, pois="r_qqhh", scan_parameter="C2V,-4,6,81", analyses=analyses))
            # brazil band
            reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses))
            reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses))
            # multiple brazil bands
            reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses))
            reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses))
            # likelihood
            reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81", analyses=analyses))
            reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses))
            # multiple likelihood
            reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="kl,-20,20,81", analyses=analyses))
            reqs["scans"].append(PlotMultipleLikelihoodScans.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses))
            # combined pulls
            reqs["pulls"]["unblind"].update({tuple(analyses): PlotPullsAndImpactsAN20119.req(self, unblind=True, analyses=analyses)})
            reqs["pulls"]["asimov"].update({tuple(analyses): PlotPullsAndImpactsAN20119.req(self, analyses=analyses)})
            if len(analyses) == 1:
                # gof
                reqs["gof"].append(PlotMultipleGoodnessOfFits.req(self, algo="saturated", toys=1000, analyses=analyses))
            else:
                # 2D
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81:kt,-5,5,81", analyses=analyses))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="CV,-5,5,81:C2V,-4,6,81", analyses=analyses))
                reqs["scans"].append(PlotLikelihoodScan.req(self, scan_parameter="kl,-20,20,81:C2V,-4,6,81", analyses=analyses))
            # fmt: on
        return reqs

    def output(self):
        return self.local_directory_target()

    @property
    def base(self):
        return Path(self.output().path)

    def bundle_output(self, task):
        # TODO: fix PlotFitDiagnosticsCombined

        # general
        if isinstance(task, AnalysisCampaignTask):
            analysis = task.analysis_choice
            year = task.year
        else:
            assert isinstance(task, CombinationTask)
            if len(task.analyses) == 1:
                (analysis,) = task.analyses
            else:
                analysis = "combined"
            if len(task.years) == 1:
                (year,) = task.years
            else:
                year = "Run2"
        # task dependent
        base = self.base / Path(analysis) / year
        parts = [type(task).__name__]
        if isinstance(task, PlotUpperLimitsAtPoint):
            parts.append(task.poi)
        if isinstance(task, (PlotLikelihoodScan, PlotMultipleLikelihoodScans)):
            # also handles 2D case
            parts.append(task.pois.replace(",", "_"))
        if isinstance(task, (PlotUpperLimits, PlotMultipleUpperLimits)):
            parts.append(task.scan_parameter.split(",")[0])
        if isinstance(task, PlotMultipleGoodnessOfFits):
            parts.append(task.algo)
        if isinstance(task, PlotExclusionAndBestFit):
            parts.append(f"{task.pois}_{task.scan_parameter.split(',')[0]}")
        parts.append("unblinded" if task.unblind else "asimov")
        if isinstance(task.output(), (list, tuple)):
            pdfs = list(filter(lambda x: x.path.endswith(".pdf"), task.output()))
            assert len(pdfs) == 1, f"Found multiple pdfs: {pdfs}"
            pdf = pdfs[0]
        elif isinstance(task.output(), law.LocalFileTarget):
            pdf = task.output()
        else:
            raise ValueError(f"Can not interpret task.output type: {type(task.output())}")
        if task.statmodel == "StatModelNoSysts":
            parts.append("autoMCStats_only")
        if task.statmodel == "StatModelStatOnly":
            parts.append("stat_only")
        # create directory
        base.mkdir(parents=True, exist_ok=True)
        # full path
        newpath = base / ("__".join(parts) + ".pdf")
        oldpath = pdf.path
        shutil.copy(oldpath, newpath)

    @property
    def copy_message(self):
        return f"[b]Now copy to AN repo from your local laptop[/b]: [cyan]'rsync -r vispa:{self.output().path}/ </path/to/AN-20-119>/figures/nonresonant/results'[/cyan]"

    def run(self):
        from rich.console import Console
        from rich.progress import track
        from rich.tree import Tree

        console = Console()

        console.print(f"Bundling outputs into {self.base} ...")

        tasks = law.util.flatten(self.requires())

        for task in track(tasks, console=console, description="Bundle"):
            console.print(task)
            self.bundle_output(task)

        Σ = law.util.human_bytes(
            sum(f.stat().st_size for f in self.base.rglob("*")), "MB", fmt=True
        )
        console.print(f"Successfully bundled {Σ} into {self.base}:")

        tree = Tree(
            f":open_file_folder: [link file://{self.base}]{self.base}",
            guide_style="bold bright_blue",
        )
        walk_directory(self.base, tree)
        console.print(tree)
        console.print(self.copy_message)


class UnblindingSteps(AN20119):
    step = luigi.IntParameter(default=1)

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        assert 1 <= self.step <= 5, f"step must be between 1 and 3, got {self.step}"

    @property
    def analysis_categories(self):
        if self.step == 1 or self.step == 2 or self.step == 5:
            # all signal + background
            return {
                analysis: analysis_inst.categories.query(name=".+", tags={"fit"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }
        elif self.step == 3:
            # resolved 1b signal + background
            return {
                analysis: analysis_inst.categories.query(name=".+", tags={"background"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "resolved_1b"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }
        elif self.step == 4:
            # resolved 1b signal + resolved 2b signal + background
            return {
                analysis: analysis_inst.categories.query(name=".+", tags={"background"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "resolved_1b"}).keys
                + analysis_inst.categories.query(name=".+", tags={"signal", "boosted"}).keys
                for analysis, analysis_inst in self.analysis_insts.items()
            }

    def store_parts(self):
        return super().store_parts() + (f"step_{self.step}",)

    def requires(self):
        reqs = {}
        if self.step == 1:
            # pulls per year and analysis
            reqs["pulls"] = [
                PlotPullsAndImpactsAN20119.req(
                    self,
                    years=(y,),
                    unblind=True,
                    analyses=(a,),
                    analysis_categories=self.analysis_categories,
                    mc_stats=True,
                    show_best_fit=False,
                )
                for y in self.years
                for a in self.analyses
            ]
            for analyses in self.analyses + [self.analyses]:
                analyses = law.util.make_list(analyses)
                reqs["pulls"].append(
                    PlotPullsAndImpactsAN20119.req(
                        self,
                        unblind=True,
                        analyses=analyses,
                        analysis_categories=self.analysis_categories,
                        mc_stats=True,
                        show_best_fit=False,
                    )
                )

        elif self.step == 2:
            reqs["gof"] = []
            for analyses in self.analyses + [self.analyses]:
                analyses = law.util.make_list(analyses)
                reqs["gof"].append(
                    PlotMultipleGoodnessOfFits.req(
                        self,
                        algo="saturated",
                        toys=1000,
                        analyses=analyses,
                        analysis_categories=self.analysis_categories,
                        # unblind=True,
                    )
                )
            # Prefit/Postfit Plots
            reqs["prefit+postfit"] = {
                (analysis, year): PlotFitDiagnosticsCombined.req(
                    self,
                    year=year,
                    analysis_choice=analysis,
                    log_scale=True,
                    blind_thresh=-np.inf,
                    process_group=("plotting",),
                    unblind=True,
                )
                for year in self.years
                for analysis in self.analyses
            }
        elif self.step >= 3:
            reqs["limits"] = []
            reqs["scans"] = []
            for analyses in self.analyses + [self.analyses]:
                analyses = law.util.make_list(analyses)
                # fmt: off
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r", analyses=analyses, analysis_categories=self.analysis_categories))
                reqs["limits"].append(PlotUpperLimitsAtPoint.req(self, poi="r_qqhh", analyses=analyses, analysis_categories=self.analysis_categories))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, analysis_categories=self.analysis_categories))
                reqs["scans"].append(PlotUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, analysis_categories=self.analysis_categories))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="kl,-20,20,81", analyses=analyses, analysis_categories=self.analysis_categories))
                reqs["scans"].append(PlotMultipleUpperLimits.req(self, scan_parameter="C2V,-4,6,81", analyses=analyses, analysis_categories=self.analysis_categories))
                # fmt: on

        return reqs

    @property
    def copy_message(self):
        return f"[b]Now copy the unblinding step {self.step} from your local laptop[/b]: [cyan]'rsync -r vispa:{self.output().path}/ .'[/cyan]"

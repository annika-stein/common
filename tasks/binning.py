# coding: utf-8

import json
from contextlib import contextmanager
from functools import cached_property
from pathlib import Path

import hist
import law
import numpy as np
import pyhf
from tqdm.auto import tqdm

from config.constants import LAMBDA0
from tasks.base import AnalysisCampaignTask, PoolMap
from tasks.group import GroupCoffeaProcesses
from tasks.mixins import CategoryMixin, ModelMixin, RecipeMixin, VariableMixin
from utils.binning import thresh_rebin2
from utils.util import AsteriskDict, ulimit


class BinningOptBase(
    ModelMixin,
    RecipeMixin,
    CategoryMixin,
    VariableMixin,
    AnalysisCampaignTask,
    PoolMap,
):
    n_parallel_max = 32

    def requires(self):
        return GroupCoffeaProcesses.req(
            self, process_group=list(self.StatModel.processes) + ["data"]
        )

    def output(self):
        return self.local_target("binnings.json")

    @staticmethod
    @contextmanager
    def silencer():
        import logging
        import warnings

        pyhflog = logging.getLogger("pyhf.optimize.mixins")
        pyhflog.disabled = True

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            yield

        pyhflog.disabled = False

    def make_spec(self, sigs, bkgs):
        return {
            "channels": [
                {
                    "name": self.category,
                    "samples": [
                        {
                            "name": name,
                            "data": list(data),
                            "modifiers": [
                                {
                                    "name": "mu",
                                    "type": "normfactor",
                                    "data": None,
                                },
                                {
                                    "name": "statunc",
                                    "type": "staterror",
                                    "data": unc,
                                },
                            ],
                        }
                        for name, (data, unc) in sigs.items()
                    ]
                    + [
                        {
                            "name": name,
                            "data": data,
                            "modifiers": [
                                {
                                    "name": f"{name}_norm",
                                    "type": "normsys",
                                    "data": {"hi": 1.1, "lo": 0.9},  # 10% bkg norm
                                },
                                {
                                    "name": "statunc",
                                    "type": "staterror",
                                    "data": unc,
                                },
                            ],
                        }
                        for name, (data, unc) in bkgs.items()
                    ],
                }
            ],
            "parameters": [{"name": "mu", "inits": [1], "bounds": [[0, 1000]]}],
        }

    def limits(self, sigs, bkgs):
        # sum(bkgs) + mu=1*sum(sigs)
        orig_sigs = sigs.copy()
        orig_bkgs = bkgs.copy()
        sigs = [s[0] for s in sigs.values()]
        bkgs = [b[0] for b in bkgs.values()]
        data = list(np.sum(np.asarray(bkgs), axis=0) + np.sum(np.asarray(sigs), axis=0))

        pdf = pyhf.Model(self.make_spec(sigs=orig_sigs, bkgs=orig_bkgs))

        data += pdf.config.auxdata

        # change step size to smaller/finer steps?
        mu_tests = np.arange(0, 1000, 10)

        try:
            with self.silencer():
                _, exp_limits, (_, _) = pyhf.infer.intervals.upperlimit(
                    data, pdf, mu_tests, level=0.05, return_results=True
                )
        except:
            exp_limits = np.array(
                [
                    np.array(np.nan),
                    np.array(np.nan),
                    np.array(np.nan),
                    np.array(np.nan),
                    np.array(np.nan),
                ]
            )

        return exp_limits

    def rebin(self, edges):
        # rebin
        hview = self.h.view()
        new_binning = np.asarray(edges)
        hnew = hist.Hist(
            self.h.axes["process"],
            hist.axis.Variable(
                new_binning,
                name=self.variable,
            ),
            storage=hist.storage.Weight(),
        )
        loc = self.h.axes[self.variable].index
        ax = hnew.axes[self.variable]
        for bin in range(ax.size):
            hnew[..., bin] = hview[..., loc(ax[bin][0]) : loc(ax[bin][1])].sum(axis=-1)

        return hnew.view().copy()

    def worker(self, e):
        view = self.rebin(e)
        hv = view["value"]
        hv_err = np.sqrt(view["variance"])
        # fix empty bins
        hv[hv <= 0] = 1e-5
        hv_err[hv_err <= 0] = 1e-5
        bkgs = {
            bkg: (
                list(hv[self.h.axes["process"].index(bkg), ...]),
                list(hv_err[self.h.axes["process"].index(bkg), ...]),
            )
            for bkg in self.StatModel.background_processes
        }
        # SM signals
        sigs = {
            sig: (
                list(hv[self.h.axes["process"].index(sig), ...]),
                list(hv_err[self.h.axes["process"].index(sig), ...]),
            )
            for sig in [
                # VBF
                "qqHH_CV_1_C2V_1_kl_1_2B2Tau",
                "qqHH_CV_1_C2V_1_kl_1_2B2V",
                # ggF
                "ggHH_kl_1_kt_1_2B2Tau",
                "ggHH_kl_1_kt_1_2B2V",
            ]
        }
        limits = self.limits(sigs=sigs, bkgs=bkgs)
        limit = limits[2]  # nominal
        return e, float(limit)

    def binnings(self):
        return NotImplementedError()

    @cached_property
    def h(self):
        inp = self.input().load()
        # remove cutflow
        inp.pop("cutflow")
        h = inp[self.variable][:, self.category, "nominal", :]
        del inp
        return h

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")
        self.output().parent.touch()
        # touch cached property
        self.h
        out = {}
        # calc current limit
        with (Path(__file__).parent.parent / self.analysis_choice / "models" / "binning.json").open(
            "r"
        ) as f:
            edges = AsteriskDict(json.load(f)[self.year])
            current_edges = edges[self.category]
            edges, limit = self.worker(current_edges)
            out["current"] = limit
        # other binnings
        binnings = self.binnings()
        work = [e for e in binnings]
        for (edges, limit) in self.pmap(self.worker, work, unit="limit", unordered=False):
            msg = f"Exp. limit: {limit:.2f} - {self.analysis_inst.categories.get(self.category).label_short}, edges: {edges}"
            if np.isnan(limit):
                msg = "[FAILED] " + msg
            tqdm.write(msg)
            key = "_".join((sorted(map(str, edges))))
            if key in out or np.isnan(limit):
                # checked this binning already or failed
                continue
            out[key] = limit
        self.output().dump(out)


class BinningOpt(BinningOptBase):
    n_parallel = 32

    def _tresh2(self, n=100):
        binnings = []
        for i in range(n):
            hnom = self.h.view().copy()
            hnom["value"] += np.random.default_rng().normal(size=hnom["value"].shape) * np.sqrt(
                hnom["variance"]
            )
            htot = np.sum(hnom, axis=1)

            bin_edges = self.h.axes[self.variable].edges

            bidx = self.h.axes["process"].index(
                [p for p in self.h.axes["process"] if "data" not in p and "HH" not in p]
            )
            t = np.arange(1, 25) ** 2
            # get fallback from (weigthed) wmean
            htot = np.sum(hnom, axis=1)
            fallback = htot["variance"] / htot["value"]

            main_bg = {"bbww_dl": ["tt", "st", "DYest"], "bbww_sl": ["tt", "st", "wjets", "fakes"]}

            # disable fallback on non-main BGs
            fallback[
                [
                    proc not in main_bg.get(self.analysis_choice, [])
                    for proc in self.h.axes["process"]
                ]
            ] = 0

            idx = thresh_rebin2(t, hnom[bidx][:, ::-1], fallback[bidx] * LAMBDA0 ** 0.5)
            # correct order + directions
            idx = hnom.shape[1] - 1 - idx[::-1]

            new_binning = np.r_[bin_edges[0], bin_edges[idx], bin_edges[-1]]
            new_binning = np.unique(new_binning)
            binnings.append(new_binning)
        return binnings

    def binnings(self):
        binnings = self._tresh2(n=500)
        self.logger.info(f"Going to test {len(binnings)} different binnings")
        return binnings


class EvalBinningOpt(ModelMixin, RecipeMixin, CategoryMixin, VariableMixin, AnalysisCampaignTask):
    def requires(self):
        return BinningOpt.req(self)

    def output(self):
        return {
            "plot": self.local_target("binning_opt.pdf"),
            "plot_hist": self.local_target("binning_opt_hist.pdf"),
            "edges_hist": self.local_target("edges_hist.pdf"),
            "best": self.local_target("best.json"),
        }

    def run(self):
        import matplotlib.pyplot as plt
        from matplotlib.patches import Patch

        self.output()["plot"].parent.touch()
        results = self.input().load()
        current = results.pop("current", 1)
        results = dict(sorted(results.items(), key=lambda item: item[1]))
        results = {tuple(map(float, k.split("_"))): v for k, v in results.items()}

        self.logger.info(
            f"[b]Ranking of {len(results)} unique binnings - {self.analysis_choice}, {self.year}, {self.category}:[/b]"
        )
        self.logger.info(f"0. current exp. limit: {current:.2f}")
        for i, (k, v) in enumerate(results.items()):
            self.logger.info(f"{i+1}. exp. limit: {v:.2f}, edges (bins={len(k)-1}): {k}")

        vals = [current] + list(results.values())
        # plot
        fig, ax = plt.subplots()
        bars = ax.bar(
            np.arange(0, len(results.keys()) + 1, 1),
            vals,
            color="green",
        )
        bars[0].set_color("red")
        ax.axhline(y=current, color="black", linestyle="--")
        ax.set_ylim(bottom=min(vals) - 0.15 * min(vals), top=max(vals) + 0.15 * max(vals))
        ax.set_xlabel("Test number")
        ax.set_ylabel(r"Upper $95\%$ exp. CL limit on $\mu$")
        ax.set_title(
            f"{self.analysis_choice} - {self.year}, {self.analysis_inst.categories.get(self.category).label_short} ({len(results)} binnings)"
        )
        ax.text(
            0.05,
            0.85,
            f"Current: {current:.2f}"
            + "\n"
            + f"Min: {min(vals):.2f}, rel diff: {100*(abs(current-min(vals))/current):.2f}%"
            + "\n"
            + f"Max: {max(vals):.2f}, rel diff: {100*(abs(current-max(vals))/current):.2f}%",
            transform=ax.transAxes,
        )
        ax.legend(
            handles=[
                Patch(facecolor="green", edgecolor="green", label="Tested binnings"),
                Patch(facecolor="red", edgecolor="red", label="Current binning"),
            ],
            loc="best",
        )
        plt.tight_layout()
        fig.savefig(self.output()["plot"].path)

        # plot hist
        fig, ax = plt.subplots()
        bars = ax.hist(
            vals,
            bins=30,
            color="green",
        )
        line = ax.axvline(x=current, color="black", linestyle="--", label="Current binning")
        ax.set_xlabel(r"Upper $95\%$ exp. CL limit on $\mu$")
        ax.set_ylabel("Number of binnings")
        ax.set_title(
            f"{self.analysis_choice} - {self.year}, {self.analysis_inst.categories.get(self.category).label_short} ({len(results)} binnings)"
        )
        ax.text(
            0.05,
            0.85,
            f"Current: {current:.2f}"
            + "\n"
            + f"Min: {min(vals):.2f}, rel diff: {100*(abs(current-min(vals))/current):.2f}%"
            + "\n"
            + f"Max: {max(vals):.2f}, rel diff: {100*(abs(current-max(vals))/current):.2f}%",
            transform=ax.transAxes,
        )
        ax.legend(
            handles=[
                Patch(facecolor="green", edgecolor="green", label="Tested binnings"),
                line,
            ],
            loc="upper right",
        )
        plt.tight_layout()
        fig.savefig(self.output()["plot_hist"].path)

        # plot hist of edges
        fig, ax = plt.subplots()
        ax.hist(
            law.util.flatten(results.keys()),
            bins=100,
            color="green",
            ec="green",
        )
        with (Path(__file__).parent.parent / self.analysis_choice / "models" / "binning.json").open(
            "r"
        ) as f:
            edges = AsteriskDict(json.load(f)[self.year])
            current_edges = edges[self.category]
        for i in current_edges:
            ax.axvline(x=i, color="black", linestyle="--", label="Current edges")
        ax.set_xlabel(r"Bin edge i")
        ax.set_ylabel("Number of binnings")
        ax.set_title(
            f"{self.analysis_choice} - {self.year}, {self.analysis_inst.categories.get(self.category).label_short} ({len(results)} binnings)"
        )
        ax.legend(
            handles=[
                Patch(facecolor="green", edgecolor="green", label="Tested edges"),
                line,
            ],
            loc="upper right",
        )
        plt.tight_layout()
        fig.savefig(self.output()["edges_hist"].path)

        # save best:
        self.output()["best"].dump(list(list(results.keys())[0]))


class EvalBinningOptWrapper(
    ModelMixin, RecipeMixin, VariableMixin, AnalysisCampaignTask, law.WrapperTask
):
    @property
    def sigcats(self):
        return self.analysis_inst.categories.query(name=".+", tags={"fit", "signal"}).keys

    def requires(self):
        from IPython import embed

        embed()
        return {c: EvalBinningOpt.req(self, category=c) for c in self.sigcats}


# Require all optimal binnings and write asterisk dict
# class CombinedBinningAsteriskDict(CombinationTask):
#     pass

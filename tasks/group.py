# coding: utf-8

from itertools import product

import law
import numpy as np
from tabulate import tabulate
from tqdm.auto import tqdm

from tasks.base import AnalysisCampaignTask, PoolMap
from tasks.coffea import CoffeaProcessor
from tasks.mixins import ModelMixin, PGroupMixin, RecipeMixin
from utils.util import ulimit, view_sdtype_flat


class GroupCoffeaProcesses(ModelMixin, PGroupMixin, RecipeMixin, PoolMap, AnalysisCampaignTask):
    def requires(self):
        req = {"processor": CoffeaProcessor.req(self)}
        return req

    def output(self):
        # return self.local_target("hists.coffea")
        return self.local_target("hists_reorder_test.coffea")
        # return self.local_target("issue_89_002.coffea")  # TODO: Change here

    @property
    def n_parallel_max(self):
        return len(self.analysis_inst.variables)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        from collections import defaultdict

        import hist
        from tqdm.auto import tqdm

        ulimit(AS="hard")

        self.output().parent.touch()
        inp = self.input()["processor"].load()

        Pget = self.analysis_inst.processes.get

        # no nested process groups
        nested = [
            f"{root} contains {other}"
            for root, other in product(map(Pget, self.legacy_processes), repeat=2)
            if root.has_process(other)
        ]
        if nested:
            msg = "illegal nesting in process group roots:\n"
            msg += "\t\n".join(nested)
            self.logger.warning(msg)

        vars = self.analysis_inst.variables.keys
        hists = {k: v.hist for k, v in inp["histograms"].items() if k in vars}

        for cf in "cutflow", "cutflow_raw":
            assert cf not in hists
            if cf in inp:
                hists[cf] = inp[cf].hist
            else:
                self.logger.info(f"missing '{cf}' in processor output: {list(inp.keys())}")

        # unpack sum_gen_weights
        sum_gen_weights = defaultdict(dict)
        for (dsname, dsshift), sgw in inp["sum_gen_weights"].items():
            sum_gen_weights[dsname][dsshift] = sgw

        skipped = set()
        sys_mul2 = defaultdict(dict)
        for k, h in tqdm(hists.items(), unit="variable"):
            assert h.axes[0].name == "dataset"

            hout = hist.Hist(
                hist.axis.StrCategory(self.legacy_processes, name="process", growth=True),
                *h.axes[1:],
                metadata=h.metadata,
                storage=hist.storage.Weight(),
                # ds_opts={},  # disable compression
            )
            vout = hout.view(True)
            # reallocate with C order
            vout = np.zeros(vout.shape, dtype=vout.dtype, order="C")
            assert vout.dtype.names == ("value", "variance")

            for d in tqdm(h.axes["dataset"], unit="dataset", leave=False):
                # skip dataset which was present during processor run, but now gone from config
                if d not in self.campaign_inst.datasets.keys:
                    # TODO: add warning
                    continue
                # get the process of the dataset
                processes = self.campaign_inst.datasets.get(d).processes.values
                if len(processes) == 1 or all(p.is_data for p in processes):
                    p = processes[0]
                else:
                    raise Exception("MC dataset must only define one process")

                # get the corresponding group root process (or skip if none)
                r = self.getPGroot(p)
                if r is None:
                    skipped.add(p.name)
                    continue

                # extract data
                view = h.view(True)[h.axes[0].index(d)]
                assert view.dtype.names == ("value", "variance")

                if p.is_mc:
                    # Xsec lumi weight
                    xs = p.xsecs[13].nominal * self.campaign_inst.aux["lumi"]
                    # combine xs & sum_gen_weights
                    w = xs / sum_gen_weights[d]["nominal"]

                    if "systematic" in h.axes.name and set(h.axes["systematic"]) - {"nominal"}:
                        w = np.full((h.axes["systematic"].extent,), w)

                        for syst, sgw in sum_gen_weights[d].items():
                            if syst in h.axes["systematic"]:
                                w[h.axes["systematic"].index(syst)] = xs / sgw

                        # fix missing factor 2 for some ScaleWeights, see:
                        # https://hypernews.cern.ch/HyperNews/CMS/get/generators/4383.html?inline=-1
                        if d.startswith("DY") and "ToLL_" in d:
                            tot = view["value"].sum(axis=-1)
                            tot = tot[[cat.endswith("_inclusive_sr_prompt") for cat in h.axes[1]]]
                            nom = tot[:, h.axes["systematic"].index("nominal")]
                            for i, sys in enumerate(h.axes["systematic"]):
                                if not sys.startswith("ScaleWeight_"):
                                    continue
                                ratio = tot[:, i] / nom
                                if np.any(np.isfinite(ratio)) and 0 < np.nanmin(ratio) <= 0.55:
                                    w[i] *= 2
                                    sys_mul2[d][sys] = (
                                        np.nanmin(ratio),
                                        np.nanmean(ratio),
                                        np.nanmax(ratio),
                                    )

                        if np.unique(w).shape == (1,):
                            w = w[0]
                        else:
                            # add dimension at the end for broadcasting into the view
                            ndim_after_syst = len(h.axes) - h.axes.name.index("systematic") - 1
                            w = w[(...,) + (None,) * ndim_after_syst]

                    view["value"] *= w
                    view["variance"] *= w**2

                # sum up
                view_sdtype_flat(vout)[hout.axes[0].index(r.name)] += view_sdtype_flat(view)

            hout.view(True)[...] = vout
            hists[k] = hout

        if sys_mul2:
            sys = set()
            for data in sys_mul2.values():
                sys.update(data.keys())
            sys = sorted(sys)

            print(
                "Fixed missing factor 2 for",
                tabulate(
                    [
                        [ds]
                        + [
                            f"{data[s][0]:.3f} < {data[s][0]:.3f} < {data[s][0]:.3f}"
                            if s in data
                            else ""
                            for s in sys
                        ]
                        for ds, data in sorted(sys_mul2.items())
                    ],
                    headers=["Dataset"] + sys,
                ),
                sep="\n",
            )

        if skipped:
            print(
                f"Skipped processes (not found in {', '.join(self.legacy_processes)}):",
                ", ".join(sorted(skipped)),
                sep="\n",
            )

        # hook for data-driven background estimations
        # à la: data - Σ MC
        processor = self.requires()["processor"].Processor
        hists = processor.group_processes(hists, self)
        self.output().dump(hists)


class Rebin(ModelMixin, PGroupMixin, RecipeMixin, AnalysisCampaignTask, PoolMap):
    def requires(self):
        return GroupCoffeaProcesses.req(self, process_group=self.process_group)

    def output(self):
        return self.local_target("rebinned_hists.coffea")

    @property
    def n_parallel_max(self):
        return len(self.analysis_inst.variables)

    @law.decorator.timeit(publish_message=True)
    @law.decorator.safe_output
    def run(self):
        ulimit(AS="hard")

        self.output().parent.touch()
        inp = self.input().load()

        # remove cutflow
        inp.pop("cutflow")
        inp.pop("cutflow_raw", None)

        # do rebinning
        rebinned_hists = {}
        for variable, hist in tqdm(inp.items(), desc="rebin", unit="variable"):
            variable = self.analysis_inst.variables.get(variable)
            rebinned = self.StatModel.rebin(variable, hist, self)
            rebinned_hists[variable.name] = rebinned

        self.output().dump(rebinned_hists)

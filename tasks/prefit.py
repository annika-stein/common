# -*- coding: utf-8 -*-

import hashlib
from functools import cached_property

import hist
import law
import luigi
import numpy as np
import uproot3 as uproot

from tasks.base import AnalysisCampaignTask, CombinationTask, PoolMap
from tasks.combine import (
    FitDiagnosticsCombined,
    FitDiagnosticsCombinedPostprocessed,
    FitDiagnosticsPostprocessed,
    years,
)
from tasks.datacard import DatacardProducer, DatacardsVariablesWrapper, DatacardsWrapper
from tasks.mixins import CreateIssueMixin, ModelMixin, RecipeMixin
from tasks.plotting import PlotHistsBase
from tools.numpy import set_thresh
from utils.uproot import decycle
from utils.util import GitlabIssue, pscan_vispa


class PrepareFitDiagnostics(AnalysisCampaignTask):
    @cached_property
    def legacy_processes_hash(self):
        if self.year == "run2":
            return "-".join(
                self.req(self, year=str(year)).legacy_processes_hash[:20] for year in years
            )
        else:
            return super().legacy_processes_hash

    def store_parts(self):
        parts = super().store_parts()
        parts += (self.requires().datacards_hash,)
        if self.unblind:
            parts += ("PreAndPostfit",)
        else:
            parts += ("Prefit",)
        return parts

    @cached_property
    def plotting_process_groups(self):
        ret = super().plotting_process_groups
        ret["data"].append("data_err")
        return ret

    def parse_cat(self, cat):
        _, cat = cat.split("__", 1)
        year, cat = cat.split("_", 1)
        year = year.replace("y", "")
        label = self.analysis_inst.categories.get(cat).label_short
        return label, self.year

    def unpack_data(self, shape, cat, _):
        cat_label, year = self.parse_cat(cat)
        return cat_label, self.input().load()[shape][cat]


class PlotFitDiagnosticsBase(PrepareFitDiagnostics, PlotHistsBase, PoolMap):
    eopt = dict(PlotHistsBase.eopt, label="Total Unc.")
    unblind = FitDiagnosticsCombined.unblind
    ratio_max_delta = 0.5

    @law.decorator.safe_output
    def run(self):
        work = [
            (shape, cat, None)
            for shape, shape_hists in self.input().load().items()
            if shape != "fit_results"
            for cat in shape_hists.keys()
        ]
        work += self.collate(work, self.analysis_inst.aux["collate_cats"])
        self.basename
        for _ in self.pmap(self.worker, work, unit="plot", unordered=True):
            pass

        self.output().touch()
        pscan = pscan_vispa(
            directory=self.local_path(),
            regex=r"(?P<stage>.+)/(?P<channel>[^_]+)_(?P<category>.+)/(?P<blind_thresh>.+)_(?P<scale>.+)\.(?P<format>[^\.]+)",
        )
        self.logger.info(pscan)
        if getattr(self, "create_issue", None):
            GI = GitlabIssue(
                self,
                opts={
                    "important_params": [
                        "year",
                        "blind_thresh",
                        "log_scale",
                        "format",
                        "unblind",
                    ],
                },
            )
            GI.post_to_issue(message=pscan)

    def get_title(self, shape, cat):
        cat_label, year = self.parse_cat(cat)
        s = shape.split("_", 1)[-1]
        s = dict(
            fit_s=r"$\text{Fit}_\text{S}",
            fit_sb=r"$\text{Fit}_\text{S+B}",
        ).get(s, s)
        # inser info about covQual
        q = self.input().load().get("fit_results", {}).get(shape[7:], {}).get("_covQual", None)
        q = {
            -1: "failed",
            0: "not pos. def.",
            1: "approximate",
            2: "forced pos. def.",
            3: None,
        }.get(q, q)
        if q is not None:
            s = f"{s}, {q} cov. matrix"
        return f"{cat_label} {year} ({s})"


class PlotFitDiagnostics(PlotFitDiagnosticsBase):
    def requires(self):
        return FitDiagnosticsPostprocessed.req(self)


class PlotFitDiagnosticsWrapper(DatacardsWrapper):
    _task = PlotFitDiagnostics


class PlotFitDiagnosticsVariablesWrapper(DatacardsVariablesWrapper):
    _producer = PlotFitDiagnostics


class PlotFitDiagnosticsCombined(PlotFitDiagnosticsBase, CreateIssueMixin):
    priority = 200

    @property
    def legacy_processes_hash(self):
        if self.year == "run2":
            """
            For the combined year (run2), the "config" is the set of all individual per-year configs.
            We merge the legacy_process_hashes into one new hash.
            """
            hashes = []

            def condition(outp):
                return isinstance(outp, dict) and all(
                    isinstance(o, DatacardsWrapper) for o in outp.values()
                )

            for year, task in self.deep_requires(condition).items():
                processes = task.analysis_inst.aux["process_groups"].get(
                    self.process_group[0], self.process_group
                )
                hash = hashlib.sha256("".join(sorted(processes)).encode("utf-8")).hexdigest()
                hashes.append(hash)
            if len(set(hashes)) == 1:
                return hashes[0]
            else:
                return hashlib.sha256("".join(hashes).encode("utf-8")).hexdigest()
        else:
            return super().legacy_processes_hash

    def requires(self):
        if self.year == "run2":
            years = ("2016", "2017", "2018")
        else:
            years = (self.year,)
        return FitDiagnosticsCombinedPostprocessed.req(
            self,
            analyses=(self.analysis_choice,),
            years=years,
        )

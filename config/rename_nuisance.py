# coding: utf-8

"""
This files only purpose is to be copied to lxplus and used with inference tools for pretty Pulls & Impacts nuisance label plots!
"""

import re

final_state_map = {"dl": "DL", "sl": "SL"}

categories_sl = {
    "all_incl_sr_prompt_wjets_other": "incl. W+Jets + Other",
    "all_incl_sr_prompt_dnn_node_wjets": "incl. W+Jets",
    "all_incl_sr_prompt_dnn_node_class_other": "incl. Other",
    "all_boosted_sr_prompt_dnn_node_class_HHGluGlu_NLO": "boost. HH(GGF)",
    "all_boosted_sr_prompt_dnn_node_class_HHVBF_NLO": "boost. HH(VBF)",
    "all_boosted_sr_prompt_tophiggs": "boost. Top+Higgs",
    "all_resolved_sr_prompt_tophiggs": "res. #leq 1b Top+Higgs",
    "all_resolved_1b_sr_prompt_dnn_node_class_HHGluGlu_NLO": "res. = 1b HH(GGF)",
    "all_resolved_1b_sr_prompt_dnn_node_class_HHVBF_NLO": "res. = 1b HH(VBF)",
    "all_resolved_2b_sr_prompt_dnn_node_class_HHGluGlu_NLO": "res. #geq 2b HH(GGF)",
    "all_resolved_2b_sr_prompt_dnn_node_class_HHVBF_NLO": "res. #geq 2b HH(VBF)",
}

categories_dl = {
    "all_incl_sr_grp_dy_vv": "incl. DY+VV(V)",
    "all_boosted_1b_sr_dnn_node_class_HHGluGlu_NLO": "boost. HH(GGF)",
    "all_boosted_1b_sr_dnn_node_class_HHVBF_NLO": "boost. HH(VBF)",
    "all_boosted_1b_sr_grp_other_top": "boost. Top+Others",
    "all_resolved_sr_grp_other_top": "res. #leq 1b Top+Others",
    "all_resolved_1b_sr_dnn_node_class_HHGluGlu_NLO": "res. = 1b HH(GGF)",
    "all_resolved_1b_sr_dnn_node_class_HHVBF_NLO": "res. = 1b HH(VBF)",
    "all_resolved_2b_sr_dnn_node_class_HHGluGlu_NLO": "res. #geq 2b HH(GGF)",
    "all_resolved_2b_sr_dnn_node_class_HHVBF_NLO": "res. #geq 2b HH(VBF)",
}


categories_map = categories_sl.copy()
categories_map.update(categories_dl)


def rename_nuisance(name):
    # THU_HH
    if name == "THU_HH":
        return "QCDscale_ggHH + mtop"

    # mc stats?
    m = re.match(r"^prop_binbbww_(.+)__(201[0-9])_(.+)_bin(\d{1,3})(|_(.+))$", name)
    if m:
        final_state = final_state_map[m.group(1)]
        year = int(m.group(2))
        cat = categories_map.get(m.group(3), m.group(3))
        b = int(m.group(4))
        proc = m.group(5) or ""
        if proc.startswith("_"):
            proc = proc[1:]
        if proc:
            # simplify process names
            m_bbtt_ggf = re.match("^ggHH_kl_([^_]+)_kt_([^_]+)(_hbbhtt)?$", proc)
            m_bbtt_vbf = re.match("^qqHH_CV_([^_]+)_C2V_([^_]+)_(kl|C3)_([^_]+)(_hbbhtt)?$", proc)
            m_bbvv_ggf = re.match("^ggHH_kl_([^_]+)_kt_([^_]+)(_hbbhww)?$", proc)
            m_bbvv_vbf = re.match("^qqHH_CV_([^_]+)_C2V_([^_]+)_(kl|C3)_([^_]+)(_hbbhww)?$", proc)
            if m_bbtt_ggf:
                proc = "ggHH (bb#tau#tau)#scale[0.6]{{ (kl={}, kt={})}}".format(
                    m_bbtt_ggf.group(1), m_bbtt_ggf.group(2)
                )
            elif m_bbtt_vbf:
                proc = "qqHH (bb#tau#tau)#scale[0.6]{{ (k2V={}, kV={}, kl={})}}".format(
                    m_bbtt_vbf.group(2), m_bbtt_vbf.group(1), m_bbtt_vbf.group(4)
                )
            elif m_bbvv_ggf:
                proc = "ggHH (bbVV)#scale[0.6]{{ (kl={}, kt={})}}".format(
                    m_bbvv_ggf.group(1), m_bbvv_ggf.group(2)
                )
            elif m_bbvv_vbf:
                proc = "qqHH (bbVV)#scale[0.6]{{ (k2V={}, kV={}, kl={})}}".format(
                    m_bbvv_vbf.group(2), m_bbvv_vbf.group(1), m_bbvv_vbf.group(4)
                )
            # elif proc == "ttH_hbb":
            #     proc = "ttH(bb, #tau#tau)"
            proc += " "
        return "MC stat, {} {} {}, {}bin {}".format(
            final_state,
            year,
            cat,
            proc,
            b + 1,
        )

    return name

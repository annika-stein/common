# coding: utf-8
# flake8: noqa

import csv
import json
import logging
import os.path
from collections import defaultdict
from fnmatch import fnmatch
from operator import itemgetter, methodcaller

import utils.aci as aci
from config.common import get as Pget
from config.constants import HHres, HHxs
from config.processes import renumerate
from utils.dsdb import auto_apply, dasquery

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def init_campaign(campaign, name, file, **kwargs):
    if name == "__main__":
        from rich.logging import RichHandler

        logging.basicConfig(
            level="NOTSET",
            format="%(message)s",
            datefmt="[%X]",
            handlers=[RichHandler()],
        )
    campaign.aux["year"] = int(campaign.name.rsplit("_", 1)[-1])
    load_campaign(campaign, **kwargs)
    auto_apply(campaign, name, file)


def load_campaign(campaign, kind="nano", maxBRshift=1e-3, ds_shifts={}):
    prefix = os.path.join(os.path.dirname(__file__), "campaigns", "%s_%s" % (campaign.name, kind))

    # collect json info (name, keys, aux, ...)
    datasets = {}
    for ds in json.load(open("%s.json" % prefix, "r")):
        base = ds["name"].lstrip("/")
        if base == "NON_EXISTENT":
            continue
        keys = ds["keys"] = [k.rsplit("_nogen", 1)[0] for k in ds["keys"]]
        # reparse name (remove _ext)
        if base.endswith("_ext"):
            base = base[:-4]
        ds["name"] = base
        # consider preexisting ds
        if base in datasets:
            assert dict(datasets[base], keys=keys) == ds, ds
            ds["keys"] = datasets[base]["keys"] + keys
            logger.info("fusing dataset: %s", base)
        datasets[base] = ds

    # extend them
    seen_ids = set()
    for fields in csv.reader(open("%s.csv" % prefix, "r")):
        if not fields or fields[0].lstrip().startswith("#"):
            continue
        dsname, dsid = fields[:2]
        procs = fields[2:]
        dsname = dsname.strip()
        if dsname not in datasets:
            logger.warning("unknown dataset: %s", dsname)
            continue
        dsid = int(dsid)
        assert dsid is not seen_ids, dsid
        seen_ids.add(dsid)
        if not procs:
            pname = dsname.replace("-", "_")
            try:
                procs = [Pget(pname)]
            except ValueError:
                tmpl = 'aci.Process(name="%s", id=9999, label="%s", xsecs={13: sn.Number(%f)}),' % (
                    pname,
                    pname,
                    datasets[dsname]["misc"].get("xs", 0),
                )
                logger.warning("missing process: %s, expected template: %s", pname, tmpl)
                continue
        else:
            procs = [Pget(p.strip()) for p in procs]
        # TODO: check xsec compatible
        datasets[dsname].update(id=dsid, procs=procs)

    outAlt = {
        "2B2WToLNu2J": ("2B2WToLNu2J", "2B2VTo2BLNu2J", "2B2VLNu2J", "2B2VToLNu2J", "2BLNu2J")
    }

    # make the damn singnal samples manually
    for dsid, dsname, br in (
        (im, "%sTo%sToHHTo%s_M-%d_narrow" % (prod, reso, outp, mass), br)
        for ip, prod in renumerate(HHres["prod"], 10000, 2000)
        for ir, reso in renumerate(HHres["resonance"], ip, 500)
        for io, (outp, br) in renumerate(
            ((outp, br) for out1, br in HHres["decays"] for outp in outAlt.get(out1, [out1])),
            ir,
            30,
        )
        for im, mass in renumerate(HHres["masses"], io)
    ):
        assert dsid not in seen_ids, dsid
        seen_ids.add(dsid)
        if dsname in datasets:
            br1 = datasets[dsname]["misc"]["xs"]
            brS = (br - br1) / br
            if abs(brS) > maxBRshift >= 0:
                logger.warn("%r has BR shift %.2e (%.7f vs %.7f expected)", dsname, brS, br1, br)
            pname = dsname
            for main, alts in outAlt.items():
                for alt in alts:
                    pname = pname.replace(alt, main)
            datasets[dsname].update(id=dsid, procs=[Pget(pname)])

    # non resonant samples
    for dsid, prod, outp, coup in (
        (ic, prod, outp, coup)
        for ip, (prod, coups) in renumerate(
            dict(
                GluGlu=["node_%s" % {0: "SM", 14: "box"}.get(i, i) for i in range(15)]
                + [
                    ("node_cHHH%.2f" % kl).rstrip("0").rstrip(".").replace(".", "p")
                    for kl in HHxs["gf_nnlo_gdocs"].keys()
                ],
                VBF=HHxs["vbf_lo"].keys(),
            ).items(),
            15000,
            2000,
        )
        for io, outp in renumerate(
            (outp for out1, br in HHres["decays"] for outp in outAlt.get(out1, [out1])), ip, 200
        )
        for ic, coup in renumerate(coups, io)
    ):
        assert dsid not in seen_ids, dsid
        seen_ids.add(dsid)
        # make dsname
        dsname = "%s%sHHTo%s_%s" % (prod, "" if prod == "VBF" else "To", outp, coup)
        proc = "%sHH_%%s_%s" % (dict(GluGlu="gg", VBF="qq")[prod], outp)
        if "node_cHHH" in coup:
            proc %= "kl_%s_kt_1" % coup.replace("node_cHHH", "")
        elif "C3_" in coup:
            proc %= coup.replace("C3_", "kl_")
        else:
            proc = dsname
        for main, alts in outAlt.items():
            for alt in alts:
                proc = proc.replace(alt, main)

        # print("%-50s %5d %s" % (dsname, dsid, proc))
        if dsname in datasets:
            datasets[dsname].update(id=dsid, procs=[Pget(proc)])

    # actually create them
    for ds in sorted(datasets.values(), key=lambda ds: (ds.get("id", -1), ds["name"])):
        if "id" not in ds:
            logger.warning("dataset without associated id: %s", ds["name"])
            continue
        info = {}
        info["nominal"] = {"keys": ds["keys"]}
        obj = aci.Dataset(
            name=ds["name"],
            id=ds["id"],
            is_data=ds.get("is_data", False),
            info=info,
            aux=ds.get("aux", None) or {},
        )
        obj.processes.extend(ds.get("procs", ()))
        if len(obj.processes) == 0:
            logger.warning("dataset without any processes: %s", obj.name)
        campaign.datasets.add(obj)

    # create dataset shifts
    for dskey, shifts in ds_shifts.items():
        try:
            # need to 'context' (via suffix) per year to avoid clashes
            ds = campaign.datasets.get(dskey)
        except KeyError:
            ds = None
        if ds is None:
            logger.warning(
                "dataset shifts provided for unknown dataset: %r in %s"
                % (dskey, campaign.aux["year"])
            )
            continue
        for name, info in shifts.items():
            if name in ds.info:
                raise ValueError("%r already has shift %s" % (ds, name))
            if not isinstance(info, dict):
                info = dict(keys=info)
            ds.info[name] = info


def find_ds_shifts(campaign, shifts, queries=(), query1=False):
    l = logger
    # .getChild("find_shifts")
    # l.setLevel(logging.DEBUG)
    if not isinstance(queries, (list, tuple)):
        queries = [queries]
    out = {}
    for shift, repl in shifts.items():
        if not callable(repl):
            repl = methodcaller("replace", repl, shift)
        l.info("processing shift %r with replacement %r", shift, repl)
        qcache = {}
        for q in queries:
            r = repl(q)
            if r != q:
                qcache[q] = list(dasquery("dataset=%s" % r, raw=True))
        l.debug("query cache: %r", qcache)
        for ds in campaign.datasets.values:
            if ds.is_data:
                continue
            l.debug("testing dataset: %r", ds)
            ok = []
            for key in ds.keys:
                l.debug("testing key: %r", key)
                rk = repl(key)
                for q, d in qcache.items():
                    if fnmatch(key, q):
                        l.debug("key %r matches cached query %r", key, q)
                        if rk in d:
                            d.remove(rk)
                            ok.append(rk)
                        break
                else:
                    if query1:
                        l.debug("key %r without cached query, querying %r", key, rk)
                        ok.extend(dasquery("dataset=%s" % rk, raw=True))
            if ok:
                l.info("dataset %r got %r keys for shfit %r", ds, ok, shift)
                out.setdefault(ds.name, {})[shift] = ok[0] if len(ok) == 1 else ok
        if qcache:
            l.info(
                "queries entries without matches: %s",
                "\n\t".join(sorted(sum(qcache.values(), [""]))),
            )
    print(json.dumps(out, indent=4, sort_keys=True))
    exit()


def check_exclusive(objs, allowed_group=()):
    exclusive = defaultdict(list)
    for obj in objs:
        if obj.aux is not None:
            exclusive[obj.aux.get("exclusive", None)].append(obj)
        else:
            exclusive[None].append(obj)
    exclusive = {
        excl: objs
        for excl, objs in exclusive.items()
        if excl is not None and 1 < len(objs) and excl not in allowed_group
    }
    if exclusive:
        raise RuntimeError(
            "\n\t".join(
                ["forbidden use of multiple exclusive objects, choose one for each group:"]
                + [
                    "\n\t\t".join(["group '%s', objects:" % group] + list(map(repr, objs)))
                    for group, objs in exclusive.items()
                ]
            )
        )


class PrepareConfig(object):
    """
    Remove all datasets from analysis_inst, which are not linked to the given processes.
    The remaining datasets are kept on the campaign and further used for processing.
    """

    def __init__(self, analysis_inst, processes=(), **kwargs):
        self.analysis_inst = analysis_inst
        self.trim_datasets(processes, **kwargs)
        self.check_process_groups()

    def trim_datasets(
        self,
        processes,
        skip_process=lambda process: False,
        ignore_processes=(),
        ignore_datasets=(),
        allowed_exclusive_processes=(),
    ):

        Pget = self.analysis_inst.processes.get
        P = []

        ignore_processes = set(ignore_processes)

        # collect all processes and their child_processes
        for process in processes:
            process = Pget(process)
            for _, p in process.walk_processes(include_self=True):
                if p.name in ignore_processes or (skip := skip_process(p)):
                    if skip:
                        # also ignore all child processes if process was skipped
                        children = set()
                        for _, p in p.walk_processes():
                            children.add(p.name)
                        ignore_processes |= children
                    continue
                else:
                    P.append(p)

        check_exclusive(P, allowed_group=allowed_exclusive_processes)
        P = {p.name for p in P}
        for campaign in self.analysis_inst.campaigns.values:
            D = campaign.datasets.values
            check_exclusive(D)
            check_exclusive(map(campaign.datasets.get, ignore_datasets))
            for dataset in D:
                if dataset.name in ignore_datasets or len(set(dataset.processes.keys) & P) == 0:
                    campaign.datasets.remove(dataset)

    def check_process_groups(self):
        Pget = self.analysis_inst.processes.get
        for pgroup, processes in self.analysis_inst.aux.get("process_groups", {}).items():
            processes = {p.name for p in map(Pget, processes) if not p.aux.get("dynamic", False)}
            for campaign in self.analysis_inst.campaigns.values:
                campaign_processes = set()
                for dataset in campaign.datasets.values:
                    campaign_processes |= set(
                        [
                            p.name
                            for process in dataset.processes.values
                            for _, p in process.walk_parent_processes(include_self=True)
                        ]
                    )
                if procs_without_datasets := (processes - campaign_processes):
                    logger.warning(
                        "Process group '%s' has processes without corresponding (active) datasets: %s in campaign %s",
                        pgroup,
                        list(procs_without_datasets),
                        campaign.name,
                    )
